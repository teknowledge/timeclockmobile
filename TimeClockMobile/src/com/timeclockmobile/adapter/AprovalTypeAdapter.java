package com.timeclockmobile.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.timeclockmobile.R;

public class AprovalTypeAdapter extends ArrayAdapter<String> {

	// Your sent context
	private Context context;
	// Your custom values for the spinner (User)
	private ArrayList<String> mApprovalTypes;

	public AprovalTypeAdapter(Context context, int textViewResourceId,
			ArrayList<String> approvalTypeArray) {
		super(context, textViewResourceId, approvalTypeArray);
		this.context = context;
		this.mApprovalTypes = approvalTypeArray;
	}

	public int getCount() {
		return mApprovalTypes.size();
	}

	public String getItem(int position) {
		return mApprovalTypes.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	// And the "magic" goes here
	// This is for the "passive" state of the spinner
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// I created a dynamic TextView here, but you can reference your own
		// custom layout for each spinner item
		// TextView label = new TextView(context);
		// label.setTextColor(Color.BLACK);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View spView = inflater.inflate(R.layout.spinner_list_item, parent,
				false);

		TextView label = (TextView) spView.findViewById(R.id.title_text);
		// Then you can get the current item using the values array (Users
		// array) and the current position
		// You can NOW reference each method you has created in your bean object
		// (User class)
		label.setText(mApprovalTypes.get(position));

		// And finally return your dynamic (or custom) view for each spinner
		// item
		return label;
	}

	// And here is when the "chooser" is popped up
	// Normally is the same view, but you can customize it if you want
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TextView label = new TextView(context);
		// label.setTextColor(Color.BLACK);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View spView = inflater.inflate(R.layout.spinner_list_item, parent,
				false);

		TextView label = (TextView) spView.findViewById(R.id.title_text);
		label.setText(mApprovalTypes.get(position));

		return label;
	}
}
