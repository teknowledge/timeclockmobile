package com.timeclockmobile.adapter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.ManageJobTypesActivity;
import com.timeclockmobile.R;
import com.timeclockmobile.data.JobTypes;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class JobTypeAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<JobTypes> mJobTypesList;
	LayoutInflater inflater;
	public ProgressDialog progressDialog;
	private DataBaseHelper db;
	Utils utils = new Utils();

	public JobTypeAdapter(Context mContext, int layoutResourceId,
			ArrayList<JobTypes> jobTypesArrayList) {
		this.mContext = mContext;
		this.mJobTypesList = jobTypesArrayList;
		this.layoutResourceId = layoutResourceId;
		progressDialog = new ProgressDialog(mContext);
		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new DataBaseHelper(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mJobTypesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mJobTypesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.jobTypeName = (TextView) convertView
					.findViewById(R.id.client_name);

			holder.edit_jobType = (ImageButton) convertView
					.findViewById(R.id.edit_client);
			holder.delete_jobType = (ImageButton) convertView
					.findViewById(R.id.delete_client);
			holder.client_address = (TextView) convertView
					.findViewById(R.id.client_address);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.jobTypeName.setText(mJobTypesList.get(position).getmJobName());
		holder.client_address.setVisibility(View.GONE);

		holder.edit_jobType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog d = new Dialog(mContext);

				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.setContentView(R.layout.add_jobtype);
				d.show();
				d.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				d.setCanceledOnTouchOutside(false);
				@SuppressWarnings("static-access")
				Display display = ((WindowManager) mContext
						.getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				@SuppressWarnings("deprecation")
				int width = display.getWidth();
				@SuppressWarnings("deprecation")
				int height = display.getHeight();

				Log.v("width", width + "");
				// d.getWindow().setLayout((width), (int) (height));

				final EditText job_name = (EditText) d
						.findViewById(R.id.enter_jobName);

				final TextView title_text = (TextView) d
						.findViewById(R.id.title_text);

				final ImageButton backBtn = (ImageButton) d
						.findViewById(R.id.backBtn);

				title_text.setText("Edit Job Type");

				backBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						d.dismiss();
					}
				});

				job_name.setText(mJobTypesList.get(position).getmJobName());

				Button submit = (Button) d.findViewById(R.id.submit);

				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String jobName = job_name.getText().toString();

						if (!jobName.equalsIgnoreCase("")) {

							if (utils.isConnectingToInternet(mContext)) {
								new UpdateJobTypeTask(mJobTypesList.get(
										position).getId(), jobName).execute("");
							} else {
								utils.showDialog(mContext);
							}
						} else {
							Toast.makeText(mContext, "Enter Job Name",
									Toast.LENGTH_LONG).show();
						}

					}
				});
			}
		});

		holder.delete_jobType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mContext);

				// Setting Dialog Title
				alertDialog.setTitle("Delete "
						+ mJobTypesList.get(position).getmJobName());

				// Setting Dialog Message
				alertDialog.setMessage("Are you sure you want to delete "
						+ mJobTypesList.get(position).getmJobName() + "?");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.

								if (utils.isConnectingToInternet(mContext)) {
									new DeleteJobTypeTask(mJobTypesList.get(
											position).getId(), position)
											.execute("");
								} else {
									utils.showDialog(mContext);
								}

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("NO",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog
								dialog.cancel();
							}
						});

				// Showing Alert Message
				alertDialog.show();
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView jobTypeName;

		ImageButton edit_jobType;
		ImageButton delete_jobType;
		TextView client_address;

	}

	private class DeleteJobTypeTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String jobTypeId;
		int mPosition;

		public DeleteJobTypeTask(String jobType_id, int position) {
			jobTypeId = jobType_id;
			mPosition = position;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_JOBTYPE));
				entity.addPart(RequestParameters.JOBTYPE_ID, new StringBody(
						jobTypeId));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			JSONObject mResponseObject;
			try {
				mResponseObject = jsonResponse.getJSONObject("response");

				String success = mResponseObject.getString("success");
				String message = mResponseObject.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("JobType Deleted"))) {

					// deleting client from database
					db.open();
					db.deleteJobTypeForId(jobTypeId);
					db.close();

					mJobTypesList.remove(mPosition);
					notifyDataSetChanged();

					Toast.makeText(mContext, "JobType has been deleted",
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class UpdateJobTypeTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String jobTypeId;
		String jobName;

		public UpdateJobTypeTask(String jobType_id, String jobType_name) {
			jobTypeId = jobType_id;
			jobName = jobType_name;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.UPDATE_JOB_TYPE));
				entity.addPart(RequestParameters.JOBTYPE_ID, new StringBody(
						jobTypeId));
				entity.addPart(RequestParameters.JOB_NAME, new StringBody(
						jobName));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jobTypeJson = jsonResponse.getJSONObject("response");
				String success = jobTypeJson.getString("success");
				String message = jobTypeJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Job Type Updated"))) {
					JSONObject jobTypeDetails = jobTypeJson
							.getJSONObject("jobtype");
					String job_Type_Id = jobTypeDetails.getString("id");
					// String employer_Id =
					// clientDetails.getString("employerid");
					String job_Name = jobTypeDetails.getString("jobname");

					// String active = clientDetails.getString("active");

					db.open();
					db.updateJobTypeDetails(job_Type_Id, job_Name);
					db.close();

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(mContext,
							ManageJobTypesActivity.class); // your
															// class
					mContext.startActivity(i);

					((Activity) mContext).finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
