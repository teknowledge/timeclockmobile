package com.timeclockmobile;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class GetWorkingEmployeeLocation extends AsyncTask<String, Void, Void> {

	String mResponse = "";
	private Context mContext;

	JSONObject jsonResponse = new JSONObject();
	ProgressDialog progressDialog;
	private String mUserId;
	private String mClockIntime;

	public GetWorkingEmployeeLocation(Context context, String userId,
			String inTime) {
		this.mContext = context;
		progressDialog = new ProgressDialog(mContext);
		mUserId = userId;
		mClockIntime = inTime;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressDialog.setMessage("Please wait....");
		progressDialog.setCancelable(false);
		progressDialog.show();

	}

	@Override
	protected Void doInBackground(String... params) {
		//

		MultipartEntity entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);

		try {

			// *** Add The JSON to the Entity ********

			entity.addPart(RequestParameters.METHOD, new StringBody(
					RequestParameters.GET_USER_LOCATION));

			entity.addPart(RequestParameters.USER_ID, new StringBody(mUserId));

		} catch (UnsupportedEncodingException e) {

			mResponse = "exception";

			e.printStackTrace();

		} catch (Exception e) {
			mResponse = "exception";
			e.printStackTrace();
		}

		// *** fetching team counts from api ********
		try {

			jsonResponse = PostingDataToServer.getJSONFromServer(entity,
					ApplicationAPIs.BASE_URL);

			System.out.println(jsonResponse);

		} catch (Exception e) {
			mResponse = "exception";

			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		//
		super.onPostExecute(result);

		if (progressDialog != null && progressDialog.isShowing()) {
			try {
				progressDialog.dismiss();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			JSONObject clockOnObject = jsonResponse.getJSONObject("response");
			String success = clockOnObject.getString("success");

			if (success.equalsIgnoreCase("1")) {

				String lattitude = clockOnObject.getString("lat");
				String longitude = clockOnObject.getString("lng");

				Intent intent = new Intent(mContext,
						GoogleMapViewActivity.class);

				intent.putExtra("startLat", lattitude);
				intent.putExtra("startLng", longitude);

				intent.putExtra("clockOnTime", mClockIntime);

				intent.putExtra("from", "team_management");

				mContext.startActivity(intent);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
