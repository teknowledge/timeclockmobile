package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.data.Clients;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.service.RepeatService;
import com.timeclockmobile.service.TimerService;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class HomeActivity extends Activity {

	private TextView dateAndDay, user_Name;
	public static TextView timer;
	private TextView manage;
	private TextView filedNotes;
	private TextView timeSheets;
	private TextView settings, logout;
	private TextView job_name_edit, client_edit;
	private Button edit_clock_on;
	private LinearLayout edit_layout;
	private DataBaseHelper db = new DataBaseHelper(HomeActivity.this);
	public ProgressDialog progressDialog;

	private Button clock_on, clock_off, startBreak;

	private String mTodayDate;

	private boolean isClockedOn;

	String session = "LOGIN";
	private Utils utils = new Utils();

	private String workingInTime, workingInDate;
	private String numberOfTimeRecord = "1";

	private long timeDifferenceInMS;

	SettingsActivity activity = new SettingsActivity();

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.home_screen);

		progressDialog = new ProgressDialog(HomeActivity.this);

		user_Name = (TextView) findViewById(R.id.user_Name);

		timer = (TextView) findViewById(R.id.timer);

		dateAndDay = (TextView) findViewById(R.id.dateAndDay);

		clock_on = (Button) findViewById(R.id.clock_on);

		clock_off = (Button) findViewById(R.id.clock_off);

		startBreak = (Button) findViewById(R.id.startBreak);

		settings = (TextView) findViewById(R.id.settings);

		logout = (TextView) findViewById(R.id.logout);

		manage = (TextView) findViewById(R.id.manage);

		filedNotes = (TextView) findViewById(R.id.filedNotes);

		timeSheets = (TextView) findViewById(R.id.timeSheets);

		job_name_edit = (TextView) findViewById(R.id.job_name_edit);

		client_edit = (TextView) findViewById(R.id.client_edit);

		edit_clock_on = (Button) findViewById(R.id.edit_clock_on);

		edit_layout = (LinearLayout) findViewById(R.id.edit_layout);

		

		user_Name.setText("Welcome "
				+ Global.getInstance().getPreferenceVal(HomeActivity.this,
						CONSTANTS.USER_NAME) + "!");

		if (!Global.getInstance().getBooleanType(HomeActivity.this,
				RequestParameters.IS_EMPLOYER)) {
			manage.setVisibility(View.GONE);
		}

		final String login = Global.getInstance().getPreferenceVal(
				HomeActivity.this, RequestParameters.SESSION);
		if (session.equals(login)) {

			db.open();
			db.deleteClients();
			db.deleteJobTypes();
			db.deleteEmployees();
			db.deleteWorkingEmployees();
			db.close();

			if (utils.isConnectingToInternet(HomeActivity.this)) {
				new LoadLoginUserInfo().execute("");
			} else {
				utils.showDialog(HomeActivity.this);
			}
		}

		timer.setText("00:00:00");

		isClockedOn = Global.getInstance().getBooleanType(HomeActivity.this,
				CONSTANTS.CLOCKED_ON);

		if (isClockedOn) {
			clock_on.setBackgroundColor(Color.parseColor("#AAAAAA"));
			clock_off.setBackgroundColor(Color.parseColor("#56A89A"));
			startBreak.setBackgroundColor(Color.parseColor("#56A89A"));
			edit_layout.setVisibility(View.VISIBLE);
			job_name_edit.setText(Global.getInstance().getPreferenceVal(
					HomeActivity.this, CONSTANTS.JOB_NAME));
			client_edit.setText(Global.getInstance().getPreferenceVal(
					HomeActivity.this, CONSTANTS.CLIENT_NAME));

		} else {
			clock_on.setBackgroundColor(Color.parseColor("#56A89A"));
			clock_off.setBackgroundColor(Color.parseColor("#AAAAAA"));
			startBreak.setBackgroundColor(Color.parseColor("#AAAAAA"));
			edit_layout.setVisibility(View.GONE);
		}

		if (Global.getInstance().getBooleanType(HomeActivity.this,
				CONSTANTS.START_BREAK_VALUE)) {
			startedBreak(true);

		} else {
			startedBreak(false);
		}

		if (Global.getInstance()
				.getPreferenceVal(HomeActivity.this, CONSTANTS.DATE_FORMAT)
				.equalsIgnoreCase("DD/MM/YYYY")) {
			mTodayDate = new SimpleDateFormat("dd/MM/yyyy, EEEE")
					.format(new Date());
		} else {

			mTodayDate = new SimpleDateFormat("MM/dd/yyyy, EEEE")
					.format(new Date());
		}

		// mTodayDate = new SimpleDateFormat("yyyy-MM-dd EEEE").format(new
		// Date());

		dateAndDay.setText(mTodayDate);

		if (Global.getInstance().getBooleanType(HomeActivity.this,
				CONSTANTS.ENABLE_NOTIFICATION)) {
			activity.showNotification(HomeActivity.this);
		}

		clock_on.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isClockedOn) {
					Toast.makeText(HomeActivity.this,
							"Sorry! You are already clocked on ",
							Toast.LENGTH_SHORT).show();
				} else {
					Intent intent = new Intent(HomeActivity.this,
							SelectClientsActivity.class);

					intent.putExtra("clock_type", "clock_on");
					startActivity(intent);

				}

			}
		});
		clock_off.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isClockedOn) {
					if (!Global.getInstance().getBooleanType(HomeActivity.this,
							CONSTANTS.START_BREAK_VALUE)) {
						Intent intent = new Intent(HomeActivity.this,
								ClockOffActivity.class);
						startActivity(intent);
					} else {
						Toast.makeText(HomeActivity.this,
								"Please finish the break ", Toast.LENGTH_SHORT)
								.show();
					}

				} else {
					Toast.makeText(HomeActivity.this,
							"Sorry! You are not yet clocked on ",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		edit_clock_on.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isClockedOn) {
					Intent intent = new Intent(HomeActivity.this,
							SelectClientsActivity.class);
					intent.putExtra("clock_type", "edit_clock_on");

					startActivity(intent);

				} else {
					Toast.makeText(HomeActivity.this,
							"Sorry! You are not yet clocked on ",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		startBreak.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// if (isClockedOn) {
				// Intent intent = new Intent(HomeActivity.this,
				// SelectClientsActivity.class);
				// intent.putExtra("clock_type", "switch_location");
				//
				// startActivity(intent);
				//
				// } else {
				// Toast.makeText(HomeActivity.this,
				// "Sorry! You are not yet clocked on ",
				// Toast.LENGTH_SHORT).show();
				// }

				if (isClockedOn) {
					if (!Global.getInstance().getBooleanType(HomeActivity.this,
							CONSTANTS.START_BREAK_VALUE)) {
						AlertDialog.Builder alertDialog = new AlertDialog.Builder(
								HomeActivity.this);

						// Setting Dialog Title
						alertDialog.setTitle("Start Break");

						// Setting Dialog Message
						alertDialog
								.setMessage("Please confirm that you are taking a break");

						// Setting Positive "Yes" Button
						alertDialog.setPositiveButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// User pressed YES button.
										// delete employee

										dialog.cancel();

									}
								});

						// Setting Negative "NO" Button
						alertDialog.setNegativeButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// User pressed No button.Cancel the
										// dialog
										if (utils
												.isConnectingToInternet(HomeActivity.this)) {
											new StartBreakTask().execute("");
										} else {
											utils.showDialog(HomeActivity.this);
										}
									}
								});

						// Showing Alert Message
						alertDialog.show();
					} else {
						AlertDialog.Builder alertDialog = new AlertDialog.Builder(
								HomeActivity.this);

						// Setting Dialog Title
						alertDialog.setTitle("End Break");

						// Setting Dialog Message
						alertDialog
								.setMessage("Please confirm that you are ending a break");

						// Setting Positive "Yes" Button
						alertDialog.setPositiveButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// User pressed YES button.
										// delete employee

										dialog.cancel();

									}
								});

						// Setting Negative "NO" Button
						alertDialog.setNegativeButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// User pressed No button.Cancel the
										// dialog
										if (utils
												.isConnectingToInternet(HomeActivity.this)) {
											new EndBreakTask().execute("");
										} else {
											utils.showDialog(HomeActivity.this);
										}
									}
								});

						// Showing Alert Message
						alertDialog.show();
					}

				} else {
					Toast.makeText(HomeActivity.this,
							"Sorry! You are not yet clocked on ",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		manage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						ManageActivty.class);
				startActivity(intent);
			}
		});

		filedNotes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						FieldNotesActivity.class);
				intent.putExtra("from", "home");
				startActivity(intent);
			}
		});

		timeSheets.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/****** getting all the time records **********/
				Intent intent = new Intent(HomeActivity.this,
						TimeRecordsActivity.class);
				startActivity(intent);
				// new GetAllTimeRecords().execute("");

			}
		});

		settings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						SettingsActivity.class);
				startActivity(intent);
				finish();
			}
		});

		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						HomeActivity.this);

				// Setting Dialog Title
				alertDialog.setTitle("Logout ");

				// Setting Dialog Message
				alertDialog.setMessage("Are you sure you want to logout?");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.
								// delete employee

								dialog.cancel();

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog

								Global.getInstance().storeIntoPreference(
										HomeActivity.this,
										RequestParameters.SESSION, "LOGOUT");

								activity.cancelNotification(0,
										HomeActivity.this);

								// stop timer service
								stopService(new Intent(HomeActivity.this,
										TimerService.class));
								
								// stop updating location service
								stopService(new Intent(HomeActivity.this,
										RepeatService.class));

								// Global.getInstance().storeBooleanType(
								// HomeActivity.this,
								// CONSTANTS.USER_IS_WORKING, false);

								Global.getInstance().storeBooleanType(
										HomeActivity.this,
										CONSTANTS.USER_IS_WORKING, false);

								Global.getInstance().storeBooleanType(
										HomeActivity.this,
										CONSTANTS.CLOCKED_ON, false);

								Global.getInstance().storeBooleanType(
										HomeActivity.this,
										CONSTANTS.START_BREAK_VALUE, false);

								Intent intent = new Intent(HomeActivity.this,
										MainActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
										| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
								startActivity(intent);
								finish();

							}
						});

				// Showing Alert Message
				alertDialog.show();

			}
		});

	}

	
	public void showTimerForClockedOn() {
		if (Global.getInstance().getBooleanType(HomeActivity.this,
				CONSTANTS.USER_IS_WORKING)) {

			workingInTime = Global.getInstance().getPreferenceVal(
					HomeActivity.this, CONSTANTS.USER_IN_TIME);

			workingInDate = Global.getInstance().getPreferenceVal(
					HomeActivity.this, CONSTANTS.USER_IN_DATE);

			timeDifferenceInMS = utils.convertDateIntoMilliseconds(
					workingInDate, workingInTime);

			Global.getInstance().storeIntoPreference(HomeActivity.this,
					CONSTANTS.CLOCKED_ON_TIME,
					String.valueOf(timeDifferenceInMS));

			startService(new Intent(HomeActivity.this, TimerService.class));

			Global.getInstance().storeBooleanType(HomeActivity.this,
					CONSTANTS.CLOCKED_ON, true);

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		finishAffinity();
	}

	public void startedBreak(boolean started) {
		if (started) {

			startBreak.setText("End Break");

			clock_off.setBackgroundColor(Color.parseColor("#AAAAAA"));

		} else {

			startBreak.setText("Start Break");

			if (isClockedOn) {
				clock_off.setBackgroundColor(Color.parseColor("#56A89A"));
			}
		}
	}

	private class StartBreakTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.START_BREAK));
				entity.addPart(RequestParameters.NUMBER_OF_TIME_RECORD,
						new StringBody(numberOfTimeRecord));

				if (Global.getInstance().getBooleanType(HomeActivity.this,
						CONSTANTS.USER_IS_WORKING)) {
					entity.addPart(
							RequestParameters.TIME_RECORD_ID + "1",
							new StringBody(Global.getInstance()
									.getPreferenceVal(HomeActivity.this,
											CONSTANTS.CLOCKED_ON_TRID)));
				} else {
					entity.addPart(
							RequestParameters.TIME_RECORD_ID + "1",
							new StringBody(Global.getInstance()
									.getPreferenceVal(HomeActivity.this,
											RequestParameters.TIME_RECORD_ID)));
				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			JSONObject clockOnObject;
			try {
				clockOnObject = jsonResponse.getJSONObject("response");
				String success = clockOnObject.getString("success");
				if (success.equalsIgnoreCase("1")) {

					startedBreak(true);

					JSONArray timeRecordsArray = clockOnObject
							.getJSONArray("timerecords");

					String outtime;
					String outdate;

					for (int i = 0; i < timeRecordsArray.length(); i++) {
						JSONObject timeRecord = timeRecordsArray
								.getJSONObject(i);

						String jobtypeid = timeRecord.getString("jobtypeid");
						String jobname = timeRecord.getString("jobname");
						String userid = timeRecord.getString("userid");

						String password = timeRecord.getString("password");
						String clientname = timeRecord.getString("clientname");
						String online = timeRecord.getString("online");

						String clientid = timeRecord.getString("clientid");

						String id = timeRecord.getString("id");

						String createdGMT = timeRecord.getString("created");
						String created = utils
								.convertGMTtoLocalTime(createdGMT);

						String name = timeRecord.getString("name");

						String workingEmployeeInTimeGMT = timeRecord
								.getString("intime");
						String workingEmployeeInDateGMT = timeRecord
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String intime = IntimeAndDate[1];
						String indate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = timeRecord
								.getString("outtime");
						String workingEmployeeOutDateGMT = timeRecord
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							outtime = outTimeAndDate[1];

							outdate = outTimeAndDate[0];
						} else {
							outtime = workingEmployeeOutTimeGMT;

							outdate = workingEmployeeOutDateGMT;
						}

						String phoneno = timeRecord.getString("phoneno");
						String endlng = timeRecord.getString("endlng");

						String userhourlyrate = timeRecord
								.getString("userhourlyrate");
						String endlat = timeRecord.getString("endlat");
						String status = timeRecord.getString("status");
						String startlat = timeRecord.getString("startlat");
						String devicetoken = timeRecord
								.getString("devicetoken");
						String startlng = timeRecord.getString("startlng");
						String address = timeRecord.getString("address");
						String email = timeRecord.getString("email");
						String active = timeRecord.getString("active");
						String clientaddress = timeRecord
								.getString("clientaddress");

						db.open();
						db.updateEmployeeDetailsForWorking(userid, "1", id,
								clientid, jobtypeid, intime, indate, startlat,
								startlng, clientname, created, endlat, endlng,
								jobname, outtime, outdate, "1");
						db.close();

						Global.getInstance().storeBooleanType(
								HomeActivity.this, CONSTANTS.START_BREAK_VALUE,
								true);

					}

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class EndBreakTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.END_BREAK));
				entity.addPart(RequestParameters.NUMBER_OF_TIME_RECORD,
						new StringBody(numberOfTimeRecord));

				if (Global.getInstance().getBooleanType(HomeActivity.this,
						CONSTANTS.USER_IS_WORKING)) {
					entity.addPart(
							RequestParameters.TIME_RECORD_ID + "1",
							new StringBody(Global.getInstance()
									.getPreferenceVal(HomeActivity.this,
											CONSTANTS.CLOCKED_ON_TRID)));
				} else {
					entity.addPart(
							RequestParameters.TIME_RECORD_ID + "1",
							new StringBody(Global.getInstance()
									.getPreferenceVal(HomeActivity.this,
											RequestParameters.TIME_RECORD_ID)));
				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			JSONObject clockOnObject;
			try {
				clockOnObject = jsonResponse.getJSONObject("response");
				String success = clockOnObject.getString("success");
				if (success.equalsIgnoreCase("1")) {

					startedBreak(false);

					JSONArray timeRecordsArray = clockOnObject
							.getJSONArray("timerecords");

					String outtime;
					String outdate;

					for (int i = 0; i < timeRecordsArray.length(); i++) {
						JSONObject timeRecord = timeRecordsArray
								.getJSONObject(i);

						String jobtypeid = timeRecord.getString("jobtypeid");
						String jobname = timeRecord.getString("jobname");
						String userid = timeRecord.getString("userid");

						String password = timeRecord.getString("password");
						String clientname = timeRecord.getString("clientname");
						String online = timeRecord.getString("online");

						String clientid = timeRecord.getString("clientid");

						String id = timeRecord.getString("id");

						String createdGMT = timeRecord.getString("created");
						String created = utils
								.convertGMTtoLocalTime(createdGMT);

						String name = timeRecord.getString("name");

						String workingEmployeeInTimeGMT = timeRecord
								.getString("intime");
						String workingEmployeeInDateGMT = timeRecord
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String intime = IntimeAndDate[1];
						String indate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = timeRecord
								.getString("outtime");
						String workingEmployeeOutDateGMT = timeRecord
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							outtime = outTimeAndDate[1];

							outdate = outTimeAndDate[0];
						} else {
							outtime = workingEmployeeOutTimeGMT;

							outdate = workingEmployeeOutDateGMT;
						}

						String phoneno = timeRecord.getString("phoneno");
						String endlng = timeRecord.getString("endlng");

						String userhourlyrate = timeRecord
								.getString("userhourlyrate");
						String endlat = timeRecord.getString("endlat");
						String status = timeRecord.getString("status");
						String startlat = timeRecord.getString("startlat");
						String devicetoken = timeRecord
								.getString("devicetoken");
						String startlng = timeRecord.getString("startlng");
						String address = timeRecord.getString("address");
						String email = timeRecord.getString("email");
						String active = timeRecord.getString("active");
						String clientaddress = timeRecord
								.getString("clientaddress");

						db.open();
						db.updateEmployeeDetailsForWorking(userid, "1", id,
								clientid, jobtypeid, intime, indate, startlat,
								startlng, clientname, created, endlat, endlng,
								jobname, outtime, outdate, "0");
						db.close();

						Global.getInstance().storeBooleanType(
								HomeActivity.this, CONSTANTS.START_BREAK_VALUE,
								false);

					}

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class LoadLoginUserInfo extends AsyncTask<String, Void, Void> {

		String response = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.LOAD_USER_INFO));

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								HomeActivity.this, RequestParameters.USER_ID)));

			} catch (UnsupportedEncodingException e) {

				response = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				response = "exception";
				e.printStackTrace();
			}

			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

				// System.out.println(response + "value text");

			} catch (Exception e) {
				response = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				System.out.println(jsonObject);

				String message = jsonObject.getString("message");
				System.out.println(message);

				if (message.equalsIgnoreCase(RequestParameters.USER_EXISTS)) {

					// ******* user information with the details *******//

					JSONObject user = jsonObject.getJSONObject("user");
					System.out.println(user);

					String user_id = user.getString("id");
					String name = user.getString("name");

					String email = user.getString("email");
					String address = user.getString("address");
					String password = user.getString("password");
					String phoneno = user.getString("phoneno");
					String profileimage = user.getString("profileimage");
					String hourlyrate = user.getString("hourlyrate");
					String devicetoken = user.getString("devicetoken");
					String isemployer = user.getString("isemployer");
					String employerid = user.getString("employerid");
					String online = user.getString("online");
					String status = user.getString("status");
					String created = user.getString("created");
					String lastlogin = user.getString("lastlogin");

					utils.convertGMTtoLocalTime(lastlogin);

					Global.getInstance().storeIntoPreference(HomeActivity.this,
							RequestParameters.USER_ID, user_id);
					Global.getInstance().storeIntoPreference(HomeActivity.this,
							CONSTANTS.USER_NAME, name);
					Global.getInstance().storeIntoPreference(HomeActivity.this,
							RequestParameters.SESSION, "LOGIN");

					if (isemployer.equalsIgnoreCase("1")) {
						Global.getInstance().storeBooleanType(
								HomeActivity.this,
								RequestParameters.IS_EMPLOYER, true);
						Global.getInstance().storeIntoPreference(
								HomeActivity.this,
								RequestParameters.EMPLOYER_ID, user_id);
						Global.getInstance().storeIntoPreference(
								HomeActivity.this,
								RequestParameters.EMPLOYER_NAME, name);
					} else {
						Global.getInstance().storeBooleanType(
								HomeActivity.this,
								RequestParameters.IS_EMPLOYER, false);
						Global.getInstance().storeIntoPreference(
								HomeActivity.this, RequestParameters.ADMIN_ID,
								employerid);
					}
					//
					// if (isemployer.equalsIgnoreCase("1")) {
					// Global.getInstance().storeIntoPreference(
					// HomeActivity.this,
					// RequestParameters.EMPLOYER_ID, user_id);
					// Global.getInstance().storeIntoPreference(
					// HomeActivity.this,
					// RequestParameters.EMPLOYER_NAME, name);

					// ****** clients array with their details*******//

					JSONArray clientsArray = jsonObject.getJSONArray("clients");
					System.out.println(clientsArray);

					for (int i = 0; i < clientsArray.length(); i++) {
						JSONObject clientObject = clientsArray.getJSONObject(i);

						String clientId = clientObject.getString("id");
						String employerId = clientObject
								.getString("employerid");
						String clientName = clientObject
								.getString("clientname");
						String clientAddress = clientObject
								.getString("address");
						String clientActive = clientObject.getString("active");

						Clients client = new Clients();
						client.setmEmployerId(employerId);
						client.setmClientName(clientName);
						client.setmClientAddress(clientAddress);
						client.setmActive(clientActive);

						// ********** saving clients and details in clients
						// table in
						// db****************//

						db.open();
						db.insertClients(clientId, employerId, clientName,
								clientAddress, clientActive);
						db.close();
					}

					// ******* Job types with their details *******//
					JSONArray jobTypesArray = jsonObject
							.getJSONArray("jobtypes");
					System.out.println(jobTypesArray);

					for (int i = 0; i < jobTypesArray.length(); i++) {
						JSONObject jobTypesObject = jobTypesArray
								.getJSONObject(i);

						String jobTypeId = jobTypesObject.getString("id");
						String jobTypeEmployerId = jobTypesObject
								.getString("employerid");
						String jobTypeName = jobTypesObject
								.getString("jobname");

						String jobTypeActive = jobTypesObject
								.getString("active");

						db.open();
						db.insertJobTypes(jobTypeId, jobTypeEmployerId,
								jobTypeName, jobTypeActive);
						db.close();

					}

					// ********* employees with their details ********//

					JSONArray employeesArray = jsonObject
							.getJSONArray("employees");
					System.out.println(employeesArray);

					for (int i = 0; i < employeesArray.length(); i++) {
						JSONObject employeesObject = employeesArray
								.getJSONObject(i);

						String employeeId = employeesObject.getString("id");
						String employeeName = employeesObject.getString("name");

						String employeeEmail = employeesObject
								.getString("email");
						String employeeAddress = employeesObject
								.getString("address");
						String employeePassword = employeesObject
								.getString("password");
						String employeePhoneNo = employeesObject
								.getString("phoneno");
						String employeeProfileImage = employeesObject
								.getString("profileimage");
						String employeeHourlyRate = employeesObject
								.getString("hourlyrate");
						String employeeDeviceToken = employeesObject
								.getString("devicetoken");
						String employeeIsEmployer = employeesObject
								.getString("isemployer");
						String employerId = employeesObject
								.getString("employerid");
						String employeeOnline = employeesObject
								.getString("online");
						String employeeStatus = employeesObject
								.getString("status");

						String employeeCreatedGMT = employeesObject
								.getString("created");
						String employeeCreated = utils
								.convertGMTtoLocalTime(employeeCreatedGMT);

						String employeeLastLoginGMT = employeesObject
								.getString("lastlogin");

						String employeeLastLogin = utils
								.convertGMTtoLocalTime(employeeLastLoginGMT);

						db.open();
						db.insertEmployees(employeeId, employeeName,
								employeeEmail, employeeAddress,
								employeePassword, employeePhoneNo,
								employeeProfileImage, employeeHourlyRate,
								employeeDeviceToken, employeeIsEmployer,
								employerId, employeeOnline, employeeStatus,
								employeeCreated, employeeLastLogin, "0", "",
								"", "", "", "", "", "", "", "", "", "", "",
								"null", "null", "0");
						db.close();
					}

					// ******* WorkingEmployee with their client
					// details********//

					JSONArray workingEmployeesArray = jsonObject
							.getJSONArray("workingemployees");
					System.out.println(workingEmployeesArray);
					String workingEmployeeOutTime;
					String workingEmployeeOutDate;
					String isBreakStarted = "0";

					// db.open();
					// db.deleteDetailsForClockOn();
					// db.close();

					for (int i = 0; i < workingEmployeesArray.length(); i++) {
						JSONObject workingEmployeesObject = workingEmployeesArray
								.getJSONObject(i);

						String workingEmployeeName = workingEmployeesObject
								.getString("name");

						String workingEmployeeEmail = workingEmployeesObject
								.getString("email");
						String workingEmployeeAddress = workingEmployeesObject
								.getString("address");
						String workingEmployeePassword = workingEmployeesObject
								.getString("password");
						String workingEmployeePhoneno = workingEmployeesObject
								.getString("phoneno");
						String workingEmployeeHourlyrate = workingEmployeesObject
								.getString("userhourlyrate");
						String workingEmployeeDevicetoken = workingEmployeesObject
								.getString("devicetoken");
						String workingEmployeeOnline = workingEmployeesObject
								.getString("online");
						String workingEmployeeStatus = workingEmployeesObject
								.getString("status");
						String workingEmployeeId = workingEmployeesObject
								.getString("id");
						String workingEmployeeUserId = workingEmployeesObject
								.getString("userid");
						String workingEmployeeClientId = workingEmployeesObject
								.getString("clientid");
						String workingEmployeeJobTypeId = workingEmployeesObject
								.getString("jobtypeid");

						String workingEmployeeInTimeGMT = workingEmployeesObject
								.getString("intime");
						String workingEmployeeInDateGMT = workingEmployeesObject
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String workingEmployeeInTime = IntimeAndDate[1];
						String workingEmployeeInDate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = workingEmployeesObject
								.getString("outtime");
						String workingEmployeeOutDateGMT = workingEmployeesObject
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							workingEmployeeOutTime = outTimeAndDate[1];

							workingEmployeeOutDate = outTimeAndDate[0];
						} else {
							workingEmployeeOutTime = workingEmployeeOutTimeGMT;

							workingEmployeeOutDate = workingEmployeeOutDateGMT;
						}

						String workingEmployeeStartLat = workingEmployeesObject
								.getString("startlat");
						String workingEmployeeStartLng = workingEmployeesObject
								.getString("startlng");
						String workingEmployeeEndLat = workingEmployeesObject
								.getString("endlat");
						String workingEmployeeEndLng = workingEmployeesObject
								.getString("endlng");
						String workingEmployeeActive = workingEmployeesObject
								.getString("active");

						String workingEmployeeCreatedGMT = workingEmployeesObject
								.getString("created");

						String workingEmployeeCreated = utils
								.convertGMTtoLocalTime(workingEmployeeCreatedGMT);

						String workingEmployeeClientName = workingEmployeesObject
								.getString("clientname");
						String workingEmployeeClientAddress = workingEmployeesObject
								.getString("clientaddress");
						String workingEmployeeJobName = workingEmployeesObject
								.getString("jobname");

						JSONArray breaksArray = workingEmployeesObject
								.getJSONArray("breaks");

						if (breaksArray.length() == 0) {
							isBreakStarted = "0";
						} else {
							for (int x = 0; x < breaksArray.length(); x++) {
								JSONObject break_ts = breaksArray
										.getJSONObject(x);

								String break_tr_id = break_ts.getString("trid");
								// String start_break_timeGMT = break_ts
								// .getString("startbreaktime");
								// String start_break_dateGMT = break_ts
								// .getString("startbreakdate");
								String end_break_timeGMT = break_ts
										.getString("endbreaktime");
								String end_break_dateGMT = break_ts
										.getString("endbreakdate");

								if (end_break_timeGMT.equalsIgnoreCase("null")
										&& end_break_dateGMT
												.equalsIgnoreCase("null")) {
									isBreakStarted = "1";
								}else{
									isBreakStarted = "0";
								}
							}
						}

						/************** to check the user that currently working or not **************/
						if (Global
								.getInstance()
								.getPreferenceVal(HomeActivity.this,
										RequestParameters.USER_ID)
								.equalsIgnoreCase(workingEmployeeUserId)) {
							if (workingEmployeeOutTime.equalsIgnoreCase("null")) {

								if (!workingEmployeeInTime.equalsIgnoreCase("")) {
									Global.getInstance().storeBooleanType(
											HomeActivity.this,
											CONSTANTS.USER_IS_WORKING, true);

									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.USER_IN_TIME,
											workingEmployeeInTime);
									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.USER_IN_DATE,
											workingEmployeeInDate);

									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.CLOCKED_ON_JOBTYPEID,
											workingEmployeeJobTypeId);
									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.CLOCKED_ON_CLIENTID,
											workingEmployeeClientId);
									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.CLOCKED_ON_JOBNAME,
											workingEmployeeJobName);
									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.CLOCKED_ON_CLIENTNAME,
											workingEmployeeClientName);
									Global.getInstance().storeIntoPreference(
											HomeActivity.this,
											CONSTANTS.CLOCKED_ON_TRID,
											workingEmployeeId);

									Global.getInstance().storeBooleanType(
											HomeActivity.this,
											CONSTANTS.CLOCKED_ON, true);

								}

							}
						}

						// inserting working employees in working employees
						// table
						db.open();
						db.insertWorkingEmployees(workingEmployeeName,
								workingEmployeeEmail, workingEmployeeAddress,
								workingEmployeePassword,
								workingEmployeePhoneno,
								workingEmployeeHourlyrate,
								workingEmployeeDevicetoken,
								workingEmployeeOnline, workingEmployeeStatus,
								workingEmployeeId, workingEmployeeUserId,
								workingEmployeeClientId,
								workingEmployeeJobTypeId,
								workingEmployeeInTime, workingEmployeeInDate,
								workingEmployeeOutTime, workingEmployeeOutDate,
								workingEmployeeStartLat,
								workingEmployeeStartLng, workingEmployeeEndLat,
								workingEmployeeEndLng, workingEmployeeActive,
								workingEmployeeCreated,
								workingEmployeeClientName,
								workingEmployeeClientAddress,
								workingEmployeeJobName);
						db.close();

						// updating employees table with the working
						// inforamtion..

						if (workingEmployeeOutTime.equalsIgnoreCase("null")) {
							if (!workingEmployeeInTime.equalsIgnoreCase("")) {
								db.open();
								db.updateEmployeeDetailsForWorking(
										workingEmployeeUserId, "1",
										workingEmployeeId,
										workingEmployeeClientId,
										workingEmployeeJobTypeId,
										workingEmployeeInTime,
										workingEmployeeInDate,
										workingEmployeeStartLat,
										workingEmployeeStartLng,
										workingEmployeeClientName,
										workingEmployeeCreated,
										workingEmployeeEndLat,
										workingEmployeeEndLng,
										workingEmployeeJobName,
										workingEmployeeOutTime,
										workingEmployeeOutDate, isBreakStarted);
								db.close();
							}
						}

					}

				}

				showTimerForClockedOn();
				// }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
