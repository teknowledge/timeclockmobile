package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class EditClockOnActivity extends Activity {

	private TextView employeeName, clientName, jobTypeName;
	private Button submit_switchLocation;
	public ProgressDialog progressDialog;
	private String mClientId, mJobTypeId;

	GpsTracker gpsTracker;
	String latitude;
	String longitude;

	Utils utils = new Utils();

	private ImageButton homeBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.switch_location);
		progressDialog = new ProgressDialog(EditClockOnActivity.this);
		employeeName = (TextView) findViewById(R.id.employee_name);
		clientName = (TextView) findViewById(R.id.client_name);
		jobTypeName = (TextView) findViewById(R.id.job_type_name);
		submit_switchLocation = (Button) findViewById(R.id.submit_switch_location);
		homeBtn = (ImageButton) findViewById(R.id.homeBtn);

		gpsTracker = new GpsTracker(this);

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(this);
		}

		employeeName.setText(Global.getInstance().getPreferenceVal(
				EditClockOnActivity.this, CONSTANTS.USER_NAME));
		clientName.setText(Global.getInstance().getPreferenceVal(
				EditClockOnActivity.this, CONSTANTS.CLIENT_NAME));
		jobTypeName.setText(Global.getInstance().getPreferenceVal(
				EditClockOnActivity.this, CONSTANTS.JOB_NAME));

		Intent intent = getIntent();

		mClientId = intent.getStringExtra("client_id");
		mJobTypeId = intent.getStringExtra("job_type_id");

		submit_switchLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (utils.isConnectingToInternet(EditClockOnActivity.this)) {
					new EditClockOnTask().execute("");
				} else {
					utils.showDialog(EditClockOnActivity.this);
				}
			}
		});

		homeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(EditClockOnActivity.this,
						HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
			}
		});

	}

	/******************
	 * this api switch location for selectionn of employee for switching the
	 * location and creates time record
	 **********************/

	private class EditClockOnTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.EDIT_CLOCK_ON));
				entity.addPart(
						RequestParameters.TIME_RECORD_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								EditClockOnActivity.this,
								RequestParameters.TIME_RECORD_ID)));
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								EditClockOnActivity.this,
								RequestParameters.USER_ID)));
				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));
				entity.addPart(RequestParameters.JOBTYPE_ID, new StringBody(
						mJobTypeId));

				entity.addPart(RequestParameters.END_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.END_LNG, new StringBody(
						longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject clockOnObject = jsonResponse
						.getJSONObject("response");
				String success = clockOnObject.getString("success");
				String message = clockOnObject.getString("message");
				if ((success.equalsIgnoreCase("1"))
						&& (success.equalsIgnoreCase("1"))) {
					JSONObject timeRecordObject = clockOnObject
							.getJSONObject("timerecord");

					String employee_name = timeRecordObject.getString("name");
					String client_name = timeRecordObject
							.getString("clientname");
					String job_name = timeRecordObject.getString("jobname");

					String time_record_id = timeRecordObject.getString("id");

					Global.getInstance().storeIntoPreference(
							EditClockOnActivity.this,
							RequestParameters.TIME_RECORD_ID, time_record_id);
					Global.getInstance().storeIntoPreference(
							EditClockOnActivity.this,
							RequestParameters.EMPLOYEE_NAME, employee_name);
					Global.getInstance().storeIntoPreference(
							EditClockOnActivity.this,
							RequestParameters.CLIENT_NAME, client_name);
					Global.getInstance().storeIntoPreference(
							EditClockOnActivity.this,
							RequestParameters.JOB_NAME, job_name);

					Intent intent = new Intent(EditClockOnActivity.this,
							HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
