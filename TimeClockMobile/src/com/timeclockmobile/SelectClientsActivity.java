package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.timeclockmobile.adapter.SelectClientsAdapter;
import com.timeclockmobile.data.Clients;
import com.timeclockmobile.data.ClockedOnUser;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class SelectClientsActivity extends Activity {

	private ListView clients_listView;
	private ImageButton backBtn, add_client_btn;

	private ArrayList<Clients> clientsArrayList = new ArrayList<Clients>();
	public ProgressDialog progressDialog;
	private SelectClientsAdapter adapter;
	private ManageClientsActivity manageClientsActivity = new ManageClientsActivity();
	private ArrayList<Employees> checkedEmployeesList;
	private String mClockOnType;
	private ArrayList<ClockedOnUser> clockedOnUsersList;
	private Utils utils = new Utils();
	private DataBaseHelper db = new DataBaseHelper(SelectClientsActivity.this);

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.select_clients_activity);

		progressDialog = new ProgressDialog(SelectClientsActivity.this);

		Intent intent = getIntent();
		mClockOnType = intent.getStringExtra("clock_type");
		if (mClockOnType.equalsIgnoreCase("massive_clock_on")) {
			checkedEmployeesList = (ArrayList<Employees>) intent
					.getSerializableExtra("checkedEmployeesArray");
		}
		if (mClockOnType.equalsIgnoreCase("massive_switch_location")) {
			clockedOnUsersList = (ArrayList<ClockedOnUser>) intent
					.getSerializableExtra("ClockedOnUsersList");
		}

		clients_listView = (ListView) findViewById(R.id.clients_list);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		add_client_btn = (ImageButton) findViewById(R.id.add_client_btn);

		add_client_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// ***** custom dialog for adding client ********//

				final Dialog d = new Dialog(SelectClientsActivity.this);

				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.setContentView(R.layout.add_client);
				d.show();
				d.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				d.setCanceledOnTouchOutside(false);
				@SuppressWarnings("static-access")
				Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				@SuppressWarnings("deprecation")
				int width = display.getWidth();
				@SuppressWarnings("deprecation")
				int height = display.getHeight();

				Log.v("width", width + "");
				// d.getWindow().setLayout((width), (int) (height));

				final EditText client_name = (EditText) d
						.findViewById(R.id.enter_clientName);
				final EditText client_address = (EditText) d
						.findViewById(R.id.enter_clientAddress);
				final ImageButton backBtn = (ImageButton) d
						.findViewById(R.id.backBtn);

				backBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						d.dismiss();
					}
				});

				Button submit = (Button) d.findViewById(R.id.submit);

				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String clientName = client_name.getText().toString();
						String clientAddress = client_address.getText()
								.toString();

						if (!clientName.equalsIgnoreCase("")) {
							if (!clientAddress.equalsIgnoreCase("")) {

								// ******* adding client to the employer
								// ******//

								if (utils
										.isConnectingToInternet(SelectClientsActivity.this)) {
									new AddClientTask(clientName, clientAddress)
											.execute("");
								} else {
									utils.showDialog(SelectClientsActivity.this);
								}

							} else {
								Toast.makeText(getApplicationContext(),
										"Enter Client Address",
										Toast.LENGTH_LONG).show();
							}
						} else {
							Toast.makeText(getApplicationContext(),
									"Enter Client Name", Toast.LENGTH_LONG)
									.show();
						}

					}
				});

			}
		});

		clientsArrayList = manageClientsActivity
				.getClientsFromDB(SelectClientsActivity.this);

		adapter = new SelectClientsAdapter(SelectClientsActivity.this,
				R.layout.select_clients_inflate, clientsArrayList);

		clients_listView.setAdapter(adapter);

		clients_listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				Clients client = (Clients) adapter.getItem(position);

				String clientId = client.getmClientId();

				String clientName = client.getmClientName();

				Global.getInstance().storeIntoPreference(
						SelectClientsActivity.this, CONSTANTS.CLIENT_NAME,
						client.getmClientName());

				if (mClockOnType.equalsIgnoreCase("massive_clock_on")) {

					Intent intent = new Intent(SelectClientsActivity.this,
							SelectJobTypesActivity.class);
					intent.putExtra("client_id", clientId);
					intent.putExtra("clientName", clientName);

					intent.putExtra("clock_on", "massive_clock_on");
					intent.putExtra("checkedEmployeesArray",
							checkedEmployeesList);
					startActivity(intent);

				} else if (mClockOnType.equalsIgnoreCase("clock_on")) {

					Intent intent = new Intent(SelectClientsActivity.this,
							SelectJobTypesActivity.class);
					intent.putExtra("clock_on", "clock_on");
					intent.putExtra("client_id", clientId);
					startActivity(intent);

				} else if (mClockOnType.equalsIgnoreCase("edit_clock_on")) {

					Intent intent = new Intent(SelectClientsActivity.this,
							SelectJobTypesActivity.class);
					intent.putExtra("clock_on", "edit_clock_on");
					intent.putExtra("client_id", clientId);
					startActivity(intent);

				} else if (mClockOnType
						.equalsIgnoreCase("massive_switch_location")) {
					Intent intent = new Intent(SelectClientsActivity.this,
							SelectJobTypesActivity.class);
					intent.putExtra("clock_on", "massive_switch_location");
					intent.putExtra("clockedOnUsersList", clockedOnUsersList);
					intent.putExtra("client_id", clientId);
					intent.putExtra("clientName", clientName);
					startActivity(intent);

				}

			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	private class AddClientTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String mClientName;
		String mClientAddress;

		public AddClientTask(String client_name, String client_address) {
			mClientName = client_name;
			mClientAddress = client_address;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.ADD_CLIENT));

				if (Global.getInstance().getBooleanType(
						SelectClientsActivity.this,
						RequestParameters.IS_EMPLOYER)) {
					entity.addPart(
							RequestParameters.EMPLOYER_ID,
							new StringBody(Global.getInstance()
									.getPreferenceVal(
											SelectClientsActivity.this,
											RequestParameters.EMPLOYER_ID)));
				} else {
					entity.addPart(
							RequestParameters.EMPLOYER_ID,
							new StringBody(Global.getInstance()
									.getPreferenceVal(
											SelectClientsActivity.this,
											RequestParameters.ADMIN_ID)));
				}

				entity.addPart(RequestParameters.CLIENT_NAME, new StringBody(
						mClientName));
				entity.addPart(RequestParameters.CLIENT_ADDRESS,
						new StringBody(mClientAddress));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject clientJson = jsonResponse.getJSONObject("response");
				String success = clientJson.getString("success");
				String message = clientJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Client Added"))) {
					JSONObject clientDetails = clientJson
							.getJSONObject("client");

					String client_Id = clientDetails.getString("id");
					String employer_Id = clientDetails.getString("employerid");
					String client_Name = clientDetails.getString("clientname");
					String client_Address = clientDetails.getString("address");
					String active = clientDetails.getString("active");

					db.open();
					db.insertClients(client_Id, employer_Id, client_Name,
							client_Address, active);
					db.close();

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(SelectClientsActivity.this,
							SelectClientsActivity.class); // your class
					i.putExtra("clock_type", mClockOnType);
					startActivity(i);
					finish();

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
