package com.timeclockmobile.adapter;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.timeclockmobile.R;
import com.timeclockmobile.data.FieldNotes;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;

public class FieldNotesAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<FieldNotes> mFieldsNotesList;
	LayoutInflater inflater;
	public ProgressDialog progressDialog;
	private String clientId;
	ArrayList<String> checkedFieldNotes;
	ArrayList<FieldNotes> checkedFNObject;
	String inDate;

	public FieldNotesAdapter(Context mContext, int layoutResourceId,
			ArrayList<FieldNotes> FieldNotesArrayList) {
		this.mContext = mContext;
		this.mFieldsNotesList = FieldNotesArrayList;
		this.layoutResourceId = layoutResourceId;
		progressDialog = new ProgressDialog(mContext);
		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		checkedFNObject = new ArrayList<FieldNotes>();
		checkedFieldNotes = new ArrayList<String>();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFieldsNotesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mFieldsNotesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.fn_user_name = (TextView) convertView
					.findViewById(R.id.fn_user_name);

			holder.fn_date = (TextView) convertView.findViewById(R.id.fn_date);

			holder.fn_client_name = (TextView) convertView
					.findViewById(R.id.fn_client_name);

			holder.check_note = (CheckBox) convertView
					.findViewById(R.id.check_note);

			holder.check_note
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub

							int getPosition = (Integer) buttonView.getTag();

							mFieldsNotesList.get(getPosition).setChecked(
									buttonView.isChecked());

							if (isChecked) {

								// itemChecked[position] = true;
								checkedFieldNotes.add(mFieldsNotesList.get(
										getPosition).getId());

								checkedFNObject.add(mFieldsNotesList
										.get(getPosition));

							} else {

								// itemChecked[position] = false;
								checkedFieldNotes.remove(mFieldsNotesList.get(
										getPosition).getId());

								checkedFNObject.remove(mFieldsNotesList
										.get(getPosition));
							}
						}
					});

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.check_note.setTag(position);
		holder.fn_user_name.setText(mFieldsNotesList.get(position).getName());
		holder.fn_client_name.setText(mFieldsNotesList.get(position)
				.getClientname());

		String onlyDate[] = mFieldsNotesList.get(position).getCreated()
				.split(" ");

		String date[] = onlyDate[0].split("-");

		if (Global.getInstance()
				.getPreferenceVal(mContext, CONSTANTS.DATE_FORMAT)
				.equalsIgnoreCase("DD/MM/YYYY")) {
			// inDate = new SimpleDateFormat("dd/MM/yyyy EEEE").format(new
			// Date());

			inDate = date[2] + "/" + date[1] + "/" + date[0];
		} else {

			// inDate = new SimpleDateFormat("MM/dd/yyyy EEEE").format(new
			// Date());

			inDate = date[1] + "/" + date[2] + "/" + date[0];
		}

		// holder.fn_date.setText(mFieldsNotesList.get(position).getCreated());

		holder.fn_date.setText(inDate);
		holder.check_note
				.setChecked(mFieldsNotesList.get(position).isChecked());
		return convertView;
	}

	class ViewHolder {
		TextView fn_user_name;
		TextView fn_date;
		TextView fn_client_name;
		CheckBox check_note;
	}

	public ArrayList<String> getArrayListOfCheckedFieldNotes() {
		return checkedFieldNotes;

	}

	public ArrayList<FieldNotes> getCheckedFieldNotes() {

		return checkedFNObject;

	}
}
