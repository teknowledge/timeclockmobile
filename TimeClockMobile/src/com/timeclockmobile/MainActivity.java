package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.timeclockmobile.data.Clients;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.service.RepeatService;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class MainActivity extends Activity {

	private EditText enterPinNo;
	private Button loginBtn;
	public ProgressDialog progressDialog;
	private Utils utils = new Utils();

	private DataBaseHelper db = new DataBaseHelper(MainActivity.this);

	private String pinNumber;

	// String session = "LOGIN";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen);

		progressDialog = new ProgressDialog(MainActivity.this);

		enterPinNo = (EditText) findViewById(R.id.pinNo);

		loginBtn = (Button) findViewById(R.id.loginBtn);

		loginBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				pinNumber = enterPinNo.getText().toString();

				if (utils.isConnectingToInternet(MainActivity.this)) {

					if (!pinNumber.equalsIgnoreCase("")) {

						db.open();
						db.deleteClients();
						db.deleteJobTypes();
						db.deleteEmployees();
						db.deleteWorkingEmployees();
						db.close();

						new LoginUser().execute("");
					} else {
						Toast.makeText(getApplicationContext(),
								"Please enter pin", Toast.LENGTH_SHORT).show();
					}

				} else {
					utils.showDialog(MainActivity.this);
				}

			}
		});

		// final String login = Global.getInstance().getPreferenceVal(
		// MainActivity.this, RequestParameters.SESSION);
		// if (session.equals(login)) {
		// Intent intent = new Intent(getApplicationContext(),
		// HomeActivity.class);
		// startActivity(intent);
		// finish();
		// }

	}

	private class LoginUser extends AsyncTask<String, Void, Void> {

		String response = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.LOGIN_USER));

				entity.addPart(RequestParameters.PIN_NUMBER, new StringBody(
						pinNumber));

			} catch (UnsupportedEncodingException e) {

				response = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				response = "exception";
				e.printStackTrace();
			}

			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

				// System.out.println(response + "value text");

			} catch (Exception e) {
				response = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				System.out.println(jsonObject);

				String message = jsonObject.getString("message");
				System.out.println(message);

				if (message.equalsIgnoreCase(RequestParameters.USER_EXISTS)) {

					// ******* user information with the details *******//

					JSONObject user = jsonObject.getJSONObject("user");
					System.out.println(user);

					String user_id = user.getString("id");
					String name = user.getString("name");

					String email = user.getString("email");
					String address = user.getString("address");
					String password = user.getString("password");
					String phoneno = user.getString("phoneno");
					String profileimage = user.getString("profileimage");
					String hourlyrate = user.getString("hourlyrate");
					String devicetoken = user.getString("devicetoken");
					String isemployer = user.getString("isemployer");
					String employerid = user.getString("employerid");
					String online = user.getString("online");
					String status = user.getString("status");
					String created = user.getString("created");
					String lastlogin = user.getString("lastlogin");

					utils.convertGMTtoLocalTime(lastlogin);

					Global.getInstance().storeIntoPreference(MainActivity.this,
							RequestParameters.USER_ID, user_id);
					Global.getInstance().storeIntoPreference(MainActivity.this,
							CONSTANTS.USER_NAME, name);
					Global.getInstance().storeIntoPreference(MainActivity.this,
							RequestParameters.SESSION, "LOGIN");

					if (isemployer.equalsIgnoreCase("1")) {
						Global.getInstance().storeBooleanType(
								MainActivity.this,
								RequestParameters.IS_EMPLOYER, true);
						Global.getInstance().storeIntoPreference(
								MainActivity.this,
								RequestParameters.EMPLOYER_ID, user_id);
						Global.getInstance().storeIntoPreference(
								MainActivity.this,
								RequestParameters.EMPLOYER_NAME, name);
					} else {
						Global.getInstance().storeBooleanType(
								MainActivity.this,
								RequestParameters.IS_EMPLOYER, false);
					}

					// if (isemployer.equalsIgnoreCase("1")) {

					// ****** clients array with their details*******//

					JSONArray clientsArray = jsonObject.getJSONArray("clients");
					System.out.println(clientsArray);

					for (int i = 0; i < clientsArray.length(); i++) {
						JSONObject clientObject = clientsArray.getJSONObject(i);

						String clientId = clientObject.getString("id");
						String employerId = clientObject
								.getString("employerid");
						String clientName = clientObject
								.getString("clientname");
						String clientAddress = clientObject
								.getString("address");
						String clientActive = clientObject.getString("active");

						Clients client = new Clients();
						client.setmEmployerId(employerId);
						client.setmClientName(clientName);
						client.setmClientAddress(clientAddress);
						client.setmActive(clientActive);

						// ********** saving clients and details in clients
						// table in
						// db****************//

						db.open();
						db.insertClients(clientId, employerId, clientName,
								clientAddress, clientActive);
						db.close();
					}

					// ******* Job types with their details *******//
					JSONArray jobTypesArray = jsonObject
							.getJSONArray("jobtypes");
					System.out.println(jobTypesArray);

					for (int i = 0; i < jobTypesArray.length(); i++) {
						JSONObject jobTypesObject = jobTypesArray
								.getJSONObject(i);

						String jobTypeId = jobTypesObject.getString("id");
						String jobTypeEmployerId = jobTypesObject
								.getString("employerid");
						String jobTypeName = jobTypesObject
								.getString("jobname");

						String jobTypeActive = jobTypesObject
								.getString("active");

						db.open();
						db.insertJobTypes(jobTypeId, jobTypeEmployerId,
								jobTypeName, jobTypeActive);
						db.close();

					}

					// ********* employees with their details ********//

					JSONArray employeesArray = jsonObject
							.getJSONArray("employees");
					System.out.println(employeesArray);

					for (int i = 0; i < employeesArray.length(); i++) {
						JSONObject employeesObject = employeesArray
								.getJSONObject(i);

						String employeeId = employeesObject.getString("id");
						String employeeName = employeesObject.getString("name");

						String employeeEmail = employeesObject
								.getString("email");
						String employeeAddress = employeesObject
								.getString("address");
						String employeePassword = employeesObject
								.getString("password");
						String employeePhoneNo = employeesObject
								.getString("phoneno");
						String employeeProfileImage = employeesObject
								.getString("profileimage");
						String employeeHourlyRate = employeesObject
								.getString("hourlyrate");
						String employeeDeviceToken = employeesObject
								.getString("devicetoken");
						String employeeIsEmployer = employeesObject
								.getString("isemployer");
						String employerId = employeesObject
								.getString("employerid");
						String employeeOnline = employeesObject
								.getString("online");
						String employeeStatus = employeesObject
								.getString("status");

						String employeeCreatedGMT = employeesObject
								.getString("created");
						String employeeCreated = utils
								.convertGMTtoLocalTime(employeeCreatedGMT);

						String employeeLastLoginGMT = employeesObject
								.getString("lastlogin");

						String employeeLastLogin = utils
								.convertGMTtoLocalTime(employeeLastLoginGMT);

						db.open();
						db.insertEmployees(employeeId, employeeName,
								employeeEmail, employeeAddress,
								employeePassword, employeePhoneNo,
								employeeProfileImage, employeeHourlyRate,
								employeeDeviceToken, employeeIsEmployer,
								employerId, employeeOnline, employeeStatus,
								employeeCreated, employeeLastLogin, "0", "",
								"", "", "", "", "", "", "", "", "", "", "",
								"null", "null", "0");
						db.close();
					}

					// ******* WorkingEmployee with their client
					// details********//

					JSONArray workingEmployeesArray = jsonObject
							.getJSONArray("workingemployees");
					System.out.println(workingEmployeesArray);
					String workingEmployeeOutTime;
					String workingEmployeeOutDate;
					String isBreakStarted = "0";

					// db.open();
					// db.deleteDetailsForClockOn();
					// db.close();

					for (int i = 0; i < workingEmployeesArray.length(); i++) {
						JSONObject workingEmployeesObject = workingEmployeesArray
								.getJSONObject(i);

						String workingEmployeeName = workingEmployeesObject
								.getString("name");

						String workingEmployeeEmail = workingEmployeesObject
								.getString("email");
						String workingEmployeeAddress = workingEmployeesObject
								.getString("address");
						String workingEmployeePassword = workingEmployeesObject
								.getString("password");
						String workingEmployeePhoneno = workingEmployeesObject
								.getString("phoneno");
						String workingEmployeeHourlyrate = workingEmployeesObject
								.getString("userhourlyrate");
						String workingEmployeeDevicetoken = workingEmployeesObject
								.getString("devicetoken");
						String workingEmployeeOnline = workingEmployeesObject
								.getString("online");
						String workingEmployeeStatus = workingEmployeesObject
								.getString("status");
						String workingEmployeeId = workingEmployeesObject
								.getString("id");
						String workingEmployeeUserId = workingEmployeesObject
								.getString("userid");

						String workingEmployeeClientId = workingEmployeesObject
								.getString("clientid");
						String workingEmployeeJobTypeId = workingEmployeesObject
								.getString("jobtypeid");
						String workingEmployeeInTimeGMT = workingEmployeesObject
								.getString("intime");
						String workingEmployeeInDateGMT = workingEmployeesObject
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String workingEmployeeInTime = IntimeAndDate[1];
						String workingEmployeeInDate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = workingEmployeesObject
								.getString("outtime");
						String workingEmployeeOutDateGMT = workingEmployeesObject
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							workingEmployeeOutTime = outTimeAndDate[1];

							workingEmployeeOutDate = outTimeAndDate[0];
						} else {
							workingEmployeeOutTime = workingEmployeeOutTimeGMT;

							workingEmployeeOutDate = workingEmployeeOutDateGMT;
						}
						String workingEmployeeStartLat = workingEmployeesObject
								.getString("startlat");
						String workingEmployeeStartLng = workingEmployeesObject
								.getString("startlng");
						String workingEmployeeEndLat = workingEmployeesObject
								.getString("endlat");
						String workingEmployeeEndLng = workingEmployeesObject
								.getString("endlng");
						String workingEmployeeActive = workingEmployeesObject
								.getString("active");
						String workingEmployeeCreated = workingEmployeesObject
								.getString("created");
						String workingEmployeeClientName = workingEmployeesObject
								.getString("clientname");
						String workingEmployeeClientAddress = workingEmployeesObject
								.getString("clientaddress");
						String workingEmployeeJobName = workingEmployeesObject
								.getString("jobname");
						JSONArray breaksArray = workingEmployeesObject
								.getJSONArray("breaks");

						if (breaksArray.length() == 0) {
							isBreakStarted = "0";
						} else {
							for (int x = 0; x < breaksArray.length(); x++) {
								JSONObject break_ts = breaksArray
										.getJSONObject(x);

								String break_tr_id = break_ts.getString("trid");
								// String start_break_timeGMT = break_ts
								// .getString("startbreaktime");
								// String start_break_dateGMT = break_ts
								// .getString("startbreakdate");
								String end_break_timeGMT = break_ts
										.getString("endbreaktime");
								String end_break_dateGMT = break_ts
										.getString("endbreakdate");

								if (end_break_timeGMT.equalsIgnoreCase("null")
										&& end_break_dateGMT
												.equalsIgnoreCase("null")) {
									isBreakStarted = "1";
								} else {
									isBreakStarted = "0";
								}
							}
						}

						if (Global
								.getInstance()
								.getPreferenceVal(MainActivity.this,
										RequestParameters.USER_ID)
								.equalsIgnoreCase(workingEmployeeUserId)) {
							if (workingEmployeeOutTime.equalsIgnoreCase("null")) {

								if (!workingEmployeeInTime.equalsIgnoreCase("")) {

									if (breaksArray.length() == 0) {
										isBreakStarted = "0";
									} else {
										for (int x = 0; x < breaksArray
												.length(); x++) {
											JSONObject break_ts = breaksArray
													.getJSONObject(x);

											String break_tr_id = break_ts
													.getString("trid");
											// String start_break_timeGMT =
											// break_ts
											// .getString("startbreaktime");
											// String start_break_dateGMT =
											// break_ts
											// .getString("startbreakdate");
											String end_break_timeGMT = break_ts
													.getString("endbreaktime");
											String end_break_dateGMT = break_ts
													.getString("endbreakdate");

											if (end_break_timeGMT
													.equalsIgnoreCase("null")
													&& end_break_dateGMT
															.equalsIgnoreCase("null")) {

												Global.getInstance()
														.storeBooleanType(
																MainActivity.this,
																CONSTANTS.START_BREAK_VALUE,
																true);
											}
										}
									}

									Global.getInstance().storeBooleanType(
											MainActivity.this,
											CONSTANTS.USER_IS_WORKING, true);
									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.USER_IN_TIME,
											workingEmployeeInTime);
									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.USER_IN_DATE,
											workingEmployeeInDate);

									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.CLOCKED_ON_JOBTYPEID,
											workingEmployeeJobTypeId);
									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.CLOCKED_ON_CLIENTID,
											workingEmployeeClientId);
									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.CLOCKED_ON_JOBNAME,
											workingEmployeeJobName);
									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.CLOCKED_ON_CLIENTNAME,
											workingEmployeeClientName);
									Global.getInstance().storeIntoPreference(
											MainActivity.this,
											CONSTANTS.CLOCKED_ON_TRID,
											workingEmployeeId);
									Global.getInstance().storeBooleanType(
											MainActivity.this,
											CONSTANTS.CLOCKED_ON, true);

									/********
									 * submit user location when login and is
									 * working
									 **********/
									submitLocationService();

								}

							}
						}

						// inserting working employees in working employees
						// table
						db.open();
						db.insertWorkingEmployees(workingEmployeeName,
								workingEmployeeEmail, workingEmployeeAddress,
								workingEmployeePassword,
								workingEmployeePhoneno,
								workingEmployeeHourlyrate,
								workingEmployeeDevicetoken,
								workingEmployeeOnline, workingEmployeeStatus,
								workingEmployeeId, workingEmployeeUserId,
								workingEmployeeClientId,
								workingEmployeeJobTypeId,
								workingEmployeeInTime, workingEmployeeInDate,
								workingEmployeeOutTime, workingEmployeeOutDate,
								workingEmployeeStartLat,
								workingEmployeeStartLng, workingEmployeeEndLat,
								workingEmployeeEndLng, workingEmployeeActive,
								workingEmployeeCreated,
								workingEmployeeClientName,
								workingEmployeeClientAddress,
								workingEmployeeJobName);
						db.close();

						// updating employees table with the working
						// inforamtion..
						db.open();
						db.updateEmployeeDetailsForWorking(
								workingEmployeeUserId, "1", workingEmployeeId,
								workingEmployeeClientId,
								workingEmployeeJobTypeId,
								workingEmployeeInTime, workingEmployeeInDate,
								workingEmployeeStartLat,
								workingEmployeeStartLng,
								workingEmployeeClientName,
								workingEmployeeCreated, workingEmployeeEndLat,
								workingEmployeeEndLng, workingEmployeeJobName,
								workingEmployeeOutTime, workingEmployeeOutDate,
								isBreakStarted);
						db.close();
					}

					Global.getInstance().storeBooleanType(MainActivity.this,
							CONSTANTS.ENABLE_NOTIFICATION, true);
					Global.getInstance().storeBooleanType(MainActivity.this,
							CONSTANTS.INCLUDE_BREAKS, true);

					Intent intent = new Intent(MainActivity.this,
							HomeActivity.class);
					startActivity(intent);
					finish();

				} else {

					int ecolor = Color.BLACK; // whatever color you wants
					String estring = "Please enter correct pin";
					ForegroundColorSpan fgcspan = new ForegroundColorSpan(
							ecolor);
					SpannableStringBuilder ssbuilder = new SpannableStringBuilder(
							estring);
					ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);
					// myedittext.setError(ssbuilder);

					enterPinNo.setError(ssbuilder);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/**********
	 * submitting user's location to the server for every 5 min in the backgroud
	 * using service
	 *************/

	public void submitLocationService() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 30);

		Intent intent = new Intent(this, RepeatService.class);

		PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		// for 30 mint 60*60*1000
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
				5 * 60 * 1000, pintent);

		startService(new Intent(this, RepeatService.class));
	}

}
