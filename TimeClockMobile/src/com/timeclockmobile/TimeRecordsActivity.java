package com.timeclockmobile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;

import com.timeclockmobile.adapter.AprovalTypeAdapter;
import com.timeclockmobile.adapter.ClientsAdapterForFieldNotes;
import com.timeclockmobile.adapter.SpinnerAdapter;
import com.timeclockmobile.adapter.EmployeesListAdapter;
import com.timeclockmobile.adapter.JobTypesAdapterForTimeRecord;
import com.timeclockmobile.data.Breaks;
import com.timeclockmobile.data.Clients;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.data.JobTypes;
import com.timeclockmobile.data.TimeRecords;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class TimeRecordsActivity extends Activity {

	private Spinner all_clients, all_employees, all_job_type, all_date,
			approval_type;
	private TextView records_count, time_count, pay_count;
	private Button record_detail, clear_records;
	private Button export_btn, add_btn;
	public ProgressDialog progressDialog;
	private ImageButton backBtn;

	private ArrayList<Employees> employeesList = new ArrayList<Employees>();
	private ArrayList<Clients> clientsList = new ArrayList<Clients>();
	private ArrayList<JobTypes> jobTypesList = new ArrayList<JobTypes>();
	private ArrayList<String> allDatesList = new ArrayList<String>();
	private ArrayList<String> approvalType = new ArrayList<String>();

	private ArrayList<TimeRecords> timeRecordsList = new ArrayList<TimeRecords>();

	private DataBaseHelper db = new DataBaseHelper(TimeRecordsActivity.this);

	private ManageEmployeesActivity manageEmployeesActivity = new ManageEmployeesActivity();
	private ManageClientsActivity manageClientsActivity = new ManageClientsActivity();
	private ManageJobTypesActivity manageJobTypesActivity = new ManageJobTypesActivity();

	private EmployeesListAdapter adapterEmployees;
	private ClientsAdapterForFieldNotes adapterClients;
	private JobTypesAdapterForTimeRecord adapterJobTypes;
	private SpinnerAdapter adapterDates;
	private AprovalTypeAdapter adapterAproval;

	private String mClientId, mEmployeeId, mJobTypeId, mDates, mAprovalType;
	private String whereQuery;

	private String mToday, mYesterday, mFirstDayOfThisWeek, mLastDayOfThisWeek,
			mFirstOfLastWeek, mLastDayOfLastWeek;

	private boolean isSelectedClient = false;
	private boolean isSelectedEmployee = false;
	private boolean isSelectedJobType = false;
	private boolean isSelectedDate = false;
	private boolean isSelectedApprovedType = false;

	int weekDaysCount = 0;

	public Utils utils = new Utils();
	private String TimeInHoursAndMinutes[] = new String[3];
	private int timeInHours;
	private int timeInMinutes;
	private int totalInSeconds;
	private double totalAmountForRecords;

	String DataBase_Name = "timeclock";
	SQLiteDatabase myDatabase = null;
	String Table_Name = "export_records";

	Uri timeRecords = null;
	File time_sheet_file;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.time_records_activity);

		progressDialog = new ProgressDialog(TimeRecordsActivity.this);

		myDatabase = this.openOrCreateDatabase(DataBase_Name, MODE_PRIVATE,
				null);

		all_clients = (Spinner) findViewById(R.id.all_clients1);
		all_employees = (Spinner) findViewById(R.id.all_employees);
		all_job_type = (Spinner) findViewById(R.id.all_job_type);
		all_date = (Spinner) findViewById(R.id.all_date);
		approval_type = (Spinner) findViewById(R.id.approval_type);

		records_count = (TextView) findViewById(R.id.records_count);
		time_count = (TextView) findViewById(R.id.time_count);
		pay_count = (TextView) findViewById(R.id.pay_count);

		clear_records = (Button) findViewById(R.id.clear_records);
		record_detail = (Button) findViewById(R.id.record_detail);
		export_btn = (Button) findViewById(R.id.export_btn);
		add_btn = (Button) findViewById(R.id.add_btn);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		if (utils.isConnectingToInternet(TimeRecordsActivity.this)) {
			// get all time records...
			new GetAllTimeRecords().execute("");
		} else {
			utils.showDialog(TimeRecordsActivity.this);
		}

		clear_records.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				all_clients.setSelection(0);
				all_employees.setSelection(0);
				all_job_type.setSelection(0);
				all_date.setSelection(0);
				approval_type.setSelection(0);

			}
		});

		record_detail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TimeRecordsActivity.this,
						TimeRecordDetailsActivity.class);
				intent.putExtra("arrayList", timeRecordsList);
				startActivity(intent);
				finish();
			}
		});

		export_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// alert dialog to delete employee

				/*************
				 * Exporting and creating csv file with selected time record
				 * sheets
				 *************/

				new ExportDatabaseCSVTask().execute();

				/********** display the dialog to select the option to mail or save ***********/

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						TimeRecordsActivity.this);

				// Setting Dialog Title
				alertDialog.setTitle("Export time records");

				// Setting Dialog Message
				alertDialog.setMessage("Do you want to Save or Email?");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("Save",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.
								// delete employee

								// new ExportDatabaseCSVTask().execute();

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("Email",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog
								dialog.cancel();

								time_sheet_file = new File(Environment
										.getExternalStorageDirectory(),
										"time_sheets.csv");

								timeRecords = Uri.fromFile(time_sheet_file);

								Intent emailIntent = new Intent(
										Intent.ACTION_SENDTO,
										Uri.fromParts(
												"mailto",
												Global.getInstance()
														.getPreferenceVal(
																TimeRecordsActivity.this,
																CONSTANTS.EMAIL_ID),
												null));
								// emailIntent.putExtra(Intent.EXTRA_SUBJECT,
								// "Call Notes - " + contactName + " - "
								// + strDate);
								emailIntent.putExtra(Intent.EXTRA_SUBJECT,
										"Time sheets");
								emailIntent.putExtra(Intent.EXTRA_STREAM,
										timeRecords);
								try {
									startActivity(Intent.createChooser(
											emailIntent, "Send email..."));
								} catch (android.content.ActivityNotFoundException ex) {
									Toast.makeText(
											TimeRecordsActivity.this,
											"There are no email clients installed.",
											Toast.LENGTH_SHORT).show();
								}
							}
						});

				// Showing Alert Message
				alertDialog.show();

			}
		});

		add_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TimeRecordsActivity.this,
						AddTimeRecordActivity.class);
				intent.putExtra("type", "add");
				startActivity(intent);
				finish();
			}
		});

		// dates

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		mYesterday = dateFormat.format(cal.getTime());

		mToday = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		// current week dates..

		cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String[] cureentWeekDays = new String[7];
		for (int i = 0; i < 7; i++) {
			Log.i("dateTag", sdf.format(cal.getTime()));
			cureentWeekDays[i] = sdf.format(cal.getTime());
			cal.add(Calendar.DAY_OF_WEEK, 1);
		}
		mFirstDayOfThisWeek = cureentWeekDays[0];
		mLastDayOfThisWeek = cureentWeekDays[6];
		// last week dates
		weekDaysCount--;
		Calendar now1 = Calendar.getInstance();
		Calendar now = (Calendar) now1.clone();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String[] days = new String[7];
		int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2;
		now.add(Calendar.WEEK_OF_YEAR, weekDaysCount);
		now.add(Calendar.DAY_OF_MONTH, delta);
		for (int i = 0; i < 7; i++) {
			days[i] = format.format(now.getTime());
			now.add(Calendar.DAY_OF_MONTH, 1);
		}

		mFirstOfLastWeek = days[0];
		mLastDayOfLastWeek = days[6];

		// assigning clients to the clients spinner

		clientsList = manageClientsActivity
				.getClientsFromDB(TimeRecordsActivity.this);

		Clients client = new Clients();
		client.setmClientId("0");
		client.setmClientName("All Clients");

		clientsList.add(0, client);

		adapterClients = new ClientsAdapterForFieldNotes(
				TimeRecordsActivity.this, R.layout.spinner_list_item,
				clientsList);

		all_clients.setAdapter(adapterClients);

		// assigning employees to the employees spinner

		if (!Global.getInstance().getBooleanType(TimeRecordsActivity.this,
				RequestParameters.IS_EMPLOYER)) {
			Employees employee = new Employees();
			employee.setId(Global.getInstance().getPreferenceVal(
					TimeRecordsActivity.this, RequestParameters.USER_ID));

			employee.setName(Global.getInstance().getPreferenceVal(
					TimeRecordsActivity.this, CONSTANTS.USER_NAME));
			employeesList.add(0, employee);

			adapterEmployees = new EmployeesListAdapter(
					TimeRecordsActivity.this, R.layout.spinner_list_item,
					employeesList);

			all_employees.setAdapter(adapterEmployees);
		} else {
			employeesList = manageEmployeesActivity
					.getEmployeesDetailsFromDB(TimeRecordsActivity.this);
			Employees employee = new Employees();
			employee.setId("0");
			employee.setName("All Employees");
			employeesList.add(0, employee);

			Employees employee1 = new Employees();

			employee1.setId(Global.getInstance().getPreferenceVal(
					TimeRecordsActivity.this, RequestParameters.EMPLOYER_ID));
			employee1.setName(Global.getInstance().getPreferenceVal(
					TimeRecordsActivity.this, RequestParameters.EMPLOYER_NAME));
			employeesList.add(employee1);

			adapterEmployees = new EmployeesListAdapter(
					TimeRecordsActivity.this, R.layout.spinner_list_item,
					employeesList);

			all_employees.setAdapter(adapterEmployees);
		}

		// assigning jobtypes to the spinner

		jobTypesList = manageJobTypesActivity
				.getJobTypesFromDB(TimeRecordsActivity.this);

		JobTypes jobtypes = new JobTypes();
		jobtypes.setId("0");
		jobtypes.setmJobName("All Job Types");

		jobTypesList.add(0, jobtypes);

		adapterJobTypes = new JobTypesAdapterForTimeRecord(
				TimeRecordsActivity.this, R.layout.spinner_list_item,
				jobTypesList);

		all_job_type.setAdapter(adapterJobTypes);

		// all dates to the dates spinner
		allDatesList.add("All Date");
		allDatesList.add("Today");
		allDatesList.add("Yesterday");
		allDatesList.add("This week");
		allDatesList.add("Last week");

		adapterDates = new SpinnerAdapter(TimeRecordsActivity.this,
				R.layout.spinner_list_item, allDatesList);

		all_date.setAdapter(adapterDates);

		// approve types to approveTypes spinner
		approvalType.add("Approved");
		approvalType.add("Not Approved");
		adapterAproval = new AprovalTypeAdapter(TimeRecordsActivity.this,
				R.layout.spinner_list_item, approvalType);
		approval_type.setAdapter(adapterAproval);

		// selecting particular client to get time record

		all_clients.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				ArrayList<String> array = new ArrayList<String>();
				Clients client = (Clients) parent.getAdapter()
						.getItem(position);

				if (!isSelectedClient) {
					isSelectedClient = true;

					mClientId = client.getmClientId();
					// getTimeRecords();
				} else {

					// Toast.makeText(getApplicationContext(),
					// client.getmClientId(),
					// Toast.LENGTH_LONG).show();

					mClientId = client.getmClientId();

					if ((mClientId.equalsIgnoreCase("0"))
							&& (mEmployeeId.equalsIgnoreCase("0"))
							&& (mJobTypeId.equalsIgnoreCase("0"))
							&& (mDates.equalsIgnoreCase("All Date"))
							&& (mAprovalType.equalsIgnoreCase("Approved"))) {
						getTimeRecords();
					} else {
						// if particular client is selected

						if (!mClientId.equalsIgnoreCase("0")) {
							array.add("time_record_client_id" + "='"
									+ mClientId + "'");
						}

						// if particular employee is selected

						if (!mEmployeeId.equalsIgnoreCase("0")) {
							array.add("time_record_user_id" + "='"
									+ mEmployeeId + "'");
						}
						// //if particular job type is selected

						if (!mJobTypeId.equalsIgnoreCase("0")) {
							array.add("time_record_job_type_id" + "='"
									+ mJobTypeId + "'");
						}

						// if particular date is selected

						// if (!mDates.equalsIgnoreCase("All Date")) {
						// array.add(mDates);
						// }
						if (mDates.equalsIgnoreCase("Today")) {
							String where = "time_record_in_date" + "='"
									+ mToday + "'";
							array.add(where);

						}
						if (mDates.equalsIgnoreCase("Yesterday")) {
							String where = "time_record_in_date" + "='"
									+ mYesterday + "'";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("This week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstDayOfThisWeek + "' AND '"
									+ mLastDayOfThisWeek + "' ";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("Last week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstOfLastWeek + "' AND '"
									+ mLastDayOfLastWeek + "' ";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Not Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "0" + "'";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "1" + "'";
							array.add(where);
						}

						for (int i = 0; i < array.size(); i++) {
							if (i == 0) {
								whereQuery = array.get(i);
							} else {
								whereQuery = whereQuery + " and "
										+ array.get(i);
							}

						}

						getTimeRecordsFromDb(whereQuery);

					}

					// whereQuery = "time_record_client_id" + "='" + "mClientId"
					// + "'" + " and " + "time_record_user_id" + "='"
					// + "mEmployeeId" + "'" + " and "
					// + "time_record_job_type_id" + "='" + "mJobTypeId"
					// + "'";

					// getTimeRecordsFromDb(whereQuery);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		// selecting particular employee or all employees to get time record
		all_employees.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ArrayList<String> array = new ArrayList<String>();

				Employees employee = (Employees) parent.getAdapter().getItem(
						position);
				if (!isSelectedEmployee) {
					isSelectedEmployee = true;
					mEmployeeId = employee.getId();

				} else {

					// Toast.makeText(getApplicationContext(),
					// employee.getId(), Toast.LENGTH_LONG).show();
					mEmployeeId = employee.getId();

					if ((mClientId.equalsIgnoreCase("0"))
							&& (mEmployeeId.equalsIgnoreCase("0"))
							&& (mJobTypeId.equalsIgnoreCase("0"))
							&& (mDates.equalsIgnoreCase("All Date"))
							&& (mAprovalType.equalsIgnoreCase("Approved"))) {
						getTimeRecords();
					} else { // if particular client is selected

						if (!mClientId.equalsIgnoreCase("0")) {
							array.add("time_record_client_id" + "='"
									+ mClientId + "'");
						}

						// if particular employee is selected

						if (!mEmployeeId.equalsIgnoreCase("0")) {
							array.add("time_record_user_id" + "='"
									+ mEmployeeId + "'");
						}
						// //if particular job type is selected

						if (!mJobTypeId.equalsIgnoreCase("0")) {
							array.add("time_record_job_type_id" + "='"
									+ mJobTypeId + "'");
						}

						// if particular date is selected

						if (mDates.equalsIgnoreCase("Today")) {
							String where = "time_record_in_date" + "='"
									+ mToday + "'";
							array.add(where);

						}
						if (mDates.equalsIgnoreCase("Yesterday")) {
							String where = "time_record_in_date" + "='"
									+ mYesterday + "'";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("This week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstDayOfThisWeek + "' AND '"
									+ mLastDayOfThisWeek + "' ";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("Last week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstOfLastWeek + "' AND '"
									+ mLastDayOfLastWeek + "' ";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Not Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "0" + "'";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "1" + "'";
							array.add(where);
						}

						for (int i = 0; i < array.size(); i++) {
							if (i == 0) {
								whereQuery = array.get(i);
							} else {
								whereQuery = whereQuery + " and "
										+ array.get(i);
							}

						}

						getTimeRecordsFromDb(whereQuery);
					}

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		// selecting particular job type or all job types to get time record
		all_job_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ArrayList<String> array = new ArrayList<String>();

				JobTypes jobType = (JobTypes) parent.getAdapter().getItem(
						position);
				if (!isSelectedJobType) {
					isSelectedJobType = true;
					mJobTypeId = jobType.getId();

				} else {

					mJobTypeId = jobType.getId();

					if ((mClientId.equalsIgnoreCase("0"))
							&& (mEmployeeId.equalsIgnoreCase("0"))
							&& (mJobTypeId.equalsIgnoreCase("0"))
							&& (mDates.equalsIgnoreCase("All Date"))
							&& (mAprovalType.equalsIgnoreCase("Approved"))) {
						getTimeRecords();
					} else { // if particular client is selected

						if (!mClientId.equalsIgnoreCase("0")) {
							array.add("time_record_client_id" + "='"
									+ mClientId + "'");
						}

						// if particular employee is selected

						if (!mEmployeeId.equalsIgnoreCase("0")) {
							array.add("time_record_user_id" + "='"
									+ mEmployeeId + "'");
						}
						// //if particular job type is selected

						if (!mJobTypeId.equalsIgnoreCase("0")) {
							array.add("time_record_job_type_id" + "='"
									+ mJobTypeId + "'");
						}

						// if particular date is selected

						if (mDates.equalsIgnoreCase("Today")) {
							String where = "time_record_in_date" + "='"
									+ mToday + "'";
							array.add(where);

						}
						if (mDates.equalsIgnoreCase("Yesterday")) {
							String where = "time_record_in_date" + "='"
									+ mYesterday + "'";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("This week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstDayOfThisWeek + "' AND '"
									+ mLastDayOfThisWeek + "' ";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("Last week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstOfLastWeek + "' AND '"
									+ mLastDayOfLastWeek + "' ";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Not Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "0" + "'";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "1" + "'";
							array.add(where);
						}

						for (int i = 0; i < array.size(); i++) {
							if (i == 0) {
								whereQuery = array.get(i);
							} else {
								whereQuery = whereQuery + " and "
										+ array.get(i);
							}

						}

						getTimeRecordsFromDb(whereQuery);
					}

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		// // selecting particular date or all dates to get time record
		all_date.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ArrayList<String> array = new ArrayList<String>();
				if (!isSelectedDate) {
					isSelectedDate = true;
					mDates = allDatesList.get(position);

				} else {
					mDates = allDatesList.get(position);

					if ((mClientId.equalsIgnoreCase("0"))
							&& (mEmployeeId.equalsIgnoreCase("0"))
							&& (mJobTypeId.equalsIgnoreCase("0"))
							&& (mDates.equalsIgnoreCase("All Date"))
							&& (mAprovalType.equalsIgnoreCase("Approved"))) {
						getTimeRecords();
					} else { // if particular client is selected

						if (!mClientId.equalsIgnoreCase("0")) {
							array.add("time_record_client_id" + "='"
									+ mClientId + "'");
						}

						// if particular employee is selected

						if (!mEmployeeId.equalsIgnoreCase("0")) {
							array.add("time_record_user_id" + "='"
									+ mEmployeeId + "'");
						}
						// //if particular job type is selected

						if (!mJobTypeId.equalsIgnoreCase("0")) {
							array.add("time_record_job_type_id" + "='"
									+ mJobTypeId + "'");
						}

						// if particular date is selected

						if (mDates.equalsIgnoreCase("Today")) {
							String where = "time_record_in_date" + "='"
									+ mToday + "'";
							array.add(where);

						}
						if (mDates.equalsIgnoreCase("Yesterday")) {
							String where = "time_record_in_date" + "='"
									+ mYesterday + "'";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("This week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstDayOfThisWeek + "' AND '"
									+ mLastDayOfThisWeek + "' ";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("Last week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstOfLastWeek + "' AND '"
									+ mLastDayOfLastWeek + "' ";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Not Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "0" + "'";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "1" + "'";
							array.add(where);
						}

						for (int i = 0; i < array.size(); i++) {
							if (i == 0) {
								whereQuery = array.get(i);
							} else {
								whereQuery = whereQuery + " and "
										+ array.get(i);
							}

						}

						getTimeRecordsFromDb(whereQuery);
					}

				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		approval_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				ArrayList<String> array = new ArrayList<String>();
				if (!isSelectedApprovedType) {
					isSelectedApprovedType = true;
					mAprovalType = approvalType.get(position);

				} else {
					mAprovalType = approvalType.get(position);

					if ((mClientId.equalsIgnoreCase("0"))
							&& (mEmployeeId.equalsIgnoreCase("0"))
							&& (mJobTypeId.equalsIgnoreCase("0"))
							&& (mDates.equalsIgnoreCase("All Date"))
							&& (mAprovalType.equalsIgnoreCase("Approved"))) {
						getTimeRecords();
					} else { // if particular client is selected

						if (!mClientId.equalsIgnoreCase("0")) {
							array.add("time_record_client_id" + "='"
									+ mClientId + "'");
						}

						// if particular employee is selected

						if (!mEmployeeId.equalsIgnoreCase("0")) {
							array.add("time_record_user_id" + "='"
									+ mEmployeeId + "'");
						}
						// //if particular job type is selected

						if (!mJobTypeId.equalsIgnoreCase("0")) {
							array.add("time_record_job_type_id" + "='"
									+ mJobTypeId + "'");
						}

						// if particular date is selected

						if (mDates.equalsIgnoreCase("Today")) {
							String where = "time_record_in_date" + "='"
									+ mToday + "'";
							array.add(where);

						}
						if (mDates.equalsIgnoreCase("Yesterday")) {
							String where = "time_record_in_date" + "='"
									+ mYesterday + "'";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("This week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstDayOfThisWeek + "' AND '"
									+ mLastDayOfThisWeek + "' ";
							array.add(where);
						}
						if (mDates.equalsIgnoreCase("Last week")) {
							String where = "time_record_in_date" + " BETWEEN '"
									+ mFirstOfLastWeek + "' AND '"
									+ mLastDayOfLastWeek + "' ";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Not Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "0" + "'";
							array.add(where);
						}
						if (mAprovalType.equalsIgnoreCase("Approved")) {
							String where = "time_record_is_approve" + "='"
									+ "1" + "'";
							array.add(where);
						}

						for (int i = 0; i < array.size(); i++) {
							if (i == 0) {
								whereQuery = array.get(i);
							} else {
								whereQuery = whereQuery + " and "
										+ array.get(i);
							}

						}

						getTimeRecordsFromDb(whereQuery);
					}

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

	}

	public void getTimeRecordsFromDb(String query) {

		timeRecordsList = new ArrayList<TimeRecords>();
		ArrayList<Breaks> breaksArray = new ArrayList<Breaks>();

		timeInHours = 0;
		timeInMinutes = 0;
		totalInSeconds = 0;
		totalAmountForRecords = 0;

		String mManuallyAdded = null;

		int timeInRoundUpValue = 0;

		db.open();

		// delete previously saved records for new csv file generatin
		db.deleteExportTimeRecords();

		// get selected time records
		Cursor trDetails = db.getTimeRecordsForSelection(query);
		if (trDetails.moveToFirst()) {
			do {

				double pay_amount_forMinutesinDouble = 0;

				double pay_amount_forMinutes = 0;

				double pay_amount = 0;

				String name = trDetails.getString(0);
				String email = trDetails.getString(1);

				String address = trDetails.getString(2);

				String password = trDetails.getString(3);

				String phoneno = trDetails.getString(4);

				String userhourlyrate = trDetails.getString(5);

				String devicetoken = trDetails.getString(6);

				String online = trDetails.getString(7);

				String status = trDetails.getString(8);

				String id = trDetails.getString(9);

				String userid = trDetails.getString(10);
				String clientid = trDetails.getString(11);

				String jobtypeid = trDetails.getString(12);

				String intime = trDetails.getString(13);

				String indate = trDetails.getString(14);

				String outtime = trDetails.getString(15);

				String outdate = trDetails.getString(16);

				String startlat = trDetails.getString(17);

				String startlng = trDetails.getString(18);

				String endlat = trDetails.getString(19);

				String endlng = trDetails.getString(20);

				String active = trDetails.getString(21);

				String created = trDetails.getString(22);

				String clientname = trDetails.getString(23);

				String clientaddress = trDetails.getString(24);

				String jobname = trDetails.getString(25);

				String is_approve = trDetails.getString(26);

				String is_manually_added = trDetails.getString(27);

				String dateStart = indate + " " + intime;
				String dateStop = outdate + " " + outtime;

				if (is_manually_added.equalsIgnoreCase("1")) {
					mManuallyAdded = "YES";
				} else {
					mManuallyAdded = "NO";
				}

				TimeRecords timeRecord = new TimeRecords();
				timeRecord.setName(name);
				timeRecord.setEmail(email);
				timeRecord.setAddress(address);
				timeRecord.setPhoneno(phoneno);

				timeRecord.setDevicetoken(devicetoken);
				timeRecord.setOnline(online);
				timeRecord.setStatus(status);
				timeRecord.setId(id);
				timeRecord.setUserid(userid);

				timeRecord.setClientid(clientid);
				timeRecord.setJobtypeid(jobtypeid);
				timeRecord.setIntime(intime);
				timeRecord.setIndate(indate);
				timeRecord.setOuttime(outtime);
				timeRecord.setOutdate(outdate);
				timeRecord.setStartlat(startlat);
				timeRecord.setStartlng(startlng);
				timeRecord.setEndlat(endlat);
				timeRecord.setEndlng(endlng);
				timeRecord.setActive(active);
				timeRecord.setCreated(created);
				timeRecord.setClientname(clientname);
				timeRecord.setClientaddress(clientaddress);
				timeRecord.setJobname(jobname);
				timeRecord.setIsApproved(is_approve);
				timeRecord.setIsManuallyAdded(is_manually_added);

				// // get the time difference between intime and out time
				// TimeInHoursAndMinutes = utils.timeDifference(dateStart,
				// dateStop);
				breaksArray = getBreaksArrayListForId(id);
				timeRecord.setBreaksList(breaksArray);
				// get breaks for time sheet id
				long totalBreaksTime = getBreaksForId(id);

				// TimeInHoursAndMinutes =
				// utils.timeDifferenceBreaksAndWorkTime(
				// dateStart, dateStop, totalBreaksTime);

				if (Global.getInstance().getBooleanType(
						TimeRecordsActivity.this, CONSTANTS.INCLUDE_BREAKS)) {
					/******* the breaks are allowed in time sheet *******************/
					TimeInHoursAndMinutes = utils
							.timeDifferenceBreaksAndWorkTime(dateStart,
									dateStop, totalBreaksTime);
				} else {

					/******* the breaks are not allowed in time sheet *******************/
					TimeInHoursAndMinutes = utils.timeDifference(dateStart,
							dateStop);
				}

				timeInHours = timeInHours
						+ Integer.parseInt(TimeInHoursAndMinutes[0]);
				timeInMinutes = timeInMinutes
						+ Integer.parseInt(TimeInHoursAndMinutes[1]);
				totalInSeconds = totalInSeconds
						+ Integer.parseInt(TimeInHoursAndMinutes[2]);

				// rounded time value

				if (Global
						.getInstance()
						.getPreferenceVal(TimeRecordsActivity.this,
								CONSTANTS.ROUNDING_VALUE).equalsIgnoreCase("")
						|| Global
								.getInstance()
								.getPreferenceVal(TimeRecordsActivity.this,
										CONSTANTS.ROUNDING_VALUE)
								.equalsIgnoreCase("0")) {
					timeInRoundUpValue = Integer
							.parseInt(TimeInHoursAndMinutes[1]);
				} else {
					timeInRoundUpValue = utils.roundUp(
							Integer.parseInt(TimeInHoursAndMinutes[1]),
							TimeRecordsActivity.this);
				}

				if (userid.equalsIgnoreCase(Global.getInstance()
						.getPreferenceVal(TimeRecordsActivity.this,
								RequestParameters.USER_ID))) {

					if (!Global
							.getInstance()
							.getPreferenceVal(TimeRecordsActivity.this,
									CONSTANTS.HOURLY_RATE).equalsIgnoreCase("")) {
						timeRecord.setUserhourlyrate(Global.getInstance()
								.getPreferenceVal(TimeRecordsActivity.this,
										CONSTANTS.HOURLY_RATE));

						// pay_amount_forMinutes = (Integer.parseInt(Global
						// .getInstance().getPreferenceVal(
						// TimeRecordsActivity.this,
						// CONSTANTS.HOURLY_RATE)) / 60)
						// * Integer.parseInt(TimeInHoursAndMinutes[1]);

						pay_amount_forMinutesinDouble = ((double) ((double) (Integer
								.parseInt(Global.getInstance()
										.getPreferenceVal(
												TimeRecordsActivity.this,
												CONSTANTS.HOURLY_RATE))) / 60) * timeInRoundUpValue);

						BigDecimal value = new BigDecimal(
								pay_amount_forMinutesinDouble);
						value = value.setScale(2, RoundingMode.UP);

						pay_amount_forMinutes = value.doubleValue();

						pay_amount = (double) ((double) Integer.parseInt(Global
								.getInstance().getPreferenceVal(
										TimeRecordsActivity.this,
										CONSTANTS.HOURLY_RATE)))
								* Integer.parseInt(TimeInHoursAndMinutes[0]);

						BigDecimal value1 = new BigDecimal(pay_amount);
						value1 = value1.setScale(2, RoundingMode.UP);

						pay_amount = value1.doubleValue();

					} else {
						timeRecord.setUserhourlyrate(userhourlyrate);

						// pay_amount_forMinutes = (Integer
						// .parseInt(userhourlyrate) / 60)
						// * Integer.parseInt(TimeInHoursAndMinutes[1]);

						pay_amount_forMinutesinDouble = ((double) ((double) (Integer
								.parseInt(userhourlyrate)) / 60) * timeInRoundUpValue);

						BigDecimal value = new BigDecimal(
								pay_amount_forMinutesinDouble);
						value = value.setScale(2, RoundingMode.UP);

						pay_amount_forMinutes = value.doubleValue();

						pay_amount = (double) ((double) Integer
								.parseInt(userhourlyrate))
								* Integer.parseInt(TimeInHoursAndMinutes[0]);

						BigDecimal value1 = new BigDecimal(pay_amount);
						value1 = value1.setScale(2, RoundingMode.UP);

						pay_amount = value1.doubleValue();
					}

				} else {

					timeRecord.setUserhourlyrate(userhourlyrate);

					// pay_amount_forMinutes = (Integer.parseInt(userhourlyrate)
					// / 60)
					// * Integer.parseInt(TimeInHoursAndMinutes[1]);

					pay_amount_forMinutesinDouble = ((double) ((double) (Integer
							.parseInt(userhourlyrate)) / 60) * timeInRoundUpValue);

					BigDecimal value = new BigDecimal(
							pay_amount_forMinutesinDouble);
					value = value.setScale(2, RoundingMode.UP);

					pay_amount_forMinutes = value.doubleValue();

					pay_amount = (double) ((double) Integer
							.parseInt(userhourlyrate))
							* Integer.parseInt(TimeInHoursAndMinutes[0]);

					BigDecimal value1 = new BigDecimal(pay_amount);
					value1 = value1.setScale(2, RoundingMode.UP);

					pay_amount = value1.doubleValue();

				}

				double totalPayAmountForEmployee = pay_amount
						+ pay_amount_forMinutes;

				BigDecimal value = new BigDecimal(totalPayAmountForEmployee);
				value = value.setScale(2, RoundingMode.UP);

				totalPayAmountForEmployee = value.doubleValue();

				totalAmountForRecords = totalAmountForRecords
						+ totalPayAmountForEmployee;

				timeRecord.setTotalAmount(totalPayAmountForEmployee + "");

				// timeRecord.setTotalHours(TimeInHoursAndMinutes[0] + ":"
				// + TimeInHoursAndMinutes[1] + ":"
				// + TimeInHoursAndMinutes[2]);

				timeRecord.setTotalHours(String.format("%02d",
						Integer.parseInt(TimeInHoursAndMinutes[0]))
						+ ":" + String.format("%02d", timeInRoundUpValue));

				// timeRecord.setTotalHours(TimeInHoursAndMinutes[0] + ":"
				// + timeInRoundUpValue);

				timeRecordsList.add(timeRecord);

				db.insertExportRecords(
						created,
						name,
						clientname,
						jobname,
						intime,
						outtime,
						String.format("%02d",
								Integer.parseInt(TimeInHoursAndMinutes[0]))
								+ ":"
								+ String.format("%02d", timeInRoundUpValue),
						String.valueOf(totalPayAmountForEmployee),
						mManuallyAdded);

			} while (trDetails.moveToNext());
		}
		trDetails.close();
		db.close();

		String minutesAndSeconds[] = utils
				.formatMinutesAndSeconds(totalInSeconds);

		String HoursAndMinutes[] = utils.formatHoursAndMinutes(timeInMinutes
				+ Integer.parseInt(minutesAndSeconds[0]));

		int timeTotalHoursAndMinutes = timeInHours
				+ Integer.parseInt(HoursAndMinutes[0]);

		BigDecimal value2 = new BigDecimal(totalAmountForRecords);
		value2 = value2.setScale(2, RoundingMode.UP);

		totalAmountForRecords = value2.doubleValue();

		pay_count.setText("$" + totalAmountForRecords);

		// time_count.setText(String.valueOf(timeTotalHoursAndMinutes) + ":"
		// + HoursAndMinutes[1] + ":" + minutesAndSeconds[1]);

		time_count
				.setText(String.format("%02d", timeTotalHoursAndMinutes)
						+ ":"
						+ String.format("%02d",
								Integer.parseInt(HoursAndMinutes[1]))
						+ ":"
						+ String.format("%02d",
								Integer.parseInt(minutesAndSeconds[1])));

		records_count.setText(timeRecordsList.size() + " Records");
	}

	public void getTimeRecords() {
		timeRecordsList = new ArrayList<TimeRecords>();

		ArrayList<Breaks> breaksArray = new ArrayList<Breaks>();

		timeInHours = 0;
		timeInMinutes = 0;
		totalInSeconds = 0;
		totalAmountForRecords = 0;
		String mManuallyAdded = null;

		int timeInRoundUpValue = 0;

		db.open();

		// delete previously saved records for new csv file generatin
		db.deleteExportTimeRecords();

		// get selected time records
		Cursor trDetails = db.getTimeRecords();
		if (trDetails.moveToFirst()) {
			do {

				double pay_amount_forMinutesinDouble = 0;

				double pay_amount_forMinutes = 0;

				double pay_amount = 0;

				String name = trDetails.getString(0);
				String email = trDetails.getString(1);

				String address = trDetails.getString(2);

				String password = trDetails.getString(3);

				String phoneno = trDetails.getString(4);

				String userhourlyrate = trDetails.getString(5);

				String devicetoken = trDetails.getString(6);

				String online = trDetails.getString(7);

				String status = trDetails.getString(8);

				String id = trDetails.getString(9);

				String userid = trDetails.getString(10);
				String clientid = trDetails.getString(11);

				String jobtypeid = trDetails.getString(12);

				String intime = trDetails.getString(13);

				String indate = trDetails.getString(14);

				String outtime = trDetails.getString(15);

				String outdate = trDetails.getString(16);

				String startlat = trDetails.getString(17);

				String startlng = trDetails.getString(18);

				String endlat = trDetails.getString(19);

				String endlng = trDetails.getString(20);

				String active = trDetails.getString(21);

				String created = trDetails.getString(22);

				String clientname = trDetails.getString(23);

				String clientaddress = trDetails.getString(24);

				String jobname = trDetails.getString(25);

				String is_approve = trDetails.getString(26);

				String is_manually_added = trDetails.getString(27);

				String dateStart = indate + " " + intime;
				String dateStop = outdate + " " + outtime;

				if (is_manually_added.equalsIgnoreCase("1")) {
					mManuallyAdded = "YES";
				} else {
					mManuallyAdded = "NO";
				}

				TimeRecords timeRecord = new TimeRecords();
				timeRecord.setName(name);
				timeRecord.setEmail(email);
				timeRecord.setAddress(address);
				timeRecord.setPhoneno(phoneno);

				timeRecord.setDevicetoken(devicetoken);
				timeRecord.setOnline(online);
				timeRecord.setStatus(status);
				timeRecord.setId(id);
				timeRecord.setUserid(userid);

				timeRecord.setClientid(clientid);
				timeRecord.setJobtypeid(jobtypeid);
				timeRecord.setIntime(intime);
				timeRecord.setIndate(indate);
				timeRecord.setOuttime(outtime);
				timeRecord.setOutdate(outdate);
				timeRecord.setStartlat(startlat);
				timeRecord.setStartlng(startlng);
				timeRecord.setEndlat(endlat);
				timeRecord.setEndlng(endlng);
				timeRecord.setActive(active);
				timeRecord.setCreated(created);
				timeRecord.setClientname(clientname);
				timeRecord.setClientaddress(clientaddress);
				timeRecord.setJobname(jobname);
				timeRecord.setIsApproved(is_approve);
				timeRecord.setIsManuallyAdded(is_manually_added);

				// get the time difference between intime and out time
				// TimeInHoursAndMinutes = utils.timeDifference(dateStart,
				// dateStop);

				breaksArray = getBreaksArrayListForId(id);
				timeRecord.setBreaksList(breaksArray);

				// get breaks for time sheet id
				long totalBreaksTime = getBreaksForId(id);

				if (Global.getInstance().getBooleanType(
						TimeRecordsActivity.this, CONSTANTS.INCLUDE_BREAKS)) {
					/******* the breaks are allowed in time sheet *******************/
					TimeInHoursAndMinutes = utils
							.timeDifferenceBreaksAndWorkTime(dateStart,
									dateStop, totalBreaksTime);
				} else {

					/******* the breaks are not allowed in time sheet *******************/
					TimeInHoursAndMinutes = utils.timeDifference(dateStart,
							dateStop);
				}

				timeInHours = timeInHours
						+ Integer.parseInt(TimeInHoursAndMinutes[0]);
				timeInMinutes = timeInMinutes
						+ Integer.parseInt(TimeInHoursAndMinutes[1]);
				totalInSeconds = totalInSeconds
						+ Integer.parseInt(TimeInHoursAndMinutes[2]);

				// rounded time value

				if (Global
						.getInstance()
						.getPreferenceVal(TimeRecordsActivity.this,
								CONSTANTS.ROUNDING_VALUE).equalsIgnoreCase("")
						|| Global
								.getInstance()
								.getPreferenceVal(TimeRecordsActivity.this,
										CONSTANTS.ROUNDING_VALUE)
								.equalsIgnoreCase("0")) {
					timeInRoundUpValue = Integer
							.parseInt(TimeInHoursAndMinutes[1]);
				} else {
					timeInRoundUpValue = utils.roundUp(
							Integer.parseInt(TimeInHoursAndMinutes[1]),
							TimeRecordsActivity.this);
				}

				if (userid.equalsIgnoreCase(Global.getInstance()
						.getPreferenceVal(TimeRecordsActivity.this,
								RequestParameters.USER_ID))) {

					if (!Global
							.getInstance()
							.getPreferenceVal(TimeRecordsActivity.this,
									CONSTANTS.HOURLY_RATE).equalsIgnoreCase("")) {
						timeRecord.setUserhourlyrate(Global.getInstance()
								.getPreferenceVal(TimeRecordsActivity.this,
										CONSTANTS.HOURLY_RATE));

						// pay_amount_forMinutes = (Integer.parseInt(Global
						// .getInstance().getPreferenceVal(
						// TimeRecordsActivity.this,
						// CONSTANTS.HOURLY_RATE)) / 60)
						// * Integer.parseInt(TimeInHoursAndMinutes[1]);

						pay_amount_forMinutesinDouble = ((double) ((double) (Integer
								.parseInt(Global.getInstance()
										.getPreferenceVal(
												TimeRecordsActivity.this,
												CONSTANTS.HOURLY_RATE))) / 60) * timeInRoundUpValue);

						BigDecimal value = new BigDecimal(
								pay_amount_forMinutesinDouble);
						value = value.setScale(2, RoundingMode.UP);

						pay_amount_forMinutes = value.doubleValue();

						pay_amount = (double) ((double) Integer.parseInt(Global
								.getInstance().getPreferenceVal(
										TimeRecordsActivity.this,
										CONSTANTS.HOURLY_RATE)))
								* Integer.parseInt(TimeInHoursAndMinutes[0]);

						BigDecimal value1 = new BigDecimal(pay_amount);
						value1 = value1.setScale(2, RoundingMode.UP);

						pay_amount = value1.doubleValue();

					} else {
						timeRecord.setUserhourlyrate(userhourlyrate);

						// pay_amount_forMinutes = (Integer
						// .parseInt(userhourlyrate) / 60)
						// * Integer.parseInt(TimeInHoursAndMinutes[1]);

						pay_amount_forMinutesinDouble = ((double) ((double) (Integer
								.parseInt(userhourlyrate)) / 60) * timeInRoundUpValue);

						BigDecimal value = new BigDecimal(
								pay_amount_forMinutesinDouble);
						value = value.setScale(2, RoundingMode.UP);

						pay_amount_forMinutes = value.doubleValue();

						pay_amount = (double) ((double) Integer
								.parseInt(userhourlyrate))
								* Integer.parseInt(TimeInHoursAndMinutes[0]);

						BigDecimal value1 = new BigDecimal(pay_amount);
						value1 = value1.setScale(2, RoundingMode.UP);

						pay_amount = value1.doubleValue();
					}

				} else {

					timeRecord.setUserhourlyrate(userhourlyrate);

					// pay_amount_forMinutes = (Integer.parseInt(userhourlyrate)
					// / 60)
					// * Integer.parseInt(TimeInHoursAndMinutes[1]);

					pay_amount_forMinutesinDouble = ((double) ((double) (Integer
							.parseInt(userhourlyrate)) / 60) * timeInRoundUpValue);

					BigDecimal value = new BigDecimal(
							pay_amount_forMinutesinDouble);
					value = value.setScale(2, RoundingMode.UP);

					pay_amount_forMinutes = value.doubleValue();

					pay_amount = (double) ((double) Integer
							.parseInt(userhourlyrate))
							* Integer.parseInt(TimeInHoursAndMinutes[0]);

					BigDecimal value1 = new BigDecimal(pay_amount);
					value1 = value1.setScale(2, RoundingMode.UP);

					pay_amount = value1.doubleValue();

				}

				double totalPayAmountForEmployee = pay_amount
						+ pay_amount_forMinutes;

				BigDecimal value = new BigDecimal(totalPayAmountForEmployee);
				value = value.setScale(2, RoundingMode.UP);

				totalPayAmountForEmployee = value.doubleValue();

				totalAmountForRecords = totalAmountForRecords
						+ totalPayAmountForEmployee;

				timeRecord.setTotalAmount(totalPayAmountForEmployee + "");

				// timeRecord.setTotalHours(TimeInHoursAndMinutes[0] + ":"
				// + TimeInHoursAndMinutes[1] + ":"
				// + TimeInHoursAndMinutes[2]);

				timeRecord.setTotalHours(String.format("%02d",
						Integer.parseInt(TimeInHoursAndMinutes[0]))
						+ ":" + String.format("%02d", timeInRoundUpValue));

				// timeRecord.setTotalHours(TimeInHoursAndMinutes[0] + ":"
				// + timeInRoundUpValue);

				timeRecordsList.add(timeRecord);

				db.insertExportRecords(
						created,
						name,
						clientname,
						jobname,
						intime,
						outtime,
						String.format("%02d",
								Integer.parseInt(TimeInHoursAndMinutes[0]))
								+ ":"
								+ String.format("%02d", timeInRoundUpValue),
						String.valueOf(totalPayAmountForEmployee),
						mManuallyAdded);

			} while (trDetails.moveToNext());
		}
		trDetails.close();
		db.close();

		String minutesAndSeconds[] = utils
				.formatMinutesAndSeconds(totalInSeconds);

		String HoursAndMinutes[] = utils.formatHoursAndMinutes(timeInMinutes
				+ Integer.parseInt(minutesAndSeconds[0]));

		int timeTotalHoursAndMinutes = timeInHours
				+ Integer.parseInt(HoursAndMinutes[0]);

		BigDecimal value2 = new BigDecimal(totalAmountForRecords);
		value2 = value2.setScale(2, RoundingMode.UP);

		totalAmountForRecords = value2.doubleValue();

		pay_count.setText("$" + totalAmountForRecords);

		// time_count.setText(String.valueOf(timeTotalHoursAndMinutes) + ":"
		// + HoursAndMinutes[1] + ":" + minutesAndSeconds[1]);

		time_count
				.setText(String.format("%02d", timeTotalHoursAndMinutes)
						+ ":"
						+ String.format("%02d",
								Integer.parseInt(HoursAndMinutes[1]))
						+ ":"
						+ String.format("%02d",
								Integer.parseInt(minutesAndSeconds[1])));

		records_count.setText(timeRecordsList.size() + " Records");

	}

	private long getBreaksForId(String tr_id) {
		// db = new DataBaseHelper(TimeRecordsActivity.this);

		long difference;
		long totalBreaksDifference = 0;

		ArrayList<Breaks> breaksArray = new ArrayList<Breaks>();

		// db.open();
		Cursor breakDetails = db.getBreaksForTimeSheet(tr_id);

		if (breakDetails.moveToFirst()) {
			do {

				String startBreakTime = breakDetails.getString(0);
				String startBreakDate = breakDetails.getString(1);
				String endBreakTime = breakDetails.getString(2);
				String endBreakDate = breakDetails.getString(3);

				Breaks breaks = new Breaks();
				breaks.setStartBreakTime(startBreakTime);
				breaks.setStartBreakDate(startBreakDate);
				breaks.setEndBreakTime(endBreakTime);
				breaks.setEndBreakDate(endBreakDate);

				breaksArray.add(breaks);

				difference = utils.timeDifferenceBetweenBreaks(startBreakDate
						+ " " + startBreakTime, endBreakDate + " "
						+ endBreakTime);

				totalBreaksDifference = totalBreaksDifference + difference;

			} while (breakDetails.moveToNext());
		}
		breakDetails.close();
		// db.close();
		return totalBreaksDifference;
	}

	private ArrayList<Breaks> getBreaksArrayListForId(String tr_id) {
		// db = new DataBaseHelper(TimeRecordsActivity.this);

		ArrayList<Breaks> breaksArray = new ArrayList<Breaks>();

		// db.open();
		Cursor breakDetails = db.getBreaksForTimeSheet(tr_id);

		if (breakDetails.moveToFirst()) {
			do {

				String startBreakTime = breakDetails.getString(0);
				String startBreakDate = breakDetails.getString(1);
				String endBreakTime = breakDetails.getString(2);
				String endBreakDate = breakDetails.getString(3);

				Breaks breaks = new Breaks();
				breaks.setStartBreakTime(startBreakTime);
				breaks.setStartBreakDate(startBreakDate);
				breaks.setEndBreakTime(endBreakTime);
				breaks.setEndBreakDate(endBreakDate);

				breaksArray.add(breaks);

			} while (breakDetails.moveToNext());
		}
		breakDetails.close();
		// db.close();
		return breaksArray;

	}

	/********* to expoert the database to make csv file in required coloumns ***********/
	public class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
		private final ProgressDialog dialog = new ProgressDialog(
				TimeRecordsActivity.this);

		@Override
		protected void onPreExecute() {
			this.dialog.setMessage("Exporting time records...");
			this.dialog.show();
		}

		protected Boolean doInBackground(final String... args) {
			File dbFile = getDatabasePath("myDatabase.db");
			System.out.println(dbFile); // displays the data base path in your
										// logcat
			File exportDir = new File(
					Environment.getExternalStorageDirectory(), "");

			if (!exportDir.exists()) {
				exportDir.mkdirs();
			}

			/****** selecting the fields and titles and values will be returned *********/
			File file = new File(exportDir, "time_sheets.csv");
			try {

				file.createNewFile();
				CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
				Cursor curCSV = myDatabase.rawQuery("select * from "
						+ Table_Name, null);

				String arrStr1[] = { "Date", "Name", "Client/Project", "Job",
						"Clock On", "Clock Off", "Hours", "Pay",
						"Manually Added" };
				csvWrite.writeNext(arrStr1);
				while (curCSV.moveToNext()) {
					String arrStr[] = { curCSV.getString(1),
							curCSV.getString(2), curCSV.getString(3),
							curCSV.getString(4), curCSV.getString(5),
							curCSV.getString(6), curCSV.getString(7),
							curCSV.getString(8), curCSV.getString(9) };
					// curCSV.getString(3),curCSV.getString(4)};
					csvWrite.writeNext(arrStr);
				}
				csvWrite.close();
				curCSV.close();
				return true;
			} catch (SQLException sqlEx) {
				Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
				return false;
			} catch (IOException e) {
				Log.e("MainActivity", e.getMessage(), e);
				return false;
			}
		}

		protected void onPostExecute(final Boolean success) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (success) {
				// Toast.makeText(ExportContactsActivity.this,
				// "Export successful!", Toast.LENGTH_SHORT).show();
				// myDatabase.close();
			} else {
				// Toast.makeText(ExportContactsActivity.this, "Export failed",
				// Toast.LENGTH_SHORT).show();
			}
		}

	}

	private class GetAllTimeRecords extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.GET_ALL_TIME_RECORDS));
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								TimeRecordsActivity.this,
								RequestParameters.USER_ID)));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			try {
				JSONObject trObject = jsonResponse.getJSONObject("response");

				String success = trObject.getString("success");
				String message = trObject.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("success"))) {
					JSONArray timeRecordsArray = trObject
							.getJSONArray("timerecords");

					db.open();
					db.deleteAllTimeRecords();
					db.deleteBreaksForAllTimeRecords();
					db.close();

					String outdate;
					String outtime;

					for (int i = 0; i < timeRecordsArray.length(); i++) {
						JSONObject timeRecordDetails = timeRecordsArray
								.getJSONObject(i);

						String name = timeRecordDetails.getString("name");
						String email = timeRecordDetails.getString("email");

						String address = timeRecordDetails.getString("address");

						String password = timeRecordDetails
								.getString("password");

						String phoneno = timeRecordDetails.getString("phoneno");

						String userhourlyrate = timeRecordDetails
								.getString("userhourlyrate");

						String devicetoken = timeRecordDetails
								.getString("devicetoken");

						String online = timeRecordDetails.getString("online");

						String status = timeRecordDetails.getString("status");

						String id = timeRecordDetails.getString("id");

						String userid = timeRecordDetails.getString("userid");
						String clientid = timeRecordDetails
								.getString("clientid");

						String jobtypeid = timeRecordDetails
								.getString("jobtypeid");

						String workingEmployeeInTimeGMT = timeRecordDetails
								.getString("intime");
						String workingEmployeeInDateGMT = timeRecordDetails
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String intime = IntimeAndDate[1];
						String indate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = timeRecordDetails
								.getString("outtime");
						String workingEmployeeOutDateGMT = timeRecordDetails
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							outtime = outTimeAndDate[1];

							outdate = outTimeAndDate[0];
						} else {
							outtime = workingEmployeeOutTimeGMT;

							outdate = workingEmployeeOutDateGMT;
						}

						String createdGMT = timeRecordDetails
								.getString("created");

						String created = utils
								.convertGMTtoLocalTime(createdGMT);

						String startlat = timeRecordDetails
								.getString("startlat");

						String startlng = timeRecordDetails
								.getString("startlng");

						String endlat = timeRecordDetails.getString("endlat");

						String endlng = timeRecordDetails.getString("endlng");

						String active = timeRecordDetails.getString("active");

						String clientname = timeRecordDetails
								.getString("clientname");

						String clientaddress = timeRecordDetails
								.getString("clientaddress");

						String jobname = timeRecordDetails.getString("jobname");

						String isApproved = timeRecordDetails
								.getString("isapprove");

						String isManuallyAdded = timeRecordDetails
								.getString("ismanuallyadded");

						// inserting all the time records into the database
						db.open();
						db.insertTimeRecords(name, email, address, password,
								phoneno, userhourlyrate, devicetoken, online,
								status, id, userid, clientid, jobtypeid,
								intime, indate, outtime, outdate, startlat,
								startlng, endlat, endlng, active, created,
								clientname, clientaddress, jobname, isApproved,
								isManuallyAdded);
						db.close();

						JSONArray breaksArray = timeRecordDetails
								.getJSONArray("breaks");

						for (int j = 0; j < breaksArray.length(); j++) {

							JSONObject break_ts = breaksArray.getJSONObject(j);

							String break_tr_id = break_ts.getString("trid");
							String start_break_timeGMT = break_ts
									.getString("startbreaktime");
							String start_break_dateGMT = break_ts
									.getString("startbreakdate");
							String end_break_timeGMT = break_ts
									.getString("endbreaktime");
							String end_break_dateGMT = break_ts
									.getString("endbreakdate");

							if (!start_break_timeGMT.equalsIgnoreCase("null")
									&& !start_break_dateGMT
											.equalsIgnoreCase("null")
									&& !end_break_timeGMT
											.equalsIgnoreCase("null")
									&& !end_break_dateGMT
											.equalsIgnoreCase("null")) {

								String startBreak = utils
										.convertGMTtoLocalTime(start_break_dateGMT
												+ " " + start_break_timeGMT);

								String startBreakLocal[] = startBreak
										.split(" ");
								String start_break_time = startBreakLocal[1];
								String start_break_date = startBreakLocal[0];

								String endBreak = utils
										.convertGMTtoLocalTime(end_break_dateGMT
												+ " " + end_break_timeGMT);

								String endBreakLocal[] = endBreak.split(" ");

								String end_break_time = endBreakLocal[1];
								String end_break_date = endBreakLocal[0];

								db.open();

								db.insertBreaksForTID(break_tr_id,
										start_break_time, start_break_date,
										end_break_time, end_break_date);

								db.close();

							}

						}

					}

					// get all the records and display
					getTimeRecords();

					// Intent intent = new Intent(HomeActivity.this,
					// TimeRecordsActivity.class);
					// startActivity(intent);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

}
