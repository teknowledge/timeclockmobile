package com.timeclockmobile.data;

public class Clients {

	public String mClientId;
	public String mEmployerId;
	public String mClientName;
	public String mClientAddress;
	public String mActive;

	public String getmClientId() {
		return mClientId;
	}

	public void setmClientId(String mClientId) {
		this.mClientId = mClientId;
	}

	public String getmEmployerId() {
		return mEmployerId;
	}

	public void setmEmployerId(String mEmployerId) {
		this.mEmployerId = mEmployerId;
	}

	public String getmClientName() {
		return mClientName;
	}

	public void setmClientName(String mClientName) {
		this.mClientName = mClientName;
	}

	public String getmClientAddress() {
		return mClientAddress;
	}

	public void setmClientAddress(String mClientAddress) {
		this.mClientAddress = mClientAddress;
	}

	public String getmActive() {
		return mActive;
	}

	public void setmActive(String mActive) {
		this.mActive = mActive;
	}

}
