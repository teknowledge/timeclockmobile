package com.timeclockmobile.data;

public class WorkingEmployees {

	public String working_employee_name;
	public String working_employee_email;
	public String working_employee_address;
	public String working_employee_password;
	public String working_employee_phoneno;
	public String working_employee_hourly_rate;
	public String working_employee_device_token;
	public String working_employee_online;
	public String working_employee_status;
	public String working_employee_id;
	public String working_employee_user_id;
	public String working_employee_client_id;
	public String working_employee_jobtype_id;

	public String getWorking_employee_name() {
		return working_employee_name;
	}

	public void setWorking_employee_name(String working_employee_name) {
		this.working_employee_name = working_employee_name;
	}

	public String getWorking_employee_email() {
		return working_employee_email;
	}

	public void setWorking_employee_email(String working_employee_email) {
		this.working_employee_email = working_employee_email;
	}

	public String getWorking_employee_address() {
		return working_employee_address;
	}

	public void setWorking_employee_address(String working_employee_address) {
		this.working_employee_address = working_employee_address;
	}

	public String getWorking_employee_password() {
		return working_employee_password;
	}

	public void setWorking_employee_password(String working_employee_password) {
		this.working_employee_password = working_employee_password;
	}

	public String getWorking_employee_phoneno() {
		return working_employee_phoneno;
	}

	public void setWorking_employee_phoneno(String working_employee_phoneno) {
		this.working_employee_phoneno = working_employee_phoneno;
	}

	public String getWorking_employee_hourly_rate() {
		return working_employee_hourly_rate;
	}

	public void setWorking_employee_hourly_rate(
			String working_employee_hourly_rate) {
		this.working_employee_hourly_rate = working_employee_hourly_rate;
	}

	public String getWorking_employee_device_token() {
		return working_employee_device_token;
	}

	public void setWorking_employee_device_token(
			String working_employee_device_token) {
		this.working_employee_device_token = working_employee_device_token;
	}

	public String getWorking_employee_online() {
		return working_employee_online;
	}

	public void setWorking_employee_online(String working_employee_online) {
		this.working_employee_online = working_employee_online;
	}

	public String getWorking_employee_status() {
		return working_employee_status;
	}

	public void setWorking_employee_status(String working_employee_status) {
		this.working_employee_status = working_employee_status;
	}

	public String getWorking_employee_id() {
		return working_employee_id;
	}

	public void setWorking_employee_id(String working_employee_id) {
		this.working_employee_id = working_employee_id;
	}

	public String getWorking_employee_user_id() {
		return working_employee_user_id;
	}

	public void setWorking_employee_user_id(String working_employee_user_id) {
		this.working_employee_user_id = working_employee_user_id;
	}

	public String getWorking_employee_client_id() {
		return working_employee_client_id;
	}

	public void setWorking_employee_client_id(String working_employee_client_id) {
		this.working_employee_client_id = working_employee_client_id;
	}

	public String getWorking_employee_jobtype_id() {
		return working_employee_jobtype_id;
	}

	public void setWorking_employee_jobtype_id(
			String working_employee_jobtype_id) {
		this.working_employee_jobtype_id = working_employee_jobtype_id;
	}

	public String getWorking_employee_intime() {
		return working_employee_intime;
	}

	public void setWorking_employee_intime(String working_employee_intime) {
		this.working_employee_intime = working_employee_intime;
	}

	public String getWorking_employee_indate() {
		return working_employee_indate;
	}

	public void setWorking_employee_indate(String working_employee_indate) {
		this.working_employee_indate = working_employee_indate;
	}

	public String getWorking_employee_outtime() {
		return working_employee_outtime;
	}

	public void setWorking_employee_outtime(String working_employee_outtime) {
		this.working_employee_outtime = working_employee_outtime;
	}

	public String getWorking_employee_outdate() {
		return working_employee_outdate;
	}

	public void setWorking_employee_outdate(String working_employee_outdate) {
		this.working_employee_outdate = working_employee_outdate;
	}

	public String getWorking_employee_startlat() {
		return working_employee_startlat;
	}

	public void setWorking_employee_startlat(String working_employee_startlat) {
		this.working_employee_startlat = working_employee_startlat;
	}

	public String getWorking_employee_startlng() {
		return working_employee_startlng;
	}

	public void setWorking_employee_startlng(String working_employee_startlng) {
		this.working_employee_startlng = working_employee_startlng;
	}

	public String getWorking_employee_endlat() {
		return working_employee_endlat;
	}

	public void setWorking_employee_endlat(String working_employee_endlat) {
		this.working_employee_endlat = working_employee_endlat;
	}

	public String getWorking_employee_endlng() {
		return working_employee_endlng;
	}

	public void setWorking_employee_endlng(String working_employee_endlng) {
		this.working_employee_endlng = working_employee_endlng;
	}

	public String getWorking_employee_active() {
		return working_employee_active;
	}

	public void setWorking_employee_active(String working_employee_active) {
		this.working_employee_active = working_employee_active;
	}

	public String getWorking_employee_created() {
		return working_employee_created;
	}

	public void setWorking_employee_created(String working_employee_created) {
		this.working_employee_created = working_employee_created;
	}

	public String getWorking_employee_clientname() {
		return working_employee_clientname;
	}

	public void setWorking_employee_clientname(
			String working_employee_clientname) {
		this.working_employee_clientname = working_employee_clientname;
	}

	public String getWorking_employee_client_address() {
		return working_employee_client_address;
	}

	public void setWorking_employee_client_address(
			String working_employee_client_address) {
		this.working_employee_client_address = working_employee_client_address;
	}

	public String getWorking_employee_job_name() {
		return working_employee_job_name;
	}

	public void setWorking_employee_job_name(String working_employee_job_name) {
		this.working_employee_job_name = working_employee_job_name;
	}

	public String working_employee_intime;
	public String working_employee_indate;
	public String working_employee_outtime;
	public String working_employee_outdate;
	public String working_employee_startlat;
	public String working_employee_startlng;
	public String working_employee_endlat;
	public String working_employee_endlng;
	public String working_employee_active;
	public String working_employee_created;
	public String working_employee_clientname;
	public String working_employee_client_address;
	public String working_employee_job_name;

}
