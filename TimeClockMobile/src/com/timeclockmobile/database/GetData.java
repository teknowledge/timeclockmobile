package com.timeclockmobile.database;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;

import com.timeclockmobile.data.WorkingEmployees;

public class GetData {

	public DataBaseHelper db;

	public ArrayList<WorkingEmployees> getWorkingEmployeesDetailsFromDB(
			Context mContext) {
		// TODO Auto-generated method stub
		db = new DataBaseHelper(mContext);
		ArrayList<WorkingEmployees> workingEmployeesArrayList1 = new ArrayList<WorkingEmployees>();
		db.open();
		Cursor workingEmployeesDetails = db.getWorkingEmployeesDetails();

		if (workingEmployeesDetails.moveToFirst()) {
			do {

				String working_employee_user_id = workingEmployeesDetails
						.getString(0);
				String working_employee_client_id = workingEmployeesDetails
						.getString(1);
				String working_employee_job_type_id = workingEmployeesDetails
						.getString(2);
				String working_employee_name = workingEmployeesDetails
						.getString(3);
				String working_employee_intime = workingEmployeesDetails
						.getString(4);
				String working_employee_indate = workingEmployeesDetails
						.getString(5);
				String working_employee_clientname = workingEmployeesDetails
						.getString(6);
				String working_employee_jobname = workingEmployeesDetails
						.getString(7);

				WorkingEmployees workingEmployee = new WorkingEmployees();
				workingEmployee
						.setWorking_employee_user_id(working_employee_user_id);
				workingEmployee
						.setWorking_employee_client_id(working_employee_client_id);
				workingEmployee
						.setWorking_employee_jobtype_id(working_employee_job_type_id);
				workingEmployee.setWorking_employee_name(working_employee_name);
				workingEmployee
						.setWorking_employee_intime(working_employee_intime);
				workingEmployee
						.setWorking_employee_indate(working_employee_indate);
				workingEmployee
						.setWorking_employee_clientname(working_employee_clientname);
				workingEmployee
						.setWorking_employee_job_name(working_employee_jobname);

				workingEmployeesArrayList1.add(workingEmployee);

			} while (workingEmployeesDetails.moveToNext());
		}
		workingEmployeesDetails.close();
		db.close();
		return workingEmployeesArrayList1;
	}
}
