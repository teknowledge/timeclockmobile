package com.timeclockmobile;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.service.RepeatService;
import com.timeclockmobile.service.TimerService;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class ClockOffActivity extends Activity {
	private TextView employeeName, clientName, jobTypeName;
	private Button submit_clockOff;
	public ProgressDialog progressDialog;
	private ImageButton homeBtn;

	ClockOnActivity clockon = new ClockOnActivity();
	GpsTracker gpsTracker;
	String latitude;
	String longitude;
	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.clock_off);

		progressDialog = new ProgressDialog(ClockOffActivity.this);
		employeeName = (TextView) findViewById(R.id.employee_name);
		clientName = (TextView) findViewById(R.id.client_name);
		jobTypeName = (TextView) findViewById(R.id.job_type_name);
		submit_clockOff = (Button) findViewById(R.id.submit_clock_off);
		homeBtn = (ImageButton) findViewById(R.id.homeBtn);

		gpsTracker = new GpsTracker(this);

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(this);
		}

		if (Global.getInstance().getBooleanType(ClockOffActivity.this,
				CONSTANTS.USER_IS_WORKING)) {
			Global.getInstance().getPreferenceVal(ClockOffActivity.this,
					CONSTANTS.CLOCKED_ON_JOBTYPEID);
			Global.getInstance().getPreferenceVal(ClockOffActivity.this,
					CONSTANTS.CLOCKED_ON_CLIENTID);
			Global.getInstance().getPreferenceVal(ClockOffActivity.this,
					CONSTANTS.CLOCKED_ON_JOBNAME);
			Global.getInstance().getPreferenceVal(ClockOffActivity.this,
					CONSTANTS.CLOCKED_ON_CLIENTNAME);
			Global.getInstance().getPreferenceVal(ClockOffActivity.this,
					CONSTANTS.CLOCKED_ON_TRID);

			jobTypeName.setText(Global.getInstance().getPreferenceVal(
					ClockOffActivity.this, CONSTANTS.CLOCKED_ON_JOBNAME));

			clientName.setText(Global.getInstance().getPreferenceVal(
					ClockOffActivity.this, CONSTANTS.CLOCKED_ON_CLIENTNAME));

		} else {
			employeeName.setText(Global.getInstance().getPreferenceVal(
					ClockOffActivity.this, CONSTANTS.USER_NAME));

			jobTypeName.setText(Global.getInstance().getPreferenceVal(
					ClockOffActivity.this, CONSTANTS.JOB_NAME));

			clientName.setText(Global.getInstance().getPreferenceVal(
					ClockOffActivity.this, CONSTANTS.CLIENT_NAME));
		}

		submit_clockOff.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (utils.isConnectingToInternet(ClockOffActivity.this)) {
					new ClockOffTask().execute("");
				} else {
					utils.showDialog(ClockOffActivity.this);
				}
			}
		});

		homeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(ClockOffActivity.this,
						HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
			}
		});
	}

	private class ClockOffTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.CLOCK_OFF));

				if (Global.getInstance().getBooleanType(ClockOffActivity.this,
						CONSTANTS.USER_IS_WORKING)) {
					entity.addPart(
							RequestParameters.TIME_RECORD_ID,
							new StringBody(Global.getInstance()
									.getPreferenceVal(ClockOffActivity.this,
											CONSTANTS.CLOCKED_ON_TRID)));
				} else {
					entity.addPart(
							RequestParameters.TIME_RECORD_ID,
							new StringBody(Global.getInstance()
									.getPreferenceVal(ClockOffActivity.this,
											RequestParameters.TIME_RECORD_ID)));
				}

				entity.addPart(RequestParameters.END_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.END_LNG, new StringBody(
						longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject clockOffObject = jsonResponse
						.getJSONObject("response");
				String success = clockOffObject.getString("success");
				String message = clockOffObject.getString("message");
				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Clocked Off"))) {
					Toast.makeText(getApplicationContext(),
							"Clocked off successfully", Toast.LENGTH_SHORT)
							.show();

					// /stopTimerView();

					stopService(new Intent(ClockOffActivity.this,
							TimerService.class));

					// stop updating location service
					stopService(new Intent(ClockOffActivity.this,
							RepeatService.class));

					Global.getInstance().storeBooleanType(
							ClockOffActivity.this, CONSTANTS.CLOCKED_ON, false);

					Global.getInstance().storeBooleanType(
							ClockOffActivity.this, CONSTANTS.USER_IS_WORKING,
							false);

					Intent intent = new Intent(ClockOffActivity.this,
							HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
					finish();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void stopTimerView() {
		clockon.mTimer.cancel();

		clockon.mTimerTask.cancel();

		clockon.stopTimer();

	}
}
