package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.timeclockmobile.adapter.FieldNotesAdapter;
import com.timeclockmobile.data.FieldNotes;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class FieldNotesActivity extends Activity {

	private ListView fieldNotesListView;
	private ImageButton backBtn;
	private Button add_field_note;
	private Button delete_field_note, edit_field_note;
	private String numberOfFieldNotes;
	private Button filter_notes;

	public ProgressDialog progressDialog;

	private ArrayList<FieldNotes> FieldNotesListArray = new ArrayList<FieldNotes>();

	private FieldNotesAdapter adapter;
	ArrayList<String> mCheckedFieldNotes;
	ArrayList<FieldNotes> checkedFileNotesObject;
	private DataBaseHelper db = new DataBaseHelper(FieldNotesActivity.this);
	private ArrayList<FieldNotes> checkedFieldNotesList;

	private String where;
	private String from;
	Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.field_notes);

		progressDialog = new ProgressDialog(FieldNotesActivity.this);

		fieldNotesListView = (ListView) findViewById(R.id.fieldNotesListView);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		add_field_note = (Button) findViewById(R.id.add_field_note);
		delete_field_note = (Button) findViewById(R.id.delete_field_note);
		edit_field_note = (Button) findViewById(R.id.edit_field_note);

		filter_notes = (Button) findViewById(R.id.filter_notes);

		Intent intent = getIntent();

		from = intent.getStringExtra("from");

		if (from.equalsIgnoreCase("filter")) {
			where = intent.getStringExtra("where");

			FieldNotesListArray = getSelectedFieldNotes(where);

			loadFiledNotesList();

		} else {

			if (utils.isConnectingToInternet(FieldNotesActivity.this)) {
				// get the fieldNotes for user
				new GetFieldNotesTask().execute("");
			} else {
				utils.showDialog(FieldNotesActivity.this);
			}
		}

		add_field_note.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FieldNotesActivity.this,
						AddFieldNotesActivity.class);
				intent.putExtra("type", "add");
				startActivity(intent);
				finish();
			}
		});

		filter_notes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FieldNotesActivity.this,
						FilterFieldNotesActivity.class);
				startActivity(intent);
				finish();
			}
		});

		fieldNotesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				FieldNotes fieldNote = (FieldNotes) parent
						.getItemAtPosition(position);

				Intent intent = new Intent(FieldNotesActivity.this,
						FieldNoteDetailsActivity.class);

				intent.putExtra("employee_name", fieldNote.getName());
				intent.putExtra("client_name", fieldNote.getClientname());
				intent.putExtra("date", fieldNote.getCreated());

				intent.putExtra("field_note", fieldNote.getNote());
				startActivity(intent);
			}
		});

		edit_field_note.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (adapter != null) {

					checkedFileNotesObject = new ArrayList<FieldNotes>();

					checkedFileNotesObject = adapter.getCheckedFieldNotes();
					if (checkedFileNotesObject.size() == 1) {
						Intent intent = new Intent(FieldNotesActivity.this,
								AddFieldNotesActivity.class);
						intent.putExtra("type", "edit");
						intent.putExtra("fieldNoteId", checkedFileNotesObject
								.get(0).getId());
						intent.putExtra("EmployeeId", checkedFileNotesObject
								.get(0).getUserid());
						intent.putExtra("ClientId",
								checkedFileNotesObject.get(0).getClientid());
						intent.putExtra("fieldNote", checkedFileNotesObject
								.get(0).getNote());

						startActivity(intent);
						finish();

					} else {
						Toast.makeText(getApplicationContext(),
								"Select One Field Note", Toast.LENGTH_SHORT)
								.show();
					}
				}
			}
		});

		delete_field_note.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (adapter != null) {

					checkedFileNotesObject = new ArrayList<FieldNotes>();

					mCheckedFieldNotes = adapter
							.getArrayListOfCheckedFieldNotes();

					checkedFileNotesObject = adapter.getCheckedFieldNotes();

					numberOfFieldNotes = mCheckedFieldNotes.size() + "";

					if (mCheckedFieldNotes.size() > 0) {
						// alert dialog to delete employee

						AlertDialog.Builder alertDialog = new AlertDialog.Builder(
								FieldNotesActivity.this);

						// Setting Dialog Title
						alertDialog.setTitle("Delete Field Note(s)");

						// Setting Dialog Message
						alertDialog
								.setMessage("Are you sure you want to delete Field Note(s)?");

						// Setting Positive "Yes" Button
						alertDialog.setPositiveButton("YES",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// User pressed YES button.
										// delete employee

										if (utils
												.isConnectingToInternet(FieldNotesActivity.this)) {
											// delete field note selected
											new DeleteFieldNoteTask(
													mCheckedFieldNotes,
													checkedFileNotesObject)
													.execute("");
										} else {
											utils.showDialog(FieldNotesActivity.this);
										}

									}
								});

						// Setting Negative "NO" Button
						alertDialog.setNegativeButton("NO",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// User pressed No button.Cancel the
										// dialog
										dialog.cancel();
									}
								});

						// Showing Alert Message
						alertDialog.show();

					} else {
						Toast.makeText(getApplicationContext(),
								"Select the Field Note", Toast.LENGTH_SHORT)
								.show();
					}

				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	public void loadFiledNotesList() {
		adapter = new FieldNotesAdapter(FieldNotesActivity.this,
				R.layout.field_notes_inflate, FieldNotesListArray);

		fieldNotesListView.setAdapter(adapter);

	}

	private class GetFieldNotesTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");

			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.GET_FIELD_NOTES));
				entity.addPart(
						RequestParameters.EMPLOYER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								FieldNotesActivity.this,
								RequestParameters.USER_ID)));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				String message = jsonObject.getString("message");
				if (message.equalsIgnoreCase("Success")) {

					db.open();
					db.deleteAllFieldNotes();
					db.close();

					JSONArray fieldNotesArray = jsonObject
							.getJSONArray("fieldnotes");

					for (int i = 0; i < fieldNotesArray.length(); i++) {
						JSONObject fieldNotesObject = fieldNotesArray
								.getJSONObject(i);

						FieldNotes fieldNote = new FieldNotes();

						String name = fieldNotesObject.getString("name");
						String email = fieldNotesObject.getString("email");
						String address = fieldNotesObject.getString("address");
						String password = fieldNotesObject
								.getString("password");
						String phoneno = fieldNotesObject.getString("phoneno");
						String hourlyrate = fieldNotesObject
								.getString("hourlyrate");
						String devicetoken = fieldNotesObject
								.getString("devicetoken");
						String online = fieldNotesObject.getString("online");
						String status = fieldNotesObject.getString("status");
						String id = fieldNotesObject.getString("id");
						String userid = fieldNotesObject.getString("userid");
						String clientid = fieldNotesObject
								.getString("clientid");
						String note = fieldNotesObject.getString("note");
						String createdGMT = fieldNotesObject
								.getString("created");
						String created = utils
								.convertGMTtoLocalTime(createdGMT);

						String active = fieldNotesObject.getString("active");
						String clientname = fieldNotesObject
								.getString("clientname");
						String clientaddress = fieldNotesObject
								.getString("clientaddress");

						fieldNote.setName(name);
						fieldNote.setEmail(email);

						fieldNote.setAddress(address);
						fieldNote.setPassword(password);
						fieldNote.setPhoneno(phoneno);

						fieldNote.setHourlyrate(hourlyrate);
						fieldNote.setDevicetoken(devicetoken);
						fieldNote.setOnline(online);
						fieldNote.setStatus(status);
						fieldNote.setId(id);
						fieldNote.setUserid(userid);
						fieldNote.setClientid(clientid);
						fieldNote.setNote(note);

						fieldNote.setCreated(created);
						fieldNote.setActive(active);
						fieldNote.setClientname(clientname);
						fieldNote.setClientaddress(clientaddress);

						FieldNotesListArray.add(fieldNote);

						String dateCreated[] = created.split(" ");

						db.open();
						db.insertFieldNotes(name, email, address, password,
								phoneno, hourlyrate, devicetoken, online,
								status, id, userid, clientid, note,
								dateCreated[0], active, clientname,
								clientaddress);
						db.close();

					}

					// Loading fieldnotes list
					loadFiledNotesList();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class DeleteFieldNoteTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		private ArrayList<String> checkedFieldNotesArray;
		ArrayList<FieldNotes> mCheckedFileNotesObject;

		public DeleteFieldNoteTask(ArrayList<String> checkedFieldNotes,
				ArrayList<FieldNotes> checkedFileNotesObject) {
			mCheckedFileNotesObject = checkedFileNotesObject;
			checkedFieldNotesArray = checkedFieldNotes;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");

			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_FIELD_NOTES));
				entity.addPart(RequestParameters.NUMBER_OF_NOTES,
						new StringBody(numberOfFieldNotes));

				for (int i = 0; i < checkedFieldNotesArray.size(); i++) {
					entity.addPart(RequestParameters.FIELD_NOTE_ID + (i + 1),
							new StringBody(checkedFieldNotesArray.get(i)));

				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				String[] ids = new String[checkedFieldNotesArray.size()];
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");

				if (success.equalsIgnoreCase("1")
						&& message.equalsIgnoreCase("Success")) {
					Toast.makeText(getApplicationContext(),
							"Field Note has been deleted", Toast.LENGTH_LONG)
							.show();

					for (int i = 0; i < checkedFieldNotesArray.size(); i++) {
						ids[i] = checkedFieldNotesArray.get(i);
					}

					// deleting single time record from database
					db.open();
					db.deleteFieldNotes(ids);
					db.close();

					for (int i = 0; i < mCheckedFileNotesObject.size(); i++) {
						FieldNotesListArray.remove(mCheckedFileNotesObject
								.get(i));
					}
					// adapter.notifyDataSetChanged();
					loadFiledNotesList();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public ArrayList<FieldNotes> getSelectedFieldNotes(String where) {
		// TODO Auto-generated method stub

		ArrayList<FieldNotes> fieldNotesArrayList1 = new ArrayList<FieldNotes>();
		db.open();
		Cursor filedNotes = db.getSelectedFieldNotes(where);

		if (filedNotes.moveToFirst()) {
			do {

				String name = filedNotes.getString(0);
				String email = filedNotes.getString(1);
				String address = filedNotes.getString(2);
				String password = filedNotes.getString(3);
				String phoneno = filedNotes.getString(4);
				String hourlyRate = filedNotes.getString(5);
				String deviceToken = filedNotes.getString(6);

				String online = filedNotes.getString(7);
				String status = filedNotes.getString(8);
				String note_id = filedNotes.getString(9);
				String userId = filedNotes.getString(10);
				String clientId = filedNotes.getString(11);
				String note = filedNotes.getString(12);
				String created = filedNotes.getString(13);
				String active = filedNotes.getString(14);
				String clientName = filedNotes.getString(15);
				String clientAddress = filedNotes.getString(16);

				FieldNotes fieldNote = new FieldNotes();
				fieldNote.setName(name);
				fieldNote.setEmail(email);
				fieldNote.setAddress(address);
				fieldNote.setPassword(password);
				fieldNote.setPhoneno(phoneno);
				fieldNote.setHourlyrate(hourlyRate);
				fieldNote.setDevicetoken(deviceToken);
				fieldNote.setOnline(online);
				fieldNote.setStatus(status);
				fieldNote.setId(note_id);
				fieldNote.setUserid(userId);
				fieldNote.setClientid(clientId);
				fieldNote.setNote(note);
				fieldNote.setCreated(created);
				fieldNote.setActive(active);
				fieldNote.setClientname(clientName);
				fieldNote.setClientaddress(clientAddress);

				fieldNotesArrayList1.add(fieldNote);

			} while (filedNotes.moveToNext());
		}
		filedNotes.close();
		db.close();
		return fieldNotesArrayList1;
	}
}
