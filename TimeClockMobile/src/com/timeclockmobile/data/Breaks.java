package com.timeclockmobile.data;

import java.io.Serializable;

public class Breaks implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String startBreakTime;

	public String getStartBreakTime() {
		return startBreakTime;
	}

	public void setStartBreakTime(String startBreakTime) {
		this.startBreakTime = startBreakTime;
	}

	public String getStartBreakDate() {
		return startBreakDate;
	}

	public void setStartBreakDate(String startBreakDate) {
		this.startBreakDate = startBreakDate;
	}

	public String getEndBreakTime() {
		return endBreakTime;
	}

	public void setEndBreakTime(String endBreakTime) {
		this.endBreakTime = endBreakTime;
	}

	public String getEndBreakDate() {
		return endBreakDate;
	}

	public void setEndBreakDate(String endBreakDate) {
		this.endBreakDate = endBreakDate;
	}

	public String startBreakDate;
	public String endBreakTime;
	public String endBreakDate;

}
