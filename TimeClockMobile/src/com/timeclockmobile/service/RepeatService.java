package com.timeclockmobile.service;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class RepeatService extends Service {

	// DataBaseHelper db = null;
	public ProgressDialog progressDialog;
	GpsTracker gpsTracker;
	String latitude;
	String longitude;
	Utils utils = new Utils();

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub

		// Toast.makeText(getApplicationContext(), "Service Created", 1).show();

		progressDialog = new ProgressDialog(getApplicationContext());
		gpsTracker = new GpsTracker(getApplicationContext());

		super.onCreate();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		// Toast.makeText(getApplicationContext(), "Service Destroy", 1).show();
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		// Toast.makeText(getApplicationContext(), "Service Running ",
		// 1).show();

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(getApplicationContext());
		}
		if (utils.isConnectingToInternet(getApplicationContext())) {
			new SubmitUserLocation().execute("");
		} else {
			// utils.showDialog(getApplicationContext());
		}

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub

		throw new UnsupportedOperationException("Not yet implemented");
	}

	private class SubmitUserLocation extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// progressDialog.setMessage("Please wait....");
			// progressDialog.setCancelable(false);
			// progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.SUBMIT_USER_LOCATION));
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								getApplicationContext(),
								RequestParameters.USER_ID)));
				entity.addPart(RequestParameters.LAT, new StringBody(latitude));
				entity.addPart(RequestParameters.LNG, new StringBody(longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			// if (progressDialog != null && progressDialog.isShowing()) {
			// try {
			// progressDialog.dismiss();
			//
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			// }

			try {
				JSONObject clockOnObject = jsonResponse
						.getJSONObject("response");
				String success = clockOnObject.getString("success");
				String message = clockOnObject.getString("message");
				if ((success.equalsIgnoreCase("1"))) {
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
