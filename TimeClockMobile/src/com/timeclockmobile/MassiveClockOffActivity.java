package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class MassiveClockOffActivity extends Activity {

	private Button submit_massive_clock_off;
	private ImageButton backBtn;

	public ProgressDialog progressDialog;
	private TextView employee_name, client_name, job_type_name;
	private ArrayList<Employees> clockedOnUsersArray;
	private String numberOfUsers;
	private DataBaseHelper db = new DataBaseHelper(MassiveClockOffActivity.this);
	String names = "";
	String jobName;
	String clientName;

	GpsTracker gpsTracker;
	String latitude;
	String longitude;
	Utils utils = new Utils();

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.massive_clock_off);

		progressDialog = new ProgressDialog(MassiveClockOffActivity.this);
		employee_name = (TextView) findViewById(R.id.employee_name);
		client_name = (TextView) findViewById(R.id.client_name);
		job_type_name = (TextView) findViewById(R.id.job_type_name);
		submit_massive_clock_off = (Button) findViewById(R.id.submit_massive_clock_off);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		gpsTracker = new GpsTracker(this);

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(this);
		}

		Intent intent = getIntent();
		clockedOnUsersArray = (ArrayList<Employees>) intent
				.getSerializableExtra("ClockedOnUsersList");

		for (int i = 0; i < clockedOnUsersArray.size(); i++) {
			names = names + "\n" + clockedOnUsersArray.get(i).getName();
		}

		employee_name.setText(names);

		client_name.setText(clockedOnUsersArray.get(0).getWorking_clientName());
		job_type_name.setText(clockedOnUsersArray.get(0).getWorking_jobName());

		numberOfUsers = String.valueOf(clockedOnUsersArray.size());

		submit_massive_clock_off.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (utils.isConnectingToInternet(MassiveClockOffActivity.this)) {
					new MassiveClockOffTask().execute("");
				} else {
					utils.showDialog(MassiveClockOffActivity.this);
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}

	private class MassiveClockOffTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String ids[] = new String[clockedOnUsersArray.size()];

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.MASSIVE_CLOCK_OFF));
				entity.addPart(RequestParameters.NUMBER_OF_USERS,
						new StringBody(numberOfUsers));

				entity.addPart(RequestParameters.END_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.END_LNG, new StringBody(
						longitude));
				for (int i = 0; i < clockedOnUsersArray.size(); i++) {
					entity.addPart(RequestParameters.TIME_RECORD_ID + (i + 1),
							new StringBody(clockedOnUsersArray.get(i)
									.getWorking_tr_id()));

				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			JSONObject clockOnObject;
			try {
				clockOnObject = jsonResponse.getJSONObject("response");
				String success = clockOnObject.getString("success");
				if (success.equalsIgnoreCase("1")) {
					Toast.makeText(getApplicationContext(),
							"Clocked off successfully", Toast.LENGTH_SHORT)
							.show();

					JSONArray timeRecordsArray = clockOnObject
							.getJSONArray("timerecords");

					String outtime;
					String outdate;

					for (int i = 0; i < timeRecordsArray.length(); i++) {
						JSONObject timeRecord = timeRecordsArray
								.getJSONObject(i);

						String jobtypeid = timeRecord.getString("jobtypeid");
						String jobname = timeRecord.getString("jobname");
						String userid = timeRecord.getString("userid");

						String password = timeRecord.getString("password");
						String clientname = timeRecord.getString("clientname");
						String online = timeRecord.getString("online");

						String clientid = timeRecord.getString("clientid");
						
						
						String id = timeRecord.getString("id");

						String createdGMT = timeRecord.getString("created");
						String created = utils.convertGMTtoLocalTime(createdGMT);
						
						String name = timeRecord.getString("name");

						// String intime = timeRecord.getString("intime");
						// String indate = timeRecord.getString("indate");

						// String outtime = timeRecord.getString("outtime");
						// String outdate = timeRecord.getString("outdate");

						String workingEmployeeInTimeGMT = timeRecord
								.getString("intime");
						String workingEmployeeInDateGMT = timeRecord
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String intime = IntimeAndDate[1];
						String indate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = timeRecord
								.getString("outtime");
						String workingEmployeeOutDateGMT = timeRecord
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							outtime = outTimeAndDate[1];

							outdate = outTimeAndDate[0];
						} else {
							outtime = workingEmployeeOutTimeGMT;

							outdate = workingEmployeeOutDateGMT;
						}

						String phoneno = timeRecord.getString("phoneno");
						String endlng = timeRecord.getString("endlng");

						String userhourlyrate = timeRecord
								.getString("userhourlyrate");
						String endlat = timeRecord.getString("endlat");
						String status = timeRecord.getString("status");
						String startlat = timeRecord.getString("startlat");
						String devicetoken = timeRecord
								.getString("devicetoken");
						String startlng = timeRecord.getString("startlng");
						String address = timeRecord.getString("address");
						String email = timeRecord.getString("email");
						String active = timeRecord.getString("active");
						String clientaddress = timeRecord
								.getString("clientaddress");

						db.open();
						db.updateEmployeeDetailsForWorking(userid, "0", id,
								clientid, jobtypeid, intime, indate, startlat,
								startlng, clientname, created, endlat, endlng,
								jobname, outtime, outdate,"0");
						db.close();

					}

					// ********* delete clocked on users from database after
					// clocked off **************//

					for (int i = 0; i < clockedOnUsersArray.size(); i++) {
						ids[i] = clockedOnUsersArray.get(i).getId();

					}

					db.open();
					db.deleteDetailsOnClockOff(ids);
					db.close();

					Intent intent = new Intent(MassiveClockOffActivity.this,
							TeamManagementActivity.class);

					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
