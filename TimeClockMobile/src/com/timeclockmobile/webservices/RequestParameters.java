package com.timeclockmobile.webservices;

public class RequestParameters {

	/************* Parameters to be used for login *************/

	public static final String SESSION = "session";

	public static final String METHOD = "method";

	public static final String LOGIN_USER = "loginuser"; // method login

	public static final String PIN_NUMBER = "pinnumber";

	public static final String EMPLOYEE_NAME = "employeename";

	public static final String EMPLOYER_NAME = "employername";

	public static final String LOAD_USER_INFO = "loaduserinfo";

	/********* Parameters to be used for add client using employer id ********/

	public static final String ADD_CLIENT = "addClient"; // method add client

	public static final String EMPLOYER_ID = "employerid";

	public static final String CLIENT_NAME = "clientname";

	public static final String CLIENT_ADDRESS = "clientaddress";

	/******* Delete client parameters *******/
	public static final String DELETE_CLIENT = "deleteClient"; // method delete
																// client

	public static final String CLIENT_ID = "clientid";

	/****** Update Client parameters ********/

	// clientid, clientname, clientaddress are also the parameters

	public static final String UPDATE_CLIENT = "updateClient"; // method update
																// client

	/********** Add job type api parameters **********/

	// employerid is also one of the parameters

	public static final String ADD_JOB_TYPE = "addJobType"; // method add job
															// type

	public static final String JOB_NAME = "jobname";

	/****** Delete job type api parameters **********/

	public static final String DELETE_JOBTYPE = "deleteJobType"; // method
																	// delete
																	// job type
																	// name

	public static final String JOBTYPE_ID = "jobtypeid";

	/********** Update job type api parameters **********/

	// jobtypeid, jobname are also the parameters..

	public static final String UPDATE_JOB_TYPE = "updateJobType"; // method
																	// update
																	// job type
																	// id

	/************* get FieldNotes api parameters ************/

	// employerid is also a parameter.

	public static final String GET_FIELD_NOTES = "getFieldNotes"; // method get
																	// field
																	// notes

	/********* Add field note api parameters ***********/

	// clientid is also parameter..

	public static final String ADD_FIELD_NOTE = "addFieldNote"; // method add
																// field notes.

	public static final String USER_ID = "userid";

	public static final String NOTE = "note";

	/********* Delete field notes api parameters *********/

	public static final String DELETE_FIELD_NOTES = "deleteFieldNotes";

	public static final String NUMBER_OF_NOTES = "numberofnotes";

	public static final String FIELD_NOTE_ID = "fieldnoteid";

	/********* Update field notes api parameters ***********/

	// fieldnoteid,clientid,userid,note are also the parameters...

	public static final String UPDATE_FIELD_NOTE = "updateFieldNote";

	/********* Add employee api parameters ************/

	// employerid, pinnumber also the parameters

	public static final String ADD_EMPLOYEE = "addEmployee"; // method add
																// employee

	public static final String NAME = "name";

	public static final String EMAIL = "email";

	public static final String ADDRESS = "address";

	public static final String PHONE_NO = "phoneno";

	public static final String HOURLY_RATE = "hourlyrate";

	/********** Delete employee api parameters ************/

	public static final String DELETE_EMPLOYEE = "deleteEmployee";

	public static final String EMPLOYEE_ID = "employeeid";

	public static final String EMPLOYER_TYPE = "isemployer";

	/************ Update employee api parameters ***********/

	// employeeid,name,email,address,pinnumber,phoneno,hourlyrate are also the
	// parameters...

	public static final String UPDATE_EMPLOYEE = "updateEmployee";

	/************ getAllTimeRecords api parameters **********/

	// userid is also a parameter..

	public static final String GET_ALL_TIME_RECORDS = "getAllTimeRecords";

	/************ add TimeRecord for userid *************/

	// userid, clientid, jobtypeid are also the parameters.....

	public static final String ADD_TIME_RECORD = "addTimeRecord";

	public static final String IN_TIME = "intime";

	public static final String IN_DATE = "indate";

	public static final String OUT_TIME = "outtime";

	public static final String OUT_DATE = "outdate";

	public static final String START_LAT = "startlat";

	public static final String START_LNG = "startlng";

	public static final String END_LAT = "endlat";

	public static final String END_LNG = "endlng";

	/******** delete time recordapi parameters ************/

	public static final String DELETE_TIME_RECORD = "deleteTimeRecord";

	public static final String TR_ID = "trid";

	/********* delete multiple records api parameters ************/

	public static final String DELETE_MULTIPLE_RECORDS = "deleteTimeRecords";

	public static final String NUMBER_OF_RECORDS = "numberofrecords";

	public static final String TIME_RECORD_ID = "timerecordid";

	/******* update time record api parameters **********/

	// trid,
	// userid,clientid,jobtypeid,intime,indate,outtime,outdate,startlat,startlng,endlat,endlng
	public static final String UPDATE_TIME_RECORD = "updateTimeRecord";

	/*********** clock on api parameters ***********/

	// userid,clientid,jobtypeid,startlat, startlng are also the parameters

	public static final String CLOCK_ON = "clockOn";

	/*********** clock off api parameters ***********/

	// timerecordid, endlat, endlng are also the parameters....

	public static final String CLOCK_OFF = "clockOff";

	/*********** switch location api parameters ***********/

	public static final String SWITCH_LOCATION = "switchLocation";

	public static final String LAT = "lat";

	public static final String LNG = "lng";

	/************ massive clock on api parameters **********/

	// clientid,jobtypeid,startlat,startlng,userid are also the parameters...

	public static final String MASSIVE_CLOCK_ON = "massiveClockOn";

	public static final String NUMBER_OF_USERS = "numberofusers";

	/************ massive clock off api parameters **********/

	// numberofusers, timerecord, endlat,endlng...

	public static final String MASSIVE_CLOCK_OFF = "massiveClockOff";

	/************ massive switch location api parameters **********/

	// numberofusers, clientid, timerrecordid, userid, lat, lng

	public static final String MASSIVE_SWITCH_LOCATION = "massiveSwitchLocation";

	/************ start break api parameters **************/

	public static final String START_BREAK = "startBreak";

	public static final String NUMBER_OF_TIME_RECORD = "numberoftimerecord";

	/*********** end break api parameters ****************/
	public static final String END_BREAK = "endBreak";

	/************ edit current clock on info ************/
	public static final String EDIT_CLOCK_ON = "editClockOn";

	/************ submit user location api *************/
	public static final String SUBMIT_USER_LOCATION = "submitUserLocation";

	/************ get user location api ****************/
	public static final String GET_USER_LOCATION = "getUserLocation";

	/************ approve time record api *************/
	public static final String APROVE_TIME_RECORD = "approveTimeRecord";

	/************* response strings from api *************/

	// userid, timerecordid,clientid,jobtypeid, are also the parameters..

	public static final String USER_EXISTS = "User is existing";

	public static final String ADMIN_ID = "adminId";

	public static final String IS_EMPLOYER = "is_employer";

}
