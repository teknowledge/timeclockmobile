package com.timeclockmobile.adapter;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.timeclockmobile.GetWorkingEmployeeLocation;
import com.timeclockmobile.GoogleMapViewActivity;
import com.timeclockmobile.R;
import com.timeclockmobile.TeamManagementActivity;

import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Utils;

public class TeamManagementAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<Employees> mEmployeesList;
	private ArrayList<Employees> searchEmployeesList;
	public ArrayList<Employees> checkedEmployeesList = new ArrayList<Employees>();
	private ArrayList<String> checkedEmployeeIds = new ArrayList<String>();
	LayoutInflater inflater;

	private String mEmployeeId;
	private DataBaseHelper db;
	GetWorkingEmployeeLocation getWorkingEmpLocation;
//	TeamManagementActivity activity;

	public TeamManagementAdapter(Context mContext, int layoutResourceId,
			ArrayList<Employees> employeesArrayList) {
		this.mContext = mContext;
		this.mEmployeesList = employeesArrayList;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		checkedEmployeesList.clear();
		checkedEmployeeIds.clear();
		// checkedEmployeesList = new ArrayList<Employees>();
		// checkedEmployeeIds = new ArrayList<String>();
		this.searchEmployeesList = new ArrayList<Employees>();
		this.searchEmployeesList.addAll(mEmployeesList);
		db = new DataBaseHelper(mContext);

//		activity = new TeamManagementActivity();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mEmployeesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mEmployeesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.employeeName = (TextView) convertView
					.findViewById(R.id.employee_name);
			holder.clientName = (TextView) convertView
					.findViewById(R.id.client_name);
			holder.jobName = (TextView) convertView.findViewById(R.id.job_name);

			holder.timer_text = (TextView) convertView
					.findViewById(R.id.timer_text);
			holder.check_employee = (CheckBox) convertView
					.findViewById(R.id.check_employee);

			holder.employee_check = (CheckBox) convertView
					.findViewById(R.id.employee_check);

			holder.check_employee
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub

							int getPosition = (Integer) buttonView.getTag();

							mEmployeesList.get(getPosition).setChecked(
									buttonView.isChecked());

							if (isChecked) {

								// itemChecked[position] = true;
								checkedEmployeeIds.add(mEmployeesList.get(
										getPosition).getId());

								checkedEmployeesList.add(mEmployeesList
										.get(getPosition));

								Utils.removeDuplicates(checkedEmployeesList);

								((TeamManagementActivity)mContext).changeViews(mContext,
										checkedEmployeesList);

							} else {

								// itemChecked[position] = false;
								checkedEmployeeIds.remove(mEmployeesList.get(
										getPosition).getId());

								checkedEmployeesList.remove(mEmployeesList
										.get(getPosition));

								Utils.removeDuplicates(checkedEmployeesList);

								((TeamManagementActivity)mContext).changeViews(mContext,
										checkedEmployeesList);

							}
						}
					});

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.check_employee.setTag(position);

		holder.employeeName.setText(mEmployeesList.get(position).getName());

		// holder.timer_text.setText(mEmployeesList.get(position)
		// .getWorking_intime()
		// + ",  "
		// + mEmployeesList.get(position).getWorking_outtime());

		holder.timer_text.setText(mEmployeesList.get(position).getTimerText());

		holder.check_employee.setChecked(mEmployeesList.get(position)
				.isChecked());

		if (!mEmployeesList.get(position).getWorking_outtime()
				.equalsIgnoreCase("null")
				&& (!mEmployeesList.get(position).getWorking_intime()
						.equalsIgnoreCase(""))) {

			holder.clientName.setText("Not Working");

			holder.employee_check.setChecked(false);

		} else if (mEmployeesList.get(position).getWorking_outtime()
				.equalsIgnoreCase("null")
				&& (mEmployeesList.get(position).getWorking_intime()
						.equalsIgnoreCase(""))) {

			holder.clientName.setText("Not Working");

			holder.employee_check.setChecked(false);

		} else {
			holder.clientName.setText(mEmployeesList.get(position)
					.getWorking_clientName()
					+ " > "
					+ mEmployeesList.get(position).getWorking_jobName());

			holder.employee_check.setChecked(true);

		}

		holder.employeeName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mEmployeesList.get(position).getIsWorking()
						.equalsIgnoreCase("1")) {

					getWorkingEmpLocation = new GetWorkingEmployeeLocation(
							mContext, mEmployeesList.get(position).getId(),
							mEmployeesList.get(position).getWorking_intime());

					getWorkingEmpLocation.execute("");
					// Intent intent = new Intent(mContext,
					// GoogleMapViewActivity.class);
					//
					// intent.putExtra("startLat", mEmployeesList.get(position)
					// .getWorking_startlat());
					// intent.putExtra("startLng", mEmployeesList.get(position)
					// .getWorking_startlng());
					//
					//
					// intent.putExtra("clockOnTime",
					// mEmployeesList.get(position)
					// .getWorking_intime());
					//
					// intent.putExtra("from", "team_management");
					//
					//
					// mContext.startActivity(intent);
				}
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView employeeName;
		TextView clientName;
		TextView jobName;
		CheckBox check_employee;
		TextView timer_text;
		CheckBox employee_check;

	}

	public ArrayList<Employees> getCheckedEmployees() {

		Utils.removeDuplicates(checkedEmployeesList);
		return checkedEmployeesList;

	}

	public ArrayList<String> getCheckedEmployeesIds() {

		Utils.removeDuplicates(checkedEmployeeIds);
		return checkedEmployeeIds;

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mEmployeesList.clear();
		if (charText.length() == 0) {
			mEmployeesList.addAll(searchEmployeesList);
		} else {
			for (Employees employee : searchEmployeesList) {
				if (employee.getName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mEmployeesList.add(employee);
				}
			}
		}
		notifyDataSetChanged();
	}
}
