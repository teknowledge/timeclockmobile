package com.timeclockmobile.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.timeclockmobile.R;
import com.timeclockmobile.data.JobTypes;

public class SpinnerAdapter extends ArrayAdapter<String> {

	// Your sent context
	private Context context;
	// Your custom values for the spinner (User)
	private ArrayList<String> mAllDatesList;

	public SpinnerAdapter(Context context, int textViewResourceId,
			ArrayList<String> datesArray) {
		super(context, textViewResourceId, datesArray);
		this.context = context;
		this.mAllDatesList = datesArray;
	}

	public int getCount() {
		return mAllDatesList.size();
	}

	public String getItem(int position) {
		return mAllDatesList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	// And the "magic" goes here
	// This is for the "passive" state of the spinner
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// I created a dynamic TextView here, but you can reference your own
		// custom layout for each spinner item
		// TextView label = new TextView(context);
		// label.setTextColor(Color.BLACK);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View spView = inflater.inflate(R.layout.spinner_list_item, parent,
				false);

		TextView label = (TextView) spView.findViewById(R.id.title_text);
		// Then you can get the current item using the values array (Users
		// array) and the current position
		// You can NOW reference each method you has created in your bean object
		// (User class)
		label.setText(mAllDatesList.get(position));

		// And finally return your dynamic (or custom) view for each spinner
		// item
		return label;
	}

	// And here is when the "chooser" is popped up
	// Normally is the same view, but you can customize it if you want
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TextView label = new TextView(context);
		// label.setTextColor(Color.BLACK);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View spView = inflater.inflate(R.layout.spinner_list_item, parent,
				false);

		TextView label = (TextView) spView.findViewById(R.id.title_text);
		label.setText(mAllDatesList.get(position));

		return label;
	}
}
