package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.adapter.ClientsAdapterForFieldNotes;
import com.timeclockmobile.adapter.EmployeesListAdapter;
import com.timeclockmobile.data.Clients;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class AddFieldNotesActivity extends Activity {

	private TextView title_text;
	private ImageButton backBtn;
	private Spinner employees_spinner;
	private Spinner clients_spinner;
	private EditText field_note;
	private Button submite_note;
	public ProgressDialog progressDialog;

	private String mClientId, mEmployeeId;

	private ArrayList<Employees> employeesList = new ArrayList<Employees>();
	private ArrayList<Clients> clientsList = new ArrayList<Clients>();

	private EmployeesListAdapter adapterEmployees;

	private ClientsAdapterForFieldNotes adapterClients;

	// private ClientsListAdapter adapterClients;

	private ManageEmployeesActivity employeesActivity = new ManageEmployeesActivity();
	private ManageClientsActivity clientsActivity = new ManageClientsActivity();

	String fieldNoteId;
	String type;
	private DataBaseHelper db = new DataBaseHelper(AddFieldNotesActivity.this);
	public boolean isSelectedEmployee;
	public boolean isSelectedClient;

	private int defaultEmployee;
	private int defaultClient;

	Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_field_notes);

		progressDialog = new ProgressDialog(AddFieldNotesActivity.this);

		Intent intent = getIntent();

		type = intent.getStringExtra("type");

		employees_spinner = (Spinner) findViewById(R.id.employees_spinner);
		clients_spinner = (Spinner) findViewById(R.id.clients_spinner);
		title_text = (TextView) findViewById(R.id.title_text);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		field_note = (EditText) findViewById(R.id.field_note);
		submite_note = (Button) findViewById(R.id.submite_note);

		// assigning employees to the employees spinner

		employeesList = employeesActivity
				.getEmployeesDetailsFromDB(AddFieldNotesActivity.this);

		adapterEmployees = new EmployeesListAdapter(AddFieldNotesActivity.this,
				R.layout.spinner_list_item, employeesList);

		employees_spinner.setAdapter(adapterEmployees);

		// employees_spinner.setSelection(4);

		// assigning clients to the clients spinner

		clientsList = clientsActivity
				.getClientsFromDB(AddFieldNotesActivity.this);

		adapterClients = new ClientsAdapterForFieldNotes(
				AddFieldNotesActivity.this, R.layout.spinner_list_item,
				clientsList);

		clients_spinner.setAdapter(adapterClients);

		if (type.equalsIgnoreCase("edit")) {

			title_text.setText("Edit Field Note");
			fieldNoteId = intent.getStringExtra("fieldNoteId");

			field_note.setText(intent.getStringExtra("fieldNote"));
			mEmployeeId = intent.getStringExtra("EmployeeId");
			mClientId = intent.getStringExtra("ClientId");

			for (int i = 0; i < employeesList.size(); i++) {

				if (mEmployeeId.equalsIgnoreCase(employeesList.get(i).getId())) {
					defaultEmployee = i;
				}

			}

			employees_spinner.setSelection(defaultEmployee);

			for (int i = 0; i < clientsList.size(); i++) {
				if (mClientId.equalsIgnoreCase(clientsList.get(i)
						.getmClientId())) {
					defaultClient = i;
				}
			}

			clients_spinner.setSelection(defaultClient);
		}

		// Setting OnItemClickListener to the employees Spinner

		employees_spinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						// employees_spinner.getItemAtPosition(position).toString();

						Employees employee = (Employees) parent.getAdapter()
								.getItem(position);

						if (!isSelectedEmployee) {
							isSelectedEmployee = true;
						}
						if (type.equalsIgnoreCase("edit")) {

							if (isSelectedEmployee) {

								mEmployeeId = employee.getId();
							}
						} else {

							mEmployeeId = employee.getId();
						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		// Setting OnItemClickListener to the clients Spinner

		clients_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// employees_spinner.getItemAtPosition(position).toString();

				Clients client = (Clients) parent.getAdapter()
						.getItem(position);

				if (!isSelectedClient) {
					isSelectedClient = true;
				}

				if (type.equalsIgnoreCase("edit")) {

					if (isSelectedClient) {

						mClientId = client.getmClientId();
					}
				} else {

					mClientId = client.getmClientId();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		submite_note.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String fieldNote = field_note.getText().toString();

				if (type.equalsIgnoreCase("add")) {
					if (!fieldNote.equalsIgnoreCase("")) {

						if (utils
								.isConnectingToInternet(AddFieldNotesActivity.this)) {
							new AddFieldNotesTask(fieldNote).execute("");
						} else {
							utils.showDialog(AddFieldNotesActivity.this);
						}

					} else {
						Toast.makeText(getApplicationContext(),
								"Field Note should not be empty",
								Toast.LENGTH_SHORT).show();
					}
				} else {
					if (!fieldNote.equalsIgnoreCase("")) {

						if (utils
								.isConnectingToInternet(AddFieldNotesActivity.this)) {
							new UpdateFieldNoteTask(fieldNote).execute("");
						} else {
							utils.showDialog(AddFieldNotesActivity.this);
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Field Note should not be empty",
								Toast.LENGTH_SHORT).show();
					}
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});
	}

	private class AddFieldNotesTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String fieldnote;

		public AddFieldNotesTask(String fieldNote) {
			fieldnote = fieldNote;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");

			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.ADD_FIELD_NOTE));

				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								AddFieldNotesActivity.this,
								RequestParameters.USER_ID)));

				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));

				entity.addPart(RequestParameters.NOTE,
						new StringBody(fieldnote));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonObject = jsonResponse.getJSONObject("response");
				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");
				if (success.equalsIgnoreCase("1")
						&& message.equalsIgnoreCase("Success")) {
					Toast.makeText(getApplicationContext(),
							"Field Note has been Added", Toast.LENGTH_LONG)
							.show();
					JSONObject fieldNotesObject = jsonObject
							.getJSONObject("fieldnote");
					String name = fieldNotesObject.getString("name");
					String email = fieldNotesObject.getString("email");
					String address = fieldNotesObject.getString("address");
					String password = fieldNotesObject.getString("password");
					String phoneno = fieldNotesObject.getString("phoneno");
					String hourlyrate = fieldNotesObject
							.getString("hourlyrate");
					String devicetoken = fieldNotesObject
							.getString("devicetoken");
					String online = fieldNotesObject.getString("online");
					String status = fieldNotesObject.getString("status");
					String id = fieldNotesObject.getString("id");
					String userid = fieldNotesObject.getString("userid");
					String clientid = fieldNotesObject.getString("clientid");
					String note = fieldNotesObject.getString("note");

					String createdGMT = fieldNotesObject.getString("created");

					String created = utils.convertGMTtoLocalTime(createdGMT);

					String active = fieldNotesObject.getString("active");
					String clientname = fieldNotesObject
							.getString("clientname");
					String clientaddress = fieldNotesObject
							.getString("clientaddress");

					String dateCreated[] = created.split(" ");

					db.open();
					db.insertFieldNotes(name, email, address, password,
							phoneno, hourlyrate, devicetoken, online, status,
							id, userid, clientid, note, dateCreated[0], active,
							clientname, clientaddress);
					db.close();

					Intent intent = new Intent(AddFieldNotesActivity.this,
							FieldNotesActivity.class);
					intent.putExtra("from", "home");
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class UpdateFieldNoteTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();
		String fieldnote;

		public UpdateFieldNoteTask(String fieldNote) {
			fieldnote = fieldNote;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");

			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.UPDATE_FIELD_NOTE));
				entity.addPart(RequestParameters.FIELD_NOTE_ID, new StringBody(
						fieldNoteId));
				entity.addPart(RequestParameters.USER_ID, new StringBody(
						mEmployeeId));
				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));
				entity.addPart(RequestParameters.NOTE,
						new StringBody(fieldnote));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			JSONObject jsonObject;
			try {
				jsonObject = jsonResponse.getJSONObject("response");
				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");
				if (success.equalsIgnoreCase("1")
						&& message
								.equalsIgnoreCase("Field Note Updated Successfully")) {
					Toast.makeText(getApplicationContext(),
							"Field Note has been updated successfully",
							Toast.LENGTH_LONG).show();

					JSONObject fieldNotesObject = jsonObject
							.getJSONObject("fieldnote");
					String name = fieldNotesObject.getString("name");
					String email = fieldNotesObject.getString("email");
					String address = fieldNotesObject.getString("address");
					String password = fieldNotesObject.getString("password");
					String phoneno = fieldNotesObject.getString("phoneno");
					String hourlyrate = fieldNotesObject
							.getString("hourlyrate");
					String devicetoken = fieldNotesObject
							.getString("devicetoken");
					String online = fieldNotesObject.getString("online");
					String status = fieldNotesObject.getString("status");
					String id = fieldNotesObject.getString("id");
					String userid = fieldNotesObject.getString("userid");
					String clientid = fieldNotesObject.getString("clientid");
					String note = fieldNotesObject.getString("note");
					String createdGMT = fieldNotesObject.getString("created");
					String created = utils.convertGMTtoLocalTime(createdGMT);

					String active = fieldNotesObject.getString("active");
					String clientname = fieldNotesObject
							.getString("clientname");
					String clientaddress = fieldNotesObject
							.getString("clientaddress");

					db.open();
					db.updateFieldNotes(name, email, address, password,
							phoneno, hourlyrate, devicetoken, online, status,
							id, userid, clientid, note, created, active,
							clientname, clientaddress);
					db.close();

					Intent intent = new Intent(AddFieldNotesActivity.this,
							FieldNotesActivity.class);
					intent.putExtra("from", "home");
					startActivity(intent);
					finish();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
