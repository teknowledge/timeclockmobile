package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.adapter.SpinnerAdapter;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class AddEmployeeActivity extends Activity {

	private EditText name, email, enter_pin, confirm_pin, pay_rate, address,
			phoneno;
	private Spinner employer_type;
	private TextView title_text;
	private Button submit_employee;
	private ImageButton backBtn;
	public ProgressDialog progressDialog;
	private String mName, mEmail, mAddress, mEnterPin, mConfirmPin, mPhoneNo,
			mPayRate;

	private String mEmployeeId;
	private String mType;
	private String mEmployerType;
	private DataBaseHelper db = new DataBaseHelper(AddEmployeeActivity.this);
	private Utils utils = new Utils();
	private ArrayList<String> employersTypeList = new ArrayList<String>();
	private SpinnerAdapter adapter;
	GpsTracker gpsTracker;
	String latitude;
	String longitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.add_new_employee_testing);

		gpsTracker = new GpsTracker(this);

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(this);
		}

		progressDialog = new ProgressDialog(AddEmployeeActivity.this);
		backBtn = (ImageButton) findViewById(R.id.backBtn);
		title_text = (TextView) findViewById(R.id.title_text);

		name = (EditText) findViewById(R.id.name);
		email = (EditText) findViewById(R.id.email);
		address = (EditText) findViewById(R.id.address);
		enter_pin = (EditText) findViewById(R.id.enter_pin);
		confirm_pin = (EditText) findViewById(R.id.confirm_pin);
		phoneno = (EditText) findViewById(R.id.phoneno);
		pay_rate = (EditText) findViewById(R.id.pay_rate);
		employer_type = (Spinner) findViewById(R.id.employer_type);

		// employer type to the dates spinner
		employersTypeList.add("Employee");
		employersTypeList.add("Employer");

		adapter = new SpinnerAdapter(AddEmployeeActivity.this,
				R.layout.spinner_list_item, employersTypeList);

		employer_type.setAdapter(adapter);

		employer_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				String employerTypeStr = employersTypeList.get(position);

				if (employerTypeStr.equalsIgnoreCase("Employee")) {
					mEmployerType = "0";
				} else {
					mEmployerType = "1";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		Intent intent = getIntent();

		mType = intent.getStringExtra("type");
		if (mType.equalsIgnoreCase("update")) {
			mEmployeeId = intent.getStringExtra("EmployeeId");
			title_text.setText("Edit Employee");

			name.setText(intent.getStringExtra("EmployeeName"));
			email.setText(intent.getStringExtra("EmployeeEmail"));
			address.setText(intent.getStringExtra("EmployeeAddress"));
			enter_pin.setText(intent.getStringExtra("EmployeePin"));
			confirm_pin.setText(intent.getStringExtra("EmployeePin"));
			phoneno.setText(intent.getStringExtra("EmployeePhoneNo"));
			pay_rate.setText(intent.getStringExtra("EmployeePayRate"));
		}

		submit_employee = (Button) findViewById(R.id.submit_employee);

		submit_employee.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mName = name.getText().toString();
				mEmail = email.getText().toString();
				mAddress = address.getText().toString();
				mEnterPin = enter_pin.getText().toString();
				mConfirmPin = confirm_pin.getText().toString();
				mPhoneNo = phoneno.getText().toString();
				mPayRate = pay_rate.getText().toString();

				if (!mName.equalsIgnoreCase("")) {
					if (!mEmail.equalsIgnoreCase("")
							&& Utils.isValidEmail(mEmail)) {
						if (!mAddress.equalsIgnoreCase("")) {
							if (!mEnterPin.equalsIgnoreCase("")) {
								if (!mConfirmPin.equalsIgnoreCase("")) {
									if (mEnterPin.equalsIgnoreCase(mConfirmPin)) {
										if (!mPhoneNo.equalsIgnoreCase("")) {
											if (!mPayRate.equalsIgnoreCase("")) {

												// add employee by employer..
												if (mType
														.equalsIgnoreCase("add")) {

													if (utils
															.isConnectingToInternet(AddEmployeeActivity.this)) {
														new AddEmployeeTask()
																.execute("");
													} else {
														utils.showDialog(AddEmployeeActivity.this);
													}

												} else {
													// update employee by
													// employer...

													if (utils
															.isConnectingToInternet(AddEmployeeActivity.this)) {
														new UpdateEmployeeTask()
																.execute("");
													} else {
														utils.showDialog(AddEmployeeActivity.this);
													}
												}

											} else {
												Toast.makeText(
														getApplicationContext(),
														"Enter Pay Rate",
														Toast.LENGTH_SHORT)
														.show();
											}
										} else {
											Toast.makeText(
													getApplicationContext(),
													"Enter Phone Number",
													Toast.LENGTH_SHORT).show();
										}
									} else {
										Toast.makeText(getApplicationContext(),
												"Confirm Pin not matched",
												Toast.LENGTH_SHORT).show();
									}
								} else {
									Toast.makeText(getApplicationContext(),
											"Enter Confirm Pin",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(getApplicationContext(),
										"Enter Pin", Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(getApplicationContext(),
									"Enter Address", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Enter Correct Email", Toast.LENGTH_SHORT)
								.show();
					}
				} else {
					Toast.makeText(getApplicationContext(), "Enter Name",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent i = new Intent(AddEmployeeActivity.this,
				ManageEmployeesActivity.class); // your class
		startActivity(i);
		finish();

	}

	private class AddEmployeeTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.ADD_EMPLOYEE));

				entity.addPart(
						RequestParameters.EMPLOYER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								AddEmployeeActivity.this,
								RequestParameters.EMPLOYER_ID)));

				entity.addPart(RequestParameters.NAME, new StringBody(mName));

				entity.addPart(RequestParameters.EMAIL, new StringBody(mEmail));

				entity.addPart(RequestParameters.ADDRESS, new StringBody(
						mAddress));

				entity.addPart(RequestParameters.PIN_NUMBER, new StringBody(
						mEnterPin));

				entity.addPart(RequestParameters.PHONE_NO, new StringBody(
						mPhoneNo));

				entity.addPart(RequestParameters.HOURLY_RATE, new StringBody(
						mPayRate));

				entity.addPart(RequestParameters.EMPLOYER_TYPE, new StringBody(
						mEmployerType));
				entity.addPart(RequestParameters.LAT, new StringBody(latitude));
				entity.addPart(RequestParameters.LNG, new StringBody(longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject employeeJson = jsonResponse
						.getJSONObject("response");
				String success = employeeJson.getString("success");
				String message = employeeJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("New Employee Added"))) {

					JSONObject employeesObject = employeeJson
							.getJSONObject("employee");

					String employeeId = employeesObject.getString("id");
					String employeeName = employeesObject.getString("name");

					String employeeEmail = employeesObject.getString("email");
					String employeeAddress = employeesObject
							.getString("address");
					String employeePassword = employeesObject
							.getString("password");
					String employeePhoneNo = employeesObject
							.getString("phoneno");
					String employeeProfileImage = employeesObject
							.getString("profileimage");
					String employeeHourlyRate = employeesObject
							.getString("hourlyrate");
					String employeeDeviceToken = employeesObject
							.getString("devicetoken");
					String employeeIsEmployer = employeesObject
							.getString("isemployer");
					String employerId = employeesObject.getString("employerid");
					String employeeOnline = employeesObject.getString("online");
					String employeeStatus = employeesObject.getString("status");

					String employeeCreatedGMT = employeesObject
							.getString("created");
					String employeeCreated = utils
							.convertGMTtoLocalTime(employeeCreatedGMT);

					String employeeLastLoginGMT = employeesObject
							.getString("lastlogin");

					String employeeLastLogin = utils
							.convertGMTtoLocalTime(employeeLastLoginGMT);

					if (!employeeIsEmployer.equalsIgnoreCase("1")) {
						db.open();
						db.insertEmployees(employeeId, employeeName,
								employeeEmail, employeeAddress,
								employeePassword, employeePhoneNo,
								employeeProfileImage, employeeHourlyRate,
								employeeDeviceToken, employeeIsEmployer,
								employerId, employeeOnline, employeeStatus,
								employeeCreated, employeeLastLogin, "0", "",
								"", "", "", "", "", "", "", "", "", "", "",
								"null", "null","0");
						db.close();
					}

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(AddEmployeeActivity.this,
							ManageEmployeesActivity.class); // your class
					startActivity(i);
					finish();

				} else {
					if ((success.equalsIgnoreCase("0"))
							&& (message
									.equalsIgnoreCase("PIN is not available"))) {

						Toast.makeText(getApplicationContext(),
								"PIN is not available", Toast.LENGTH_SHORT)
								.show();
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class UpdateEmployeeTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.UPDATE_EMPLOYEE));

				entity.addPart(RequestParameters.EMPLOYEE_ID, new StringBody(
						mEmployeeId));

				entity.addPart(RequestParameters.NAME, new StringBody(mName));

				entity.addPart(RequestParameters.EMAIL, new StringBody(mEmail));

				entity.addPart(RequestParameters.ADDRESS, new StringBody(
						mAddress));

				entity.addPart(RequestParameters.PIN_NUMBER, new StringBody(
						mEnterPin));

				entity.addPart(RequestParameters.PHONE_NO, new StringBody(
						mPhoneNo));

				entity.addPart(RequestParameters.HOURLY_RATE, new StringBody(
						mPayRate));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject employeeJson = jsonResponse
						.getJSONObject("response");
				String success = employeeJson.getString("success");
				String message = employeeJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message
								.equalsIgnoreCase("Employee Information Updated"))) {

					JSONObject employeesObject = employeeJson
							.getJSONObject("employee");

					String employeeId = employeesObject.getString("id");
					String employeeName = employeesObject.getString("name");

					String employeeEmail = employeesObject.getString("email");
					String employeeAddress = employeesObject
							.getString("address");
					String employeePassword = employeesObject
							.getString("password");
					String employeePhoneNo = employeesObject
							.getString("phoneno");
					String employeeProfileImage = employeesObject
							.getString("profileimage");
					String employeeHourlyRate = employeesObject
							.getString("hourlyrate");
					String employeeDeviceToken = employeesObject
							.getString("devicetoken");
					String employeeIsEmployer = employeesObject
							.getString("isemployer");
					String employerId = employeesObject.getString("employerid");
					String employeeOnline = employeesObject.getString("online");
					String employeeStatus = employeesObject.getString("status");

					String employeeCreatedGMT = employeesObject
							.getString("created");

					String employeeCreated = utils
							.convertGMTtoLocalTime(employeeCreatedGMT);

					String employeeLastLoginGMT = employeesObject
							.getString("lastlogin");

					String employeeLastLogin = utils
							.convertGMTtoLocalTime(employeeLastLoginGMT);

					db.open();
					db.updateEmployeeDetails(employeeId, employeeName,
							employeeEmail, employeeAddress, employeePassword,
							employeePhoneNo, employeeProfileImage,
							employeeHourlyRate, employeeDeviceToken,
							employeeIsEmployer, employerId, employeeOnline,
							employeeStatus, employeeCreated, employeeLastLogin);
					db.close();

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(AddEmployeeActivity.this,
							ManageEmployeesActivity.class); // your class
					startActivity(i);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
