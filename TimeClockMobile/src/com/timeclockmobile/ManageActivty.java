package com.timeclockmobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class ManageActivty extends Activity {

	private TextView manage_clients;
	private TextView manage_jobTypes;
	private TextView manage_employees;
	private TextView manage_working;
	private TextView team_management;

	private ImageButton backBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.manage);

		manage_clients = (TextView) findViewById(R.id.manage_clients);
		manage_clients.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ManageActivty.this,
						ManageClientsActivity.class);
				startActivity(intent);
			}
		});
		manage_employees = (TextView) findViewById(R.id.manage_employees);
		manage_employees.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ManageActivty.this,
						ManageEmployeesActivity.class);
				startActivity(intent);
			}
		});

		manage_jobTypes = (TextView) findViewById(R.id.manage_jobtypes);
		manage_jobTypes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ManageActivty.this,
						ManageJobTypesActivity.class);
				startActivity(intent);
			}
		});
		manage_working = (TextView) findViewById(R.id.manage_working);
		manage_working.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ManageActivty.this,
						ManageWorkingEmployeesActivity.class);
				startActivity(intent);
			}
		});

		team_management = (TextView) findViewById(R.id.team_management);
		team_management.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ManageActivty.this,
						TeamManagementActivity.class);
				startActivity(intent);
			}
		});

		backBtn = (ImageButton) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent = new Intent(ManageActivty.this, HomeActivity.class);
		startActivity(intent);
		finish();
	}
}
