package com.timeclockmobile;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.timeclockmobile.adapter.ClientsAdapterForFieldNotes;
import com.timeclockmobile.adapter.EmployeesListAdapter;
import com.timeclockmobile.data.Clients;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.webservices.RequestParameters;

public class FilterFieldNotesActivity extends Activity {

	private Spinner filter_fn_clients, filter_fn_employees;
	private Button filter_fn_dates, filter_fn;
	private ManageEmployeesActivity manageEmployeesActivity = new ManageEmployeesActivity();
	private ManageClientsActivity manageClientsActivity = new ManageClientsActivity();

	private ArrayList<Clients> clientsList = new ArrayList<Clients>();
	private ArrayList<Employees> employeesList = new ArrayList<Employees>();
	private ClientsAdapterForFieldNotes adapterForClients;
	private EmployeesListAdapter adapterForEmployees;
	private ImageButton backBtn;
	private String mClientId, mEmployeeId;
	private String selectedDate;

	private String clientQuery;
	private String employeeQuery;
	private String dateQuery;
	private int year;
	private int month;
	private int day;
	static final int DATE_PICKER_ID = 1111;
	int mYear, mMonth1, mDay;

	private String where;
	private DataBaseHelper db = new DataBaseHelper(
			FilterFieldNotesActivity.this);

	private String[] months = { "01", "02", "03", "04", "05", "06", "07", "08",
			"09", "10", "11", "12" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.filter_field_notes);

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		mYear = c.get(Calendar.YEAR);
		mMonth1 = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		filter_fn_clients = (Spinner) findViewById(R.id.filter_fn_clients);
		filter_fn_employees = (Spinner) findViewById(R.id.filter_fn_employees);
		filter_fn_dates = (Button) findViewById(R.id.filter_fn_dates);
		filter_fn = (Button) findViewById(R.id.filter_fn);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		clientsList = manageClientsActivity
				.getClientsFromDB(FilterFieldNotesActivity.this);

		Clients client = new Clients();
		client.setmClientId("0");
		client.setmClientName("All Clients");

		clientsList.add(0, client);

		adapterForClients = new ClientsAdapterForFieldNotes(
				FilterFieldNotesActivity.this, R.layout.spinner_list_item,
				clientsList);

		filter_fn_clients.setAdapter(adapterForClients);

		filter_fn_clients
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						Clients client = (Clients) parent.getAdapter().getItem(
								position);
						mClientId = client.getmClientId();

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		// assigning employees to the employees spinner

		// assigning employees to the employees spinner

		if (!Global.getInstance().getBooleanType(FilterFieldNotesActivity.this,
				RequestParameters.IS_EMPLOYER)) {
			Employees employee = new Employees();
			employee.setId(Global.getInstance().getPreferenceVal(
					FilterFieldNotesActivity.this, RequestParameters.USER_ID));

			employee.setName(Global.getInstance().getPreferenceVal(
					FilterFieldNotesActivity.this, CONSTANTS.USER_NAME));
			employeesList.add(0, employee);

			adapterForEmployees = new EmployeesListAdapter(
					FilterFieldNotesActivity.this, R.layout.spinner_list_item,
					employeesList);

			filter_fn_employees.setAdapter(adapterForEmployees);
		} else {
			employeesList = manageEmployeesActivity
					.getEmployeesDetailsFromDB(FilterFieldNotesActivity.this);

			Employees employee = new Employees();
			employee.setId("0");
			employee.setName("All Employees");
			employeesList.add(0, employee);

			Employees employee1 = new Employees();

			employee1.setId(Global.getInstance().getPreferenceVal(
					FilterFieldNotesActivity.this,
					RequestParameters.EMPLOYER_ID));
			employee1.setName(Global.getInstance().getPreferenceVal(
					FilterFieldNotesActivity.this,
					RequestParameters.EMPLOYER_NAME));
			employeesList.add(employee1);

			adapterForEmployees = new EmployeesListAdapter(
					FilterFieldNotesActivity.this, R.layout.spinner_list_item,
					employeesList);

			filter_fn_employees.setAdapter(adapterForEmployees);
		}

		filter_fn_employees
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						Employees employee = (Employees) parent.getAdapter()
								.getItem(position);
						mEmployeeId = employee.getId();

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		filter_fn_dates.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// On button click show datepicker dialog
				showDialog(DATE_PICKER_ID);
			}
		});

		filter_fn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// selectedDate = filter_fn_dates.getText().toString();
				ArrayList<String> array = new ArrayList<String>();

				if (!mClientId.equalsIgnoreCase("0")) {
					clientQuery = "field_note_client_id" + "='" + mClientId
							+ "'";
					array.add(clientQuery);
				}

				if (!mEmployeeId.equalsIgnoreCase("0")) {
					employeeQuery = "field_note_user_id" + "='" + mEmployeeId
							+ "'";
					array.add(employeeQuery);
				}

				if (!filter_fn_dates.getText().toString()
						.equalsIgnoreCase("All Dates")) {
					dateQuery = "field_note_created" + "='" + selectedDate
							+ "'";
					array.add(dateQuery);

				}

				for (int i = 0; i < array.size(); i++) {
					if (i == 0) {
						where = array.get(i);
					} else {
						where = where + " AND " + array.get(i);
					}

				}

				if (mClientId.equalsIgnoreCase("0")
						&& mEmployeeId.equalsIgnoreCase("0")
						&& filter_fn_dates.getText().toString()
								.equalsIgnoreCase("All Dates")) {

					

					Intent intent = new Intent(FilterFieldNotesActivity.this,
							FieldNotesActivity.class);
					intent.putExtra("where", where);
					intent.putExtra("from", "home");
					startActivity(intent);
					finish();
				} else {
					// where = clientQuery + employeeQuery + dateQuery;

					Intent intent = new Intent(FilterFieldNotesActivity.this,
							FieldNotesActivity.class);
					intent.putExtra("where", where);
					intent.putExtra("from", "filter");
					startActivity(intent);
					finish();
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			// return new DatePickerDialog(this, pickerListener, year, month,
			// day);

			DatePickerDialog dialog = new DatePickerDialog(this,
					pickerListener, year, month, day) {
				public void onDateChanged(DatePicker view, int myear,
						int mmonth, int mday) {
					System.out.println("----------onDateChanged()-----------"
							+ year + "--" + myear);
					System.out.println("----------onDateChanged()-----------"
							+ month + "--" + mmonth);
					System.out.println("----------onDateChanged()-----------"
							+ day + "--" + mday);

					/*
					 * These lines of commented code used for only setting the
					 * maximum date on Date Picker..
					 * 
					 * if (year > mYear && year) view.updateDate(mYear, mMonth,
					 * mDay);
					 * 
					 * if (monthOfYear > mMonth && year == mYear )
					 * view.updateDate(mYear, mMonth, mDay);
					 * 
					 * if (dayOfMonth > mDay && year == mYear && monthOfYear ==
					 * mMonth) view.updateDate(mYear, mMonth, mDay);
					 */

					// these below lines of code used for setting the maximum as
					// well as minimum dates on Date Picker Dialog..
					if (myear > mYear)
						view.updateDate(mYear, mMonth1, mDay);

					if (mmonth > mMonth1 && myear == mYear)
						view.updateDate(mYear, mMonth1, mDay);

					if (mday > mDay && myear == mYear && mmonth == mMonth1)
						view.updateDate(mYear, mMonth1, mDay);

				};
			};

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				dialog.getDatePicker().setMaxDate(
						System.currentTimeMillis() - 1000);
			} else {

			}

			return dialog;

		}
		return null;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		

		Intent intent = new Intent(FilterFieldNotesActivity.this,
				FieldNotesActivity.class);
		intent.putExtra("where", where);
		intent.putExtra("from", "home");
		startActivity(intent);
		finish();
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			if (Global
					.getInstance()
					.getPreferenceVal(FilterFieldNotesActivity.this,
							CONSTANTS.DATE_FORMAT)
					.equalsIgnoreCase("DD/MM/YYYY")) {
				// inDate = new SimpleDateFormat("dd/MM/yyyy EEEE").format(new
				// Date());

				// Show selected date
				filter_fn_dates.setText(new StringBuilder().append(day)
						.append("/").append(months[month]).append("/")
						.append(year).append(" "));

				selectedDate = year + "-" + months[month] + "-" + day;
				// selectedDate = "2015-01-16";
			} else {

				// inDate = new SimpleDateFormat("MM/dd/yyyy EEEE").format(new
				// Date());

				// Show selected date
				filter_fn_dates.setText(new StringBuilder()
						.append(months[month]).append("/").append(day)
						.append("/").append(year).append(" "));

				selectedDate = year + "-" + months[month] + "-" + day;
				// /selectedDate = "2015-01-16";
			}

		}
	};
}
