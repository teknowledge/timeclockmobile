package com.timeclockmobile.adapter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.AddEmployeeActivity;
import com.timeclockmobile.ClockOnActivity;
import com.timeclockmobile.R;

import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class EmployeesAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<Employees> mEmployeesList;
	LayoutInflater inflater;
	public ProgressDialog progressDialog;
	private String mEmployeeId;
	private DataBaseHelper db;
	Utils utils = new Utils();

	public EmployeesAdapter(Context mContext, int layoutResourceId,
			ArrayList<Employees> employeesArrayList) {
		this.mContext = mContext;
		this.mEmployeesList = employeesArrayList;
		this.layoutResourceId = layoutResourceId;
		progressDialog = new ProgressDialog(mContext);
		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		db = new DataBaseHelper(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mEmployeesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mEmployeesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.employeeName = (TextView) convertView
					.findViewById(R.id.client_name);
			holder.deleteBtnEmployee = (ImageButton) convertView
					.findViewById(R.id.delete_client);
			holder.editBtnEmployee = (ImageButton) convertView
					.findViewById(R.id.edit_client);
			holder.client_address = (TextView) convertView
					.findViewById(R.id.client_address);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.employeeName.setText(mEmployeesList.get(position).getName());

		holder.client_address.setVisibility(View.GONE);
		holder.editBtnEmployee.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, AddEmployeeActivity.class);
				intent.putExtra("type", "update");
				intent.putExtra("EmployeeId", mEmployeesList.get(position)
						.getId());
				intent.putExtra("EmployeeName", mEmployeesList.get(position)
						.getName());
				intent.putExtra("EmployeeEmail", mEmployeesList.get(position)
						.getEmail());
				intent.putExtra("EmployeeAddress", mEmployeesList.get(position)
						.getAddress());
				intent.putExtra("EmployeePin", mEmployeesList.get(position)
						.getPassword());
				intent.putExtra("EmployeePhoneNo", mEmployeesList.get(position)
						.getPhoneno());
				intent.putExtra("EmployeePayRate", mEmployeesList.get(position)
						.getHourlyrate());

				mContext.startActivity(intent);
				((Activity) mContext).finish();
			}
		});
		holder.deleteBtnEmployee.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mEmployeeId = mEmployeesList.get(position).getId();

				// alert dialog to delete employee

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mContext);

				// Setting Dialog Title
				alertDialog.setTitle("Delete "
						+ mEmployeesList.get(position).getName());

				// Setting Dialog Message
				alertDialog.setMessage("Are you sure you want to delete "
						+ mEmployeesList.get(position).getName() + "?");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.
								// delete employee

								if (utils.isConnectingToInternet(mContext)) {
									new DeleteEmployeeTask(mEmployeeId,
											position).execute("");
								} else {
									utils.showDialog(mContext);
								}

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("NO",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog
								dialog.cancel();
							}
						});

				// Showing Alert Message
				alertDialog.show();
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView employeeName;
		ImageButton deleteBtnEmployee;
		ImageButton editBtnEmployee;
		TextView client_address;

	}

	private class DeleteEmployeeTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String employeeId;
		int mPosition;

		public DeleteEmployeeTask(String employee_id, int position) {
			employeeId = employee_id;
			mPosition = position;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_EMPLOYEE));

				entity.addPart(RequestParameters.EMPLOYEE_ID, new StringBody(
						employeeId));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject mResponseObject = jsonResponse
						.getJSONObject("response");

				String success = mResponseObject.getString("success");
				String message = mResponseObject.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Employee Deleted"))) {
					// deleting client from database
					db.open();
					db.deleteEmployeeForId(employeeId);
					db.close();

					mEmployeesList.remove(mPosition);
					notifyDataSetChanged();

					Toast.makeText(mContext, "Employee has been deleted",
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
