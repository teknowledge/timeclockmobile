package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.adapter.ClientsAdapterForFieldNotes;
import com.timeclockmobile.adapter.EmployeesListAdapter;
import com.timeclockmobile.adapter.JobTypesAdapterForTimeRecord;
import com.timeclockmobile.custompicker.CustomDateTimePicker;

import com.timeclockmobile.data.Clients;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.data.JobTypes;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class AddTimeRecordActivity extends Activity {

	// implements DateWatcher

	public ProgressDialog progressDialog;
	private ImageButton backBtn;

	private Spinner all_clients, all_employees, all_job_type;
	private TextView title_text;

	private TextView start_date, end_date;

	private EditText enter_rate, enter_hours;
	private Button submit;
	private DataBaseHelper db = new DataBaseHelper(AddTimeRecordActivity.this);
	private ManageEmployeesActivity manageEmployeesActivity = new ManageEmployeesActivity();
	private ManageClientsActivity manageClientsActivity = new ManageClientsActivity();
	private ManageJobTypesActivity manageJobTypesActivity = new ManageJobTypesActivity();

	private ArrayList<Employees> employeesList = new ArrayList<Employees>();
	private ArrayList<Clients> clientsList = new ArrayList<Clients>();
	private ArrayList<JobTypes> jobTypesList = new ArrayList<JobTypes>();

	private EmployeesListAdapter adapterEmployees;
	private ClientsAdapterForFieldNotes adapterClients;
	private JobTypesAdapterForTimeRecord adapterJobTypes;

	private String mClientId;
	private String mEmployeeId;
	private String mJobTypeId;

	private String mStartDate, mEndDate, mStartTime, mEndTime;
	String[] months = { "Jan", "Feb", "March", "April", "May", "June", "July",
			"Aug", "Sep", "Oct", "Nov", "Dec" };
	String[] monthsInNum = { "01", "02", "03", "04", "05", "06", "07", "08",
			"09", "10", "11", "12" };

	private String mType;
	private String mTimeRecordId;

	GpsTracker gpsTracker;
	String latitude;
	String longitude;
	Utils utils = new Utils();

	private String mStartDateGMT, mStartTimeGMT, mEndDateGMT, mEndTimeGMT;

	private boolean isHoursEntered;

	CustomDateTimePicker customPicker;

	private String pickerStr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.add_time_record);
		progressDialog = new ProgressDialog(AddTimeRecordActivity.this);

		title_text = (TextView) findViewById(R.id.title_text);

		all_clients = (Spinner) findViewById(R.id.all_clients);

		all_employees = (Spinner) findViewById(R.id.all_employees);
		all_job_type = (Spinner) findViewById(R.id.all_job_type);
		start_date = (TextView) findViewById(R.id.start_date);
		end_date = (TextView) findViewById(R.id.end_date);
		// enter_rate = (EditText) findViewById(R.id.enter_rate);
		enter_hours = (EditText) findViewById(R.id.enter_hours);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		submit = (Button) findViewById(R.id.submit_time_record);

		gpsTracker = new GpsTracker(this);

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(this);
		}

		Intent intent = getIntent();
		mType = intent.getStringExtra("type");

		if (mType.equalsIgnoreCase("update")) {

			title_text.setText("Edit Time Record");
			mEmployeeId = intent.getStringExtra("employeeId");
			mTimeRecordId = intent.getStringExtra("trid");

			all_employees.setVisibility(View.GONE);
		}

		// assigning clients to the clients spinner

		clientsList = manageClientsActivity
				.getClientsFromDB(AddTimeRecordActivity.this);

		Clients client = new Clients();
		client.setmClientId("0");
		client.setmClientName("All Clients");

		clientsList.add(0, client);

		adapterClients = new ClientsAdapterForFieldNotes(
				AddTimeRecordActivity.this, R.layout.spinner_list_item,
				clientsList);

		all_clients.setAdapter(adapterClients);

		// assigning employees to the employees spinner

		if (!Global.getInstance().getBooleanType(AddTimeRecordActivity.this,
				RequestParameters.IS_EMPLOYER)) {
			Employees employee = new Employees();
			employee.setId(Global.getInstance().getPreferenceVal(
					AddTimeRecordActivity.this, RequestParameters.USER_ID));

			employee.setName(Global.getInstance().getPreferenceVal(
					AddTimeRecordActivity.this, CONSTANTS.USER_NAME));
			employeesList.add(0, employee);

			adapterEmployees = new EmployeesListAdapter(
					AddTimeRecordActivity.this, R.layout.spinner_list_item,
					employeesList);

			all_employees.setAdapter(adapterEmployees);
		} else {
			employeesList = manageEmployeesActivity
					.getEmployeesDetailsFromDB(AddTimeRecordActivity.this);
			Employees employee = new Employees();
			employee.setId("0");
			employee.setName("All Employees");
			employeesList.add(0, employee);

			Employees employee1 = new Employees();

			employee1.setId(Global.getInstance().getPreferenceVal(
					AddTimeRecordActivity.this, RequestParameters.EMPLOYER_ID));
			employee1.setName(Global.getInstance()
					.getPreferenceVal(AddTimeRecordActivity.this,
							RequestParameters.EMPLOYER_NAME));
			employeesList.add(employee1);

			adapterEmployees = new EmployeesListAdapter(
					AddTimeRecordActivity.this, R.layout.spinner_list_item,
					employeesList);

			all_employees.setAdapter(adapterEmployees);
		}

		// employeesList = manageEmployeesActivity
		// .getEmployeesDetailsFromDB(AddTimeRecordActivity.this);
		//
		// Employees employee = new Employees();
		// employee.setId("0");
		// employee.setName("All Employees");
		//
		// employeesList.add(0, employee);
		//
		// adapterEmployees = new
		// EmployeesListAdapter(AddTimeRecordActivity.this,
		// R.layout.spinner_list_item, employeesList);
		//
		// all_employees.setAdapter(adapterEmployees);

		// assigning jobtypes to the spinner

		jobTypesList = manageJobTypesActivity
				.getJobTypesFromDB(AddTimeRecordActivity.this);

		JobTypes jobtypes = new JobTypes();
		jobtypes.setId("0");
		jobtypes.setmJobName("All Job Types");

		jobTypesList.add(0, jobtypes);

		adapterJobTypes = new JobTypesAdapterForTimeRecord(
				AddTimeRecordActivity.this, R.layout.spinner_list_item,
				jobTypesList);

		all_job_type.setAdapter(adapterJobTypes);

		// selecting particular client to get time record

		all_clients.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				Clients client = (Clients) parent.getAdapter()
						.getItem(position);

				mClientId = client.getmClientId();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		all_employees.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (mType.equalsIgnoreCase("add")) {
					Employees employee = (Employees) parent.getAdapter()
							.getItem(position);

					mEmployeeId = employee.getId();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		all_job_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				JobTypes jobType = (JobTypes) parent.getAdapter().getItem(
						position);

				mJobTypeId = jobType.getId();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		start_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// String start = "start";

				// button_click(start_date, start);

				pickerStr = "start";

				customPicker.setDialog();

			}
		});

		end_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// String end = "end";
				// button_click(end_date, end);

				pickerStr = "end";

				customPicker.setDialog();

			}
		});

		customPicker = new CustomDateTimePicker(this,
				new CustomDateTimePicker.ICustomDateTimeListener() {

					@Override
					public void onSet(Dialog dialog, Calendar calendarSelected,
							Date dateSelected, int year, String monthFullName,
							String monthShortName, int monthNumber, int date,
							String weekDayFullName, String weekDayShortName,
							int hour24, int hour12, int min, int sec) {

						if (pickerStr.equalsIgnoreCase("start")) {

							if (Global.getInstance().getBooleanType(
									AddTimeRecordActivity.this,
									CONSTANTS.ENABLE_TIME)) {
								/*********** if the time format is 24 hours **********/

								start_date.setText(calendarSelected
										.get(Calendar.DAY_OF_MONTH)
										+ "-"
										+ months[monthNumber]
										+ "-"
										+ year
										+ " " +

										+hour12 + ":" + min);
							} else {

								/*********** if the time format is 12 hours **********/

								String date1 = calendarSelected
										.get(Calendar.DAY_OF_MONTH)
										+ "-"
										+ months[monthNumber] + "-" + year;

								String timeIn12Hour = Utils
										.Convert24to12(hour12 + ":" + min + ":"
												+ sec);
								start_date.setText(date1 + " " + timeIn12Hour);

							}

							/******** send time in GMT format **********/
							String timeAndDateInGMT = utils
									.convertLocalTimetoGMT(year
											+ "-"
											+ monthsInNum[monthNumber]
											+ "-"
											+ calendarSelected
													.get(Calendar.DAY_OF_MONTH)
											+ " " + hour12 + ":" + min + ":"
											+ sec);

							String timeAndDate[] = timeAndDateInGMT.split(" ");

							mStartDate = timeAndDate[0];

							mStartTime = timeAndDate[1];

							if (isHoursEntered) {
								String timeDifference = utils
										.timeDifferenceCalculate1(mStartDate
												+ " " + mStartTime, mEndDate
												+ " " + mEndTime);

								enter_hours.setText(timeDifference + "hr");
							} else {
								enter_hours.setText("00:00hr");
								isHoursEntered = true;
							}

						} else {

							if (Global.getInstance().getBooleanType(
									AddTimeRecordActivity.this,
									CONSTANTS.ENABLE_TIME)) {
								end_date.setText(calendarSelected
										.get(Calendar.DAY_OF_MONTH)
										+ "-"
										+ months[monthNumber]
										+ "-"
										+ year
										+ " " +

										+hour12 + ":" + min);
							} else {
								String date1 = calendarSelected
										.get(Calendar.DAY_OF_MONTH)
										+ "-"
										+ months[monthNumber] + "-" + year;

								String timeIn12Hour = Utils
										.Convert24to12(hour12 + ":" + min + ":"
												+ sec);
								end_date.setText(date1 + " " + timeIn12Hour);
							}

							/******** send time in GMT format **********/

							String timeAndDateInGMT = utils
									.convertLocalTimetoGMT(year
											+ "-"
											+ monthsInNum[monthNumber]
											+ "-"
											+ calendarSelected
													.get(Calendar.DAY_OF_MONTH)
											+ " " + hour12 + ":" + min + ":"
											+ sec);

							String timeAndDate[] = timeAndDateInGMT.split(" ");

							mEndDate = timeAndDate[0];

							mEndTime = timeAndDate[1];

							if (isHoursEntered) {
								String timeDifference = utils
										.timeDifferenceCalculate1(mStartDate
												+ " " + mStartTime, mEndDate
												+ " " + mEndTime);

								enter_hours.setText(timeDifference + "hr");
							} else {
								enter_hours.setText("00:00hr");
								isHoursEntered = true;
							}

						}

					}

					@Override
					public void onCancel() {

					}
				});

		/**
		 * Pass Directly current time format it will return AM and PM if you set
		 * false
		 */

		if (Global.getInstance().getBooleanType(AddTimeRecordActivity.this,
				CONSTANTS.ENABLE_TIME)) {
			customPicker.set24HourFormat(true);
		} else {
			customPicker.set24HourFormat(false);
		}

		/**
		 * Pass Directly current data and time to show when it pop up
		 */
		customPicker.setDate(Calendar.getInstance());

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!mClientId.equalsIgnoreCase("0")) {
					if (!mEmployeeId.equalsIgnoreCase("0")) {
						if (!mJobTypeId.equalsIgnoreCase("0")) {

							if (mStartDate != null) {

								if (mEndDate != null) {

									if (utils.isCorrectdateAndTime(mStartDate,
											mStartTime, mEndDate, mEndTime)) {

										if (!enter_hours.getText().toString()
												.equalsIgnoreCase("")) {

											if (mType.equalsIgnoreCase("add")) {

												if (utils
														.isConnectingToInternet(AddTimeRecordActivity.this)) {
													// add time record
													new AddTimeRecordTask()
															.execute("");
												} else {
													utils.showDialog(AddTimeRecordActivity.this);
												}
											} else {

												if (utils
														.isConnectingToInternet(AddTimeRecordActivity.this)) {
													// update time record
													new UpdateTimeRecordTask()
															.execute("");
												} else {
													utils.showDialog(AddTimeRecordActivity.this);
												}
											}

										} else {
											Toast.makeText(
													getApplicationContext(),
													"Enter Hours",
													Toast.LENGTH_SHORT).show();
										}

									} else {
										Toast.makeText(
												getApplicationContext(),
												"Select Correct  End date and time",
												Toast.LENGTH_SHORT).show();
									}
								} else {
									Toast.makeText(getApplicationContext(),
											"Select End date and time",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(getApplicationContext(),
										"Select Start date and time",
										Toast.LENGTH_SHORT).show();

							}
						} else {
							Toast.makeText(getApplicationContext(),
									"Select Job Type", Toast.LENGTH_SHORT)
									.show();
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Select Employee", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getApplicationContext(), "Select Client",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	// public void button_click(final TextView view, final String type) {
	// // Create the dialog
	// final Dialog mDateTimeDialog = new Dialog(this);
	// // Inflate the root layout
	// final RelativeLayout mDateTimeDialogView = (RelativeLayout)
	// getLayoutInflater()
	// .inflate(R.layout.date_time_dialog, null);
	// // Grab widget instance
	// final DateTimePicker mDateTimePicker = (DateTimePicker)
	// mDateTimeDialogView
	// .findViewById(R.id.DateTimePicker);
	// mDateTimePicker.setDateChangedListener(this);
	//
	// // Update demo TextViews when the "OK" button is clicked
	// ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime))
	// .setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// mDateTimePicker.clearFocus();
	// // TODO Auto-generated method stub
	// String result_string = mDateTimePicker.getDay()
	// + "-"
	// + String.valueOf(months[Integer
	// .parseInt(mDateTimePicker.getMonth()) - 1])
	// + "-"
	// + String.valueOf(mDateTimePicker.getYear())
	// + "  "
	// + String.valueOf(mDateTimePicker.getHour())
	// + ":"
	// + String.valueOf(mDateTimePicker.getMinute());
	//
	// // if (mDateTimePicker.getHour() > 12) {
	// // result_string =
	// //
	// // result_string + "PM";
	// // } else {
	// // result_string = result_string + "AM";
	// // }
	// view.setText(result_string);
	//
	// if (type.equalsIgnoreCase("start")) {
	// mStartDateGMT = mDateTimePicker.getYear() + "-"
	// + mDateTimePicker.getMonth() + "-"
	// + mDateTimePicker.getDay();
	//
	// mStartTimeGMT = mDateTimePicker.getHour() + ":"
	// + mDateTimePicker.getMinute() + ":" + "00";
	//
	// String timeAndDateInGMT = utils
	// .convertLocalTimetoGMT(mStartDateGMT + " "
	// + mStartTimeGMT);
	//
	// String timeAndDate[] = timeAndDateInGMT.split(" ");
	//
	// mStartDate = timeAndDate[0];
	//
	// mStartTime = timeAndDate[1];
	//
	// if (isHoursEntered) {
	// String timeDifference = utils
	// .timeDifferenceCalculate1(mStartDate
	// + " " + mStartTime, mEndDate
	// + " " + mEndTime);
	//
	// enter_hours.setText(timeDifference + "hr");
	// } else {
	// enter_hours.setText("00:00hr");
	// isHoursEntered = true;
	// }
	//
	// } else {
	// mEndDateGMT = mDateTimePicker.getYear() + "-"
	// + mDateTimePicker.getMonth() + "-"
	// + mDateTimePicker.getDay();
	//
	// mEndTimeGMT = mDateTimePicker.getHour() + ":"
	// + mDateTimePicker.getMinute() + ":" + "00";
	//
	// String timeAndDateOutGMT = utils
	// .convertLocalTimetoGMT(mEndDateGMT + " "
	// + mEndTimeGMT);
	//
	// String timeAndDate[] = timeAndDateOutGMT.split(" ");
	//
	// mEndDate = timeAndDate[0];
	//
	// mEndTime = timeAndDate[1];
	//
	// if (isHoursEntered) {
	// String timeDifference = utils
	// .timeDifferenceCalculate1(mStartDate
	// + " " + mStartTime, mEndDate
	// + " " + mEndTime);
	//
	// enter_hours.setText(timeDifference + "hr");
	// } else {
	// enter_hours.setText("00:00hr");
	// isHoursEntered = true;
	// }
	//
	// }
	//
	// mDateTimeDialog.dismiss();
	// }
	// });
	//
	// // Cancel the dialog when the "Cancel" button is clicked
	// ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog))
	// .setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// mDateTimeDialog.cancel();
	// }
	// });
	//
	// // Reset Date and Time pickers when the "Reset" button is clicked
	//
	// ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime))
	// .setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// mDateTimePicker.reset();
	// }
	// });
	//
	// // Setup TimePicker
	// // No title on the dialog window
	// mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// // Set the dialog content view
	// mDateTimeDialog.setContentView(mDateTimeDialogView);
	// // Display the dialog
	// mDateTimeDialog.show();
	// }

	// public void onDateChanged(Calendar c) { // goi khi co thay doi tu
	// calendar
	// Log.e("",
	// "" + c.get(Calendar.MONTH) + " " + c.get(Calendar.DAY_OF_MONTH)
	// + " " + c.get(Calendar.YEAR));
	// }

	private class AddTimeRecordTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.ADD_TIME_RECORD));

				entity.addPart(RequestParameters.USER_ID, new StringBody(
						mEmployeeId));

				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));

				entity.addPart(RequestParameters.JOBTYPE_ID, new StringBody(
						mJobTypeId));

				entity.addPart(RequestParameters.IN_TIME, new StringBody(
						mStartTime));

				entity.addPart(RequestParameters.IN_DATE, new StringBody(
						mStartDate));

				entity.addPart(RequestParameters.OUT_TIME, new StringBody(
						mEndTime));

				entity.addPart(RequestParameters.OUT_DATE, new StringBody(
						mEndDate));
				entity.addPart(RequestParameters.START_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.START_LNG, new StringBody(
						longitude));
				entity.addPart(RequestParameters.END_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.END_LNG, new StringBody(
						longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject employeeJson = jsonResponse
						.getJSONObject("response");
				String success = employeeJson.getString("success");
				String message = employeeJson.getString("message");

				String outtime;
				String outdate;

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Success"))) {
					JSONObject JsonTimeRecord = employeeJson
							.getJSONObject("timerecord");

					String name = JsonTimeRecord.getString("name");
					String email = JsonTimeRecord.getString("email");

					String address = JsonTimeRecord.getString("address");

					String password = JsonTimeRecord.getString("password");

					String phoneno = JsonTimeRecord.getString("phoneno");

					String userhourlyrate = JsonTimeRecord
							.getString("userhourlyrate");

					String devicetoken = JsonTimeRecord
							.getString("devicetoken");

					String online = JsonTimeRecord.getString("online");

					String status = JsonTimeRecord.getString("status");

					String id = JsonTimeRecord.getString("id");

					String userid = JsonTimeRecord.getString("userid");
					String clientid = JsonTimeRecord.getString("clientid");

					String jobtypeid = JsonTimeRecord.getString("jobtypeid");

					//
					// String intime = JsonTimeRecord.getString("intime");
					//
					// String indate = JsonTimeRecord.getString("indate");
					//
					// String outtime = JsonTimeRecord.getString("outtime");
					//
					// String outdate = JsonTimeRecord.getString("outdate");

					String workingEmployeeInTimeGMT = JsonTimeRecord
							.getString("intime");
					String workingEmployeeInDateGMT = JsonTimeRecord
							.getString("indate");

					String IntimeAndDateInLocal = utils
							.convertGMTtoLocalTime(workingEmployeeInDateGMT
									+ " " + workingEmployeeInTimeGMT);

					String IntimeAndDate[] = IntimeAndDateInLocal.split(" ");

					String intime = IntimeAndDate[1];
					String indate = IntimeAndDate[0];

					//

					String workingEmployeeOutTimeGMT = JsonTimeRecord
							.getString("outtime");
					String workingEmployeeOutDateGMT = JsonTimeRecord
							.getString("outdate");

					if (!workingEmployeeOutTimeGMT.equals("null")) {
						String OuttimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeOutDateGMT
										+ " " + workingEmployeeOutTimeGMT);

						String outTimeAndDate[] = OuttimeAndDateInLocal
								.split(" ");

						outtime = outTimeAndDate[1];

						outdate = outTimeAndDate[0];
					} else {
						outtime = workingEmployeeOutTimeGMT;

						outdate = workingEmployeeOutDateGMT;
					}

					String startlat = JsonTimeRecord.getString("startlat");

					String startlng = JsonTimeRecord.getString("startlng");

					String endlat = JsonTimeRecord.getString("endlat");

					String endlng = JsonTimeRecord.getString("endlng");

					String active = JsonTimeRecord.getString("active");

					String createdGMT = JsonTimeRecord.getString("created");

					String created = utils.convertGMTtoLocalTime(createdGMT);

					String clientname = JsonTimeRecord.getString("clientname");

					String clientaddress = JsonTimeRecord
							.getString("clientaddress");

					String jobname = JsonTimeRecord.getString("jobname");

					String isApproved = JsonTimeRecord.getString("isapprove");

					String isManuallyAdded = JsonTimeRecord
							.getString("ismanuallyadded");

					// inserting all the time records into the database

					db.open();
					db.insertTimeRecords(name, email, address, password,
							phoneno, userhourlyrate, devicetoken, online,
							status, id, userid, clientid, jobtypeid, intime,
							indate, outtime, outdate, startlat, startlng,
							endlat, endlng, active, created, clientname,
							clientaddress, jobname, isApproved, isManuallyAdded);
					db.close();

				}

				Intent intent = new Intent(AddTimeRecordActivity.this,
						TimeRecordsActivity.class);
				startActivity(intent);
				finish();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent = new Intent(AddTimeRecordActivity.this,
				TimeRecordsActivity.class);
		startActivity(intent);
		finish();

	}

	private class UpdateTimeRecordTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.UPDATE_TIME_RECORD));

				entity.addPart(RequestParameters.TR_ID, new StringBody(
						mTimeRecordId));

				entity.addPart(RequestParameters.USER_ID, new StringBody(
						mEmployeeId));

				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));

				entity.addPart(RequestParameters.JOBTYPE_ID, new StringBody(
						mJobTypeId));

				entity.addPart(RequestParameters.IN_TIME, new StringBody(
						mStartTime));

				entity.addPart(RequestParameters.IN_DATE, new StringBody(
						mStartDate));

				entity.addPart(RequestParameters.OUT_TIME, new StringBody(
						mEndTime));

				entity.addPart(RequestParameters.OUT_DATE, new StringBody(
						mEndDate));
				entity.addPart(RequestParameters.START_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.START_LNG, new StringBody(
						longitude));
				entity.addPart(RequestParameters.END_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.END_LNG, new StringBody(
						longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject employeeJson = jsonResponse
						.getJSONObject("response");
				String success = employeeJson.getString("success");
				String message = employeeJson.getString("message");

				String outtime;
				String outdate;

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Success"))) {
					JSONObject JsonTimeRecord = employeeJson
							.getJSONObject("timerecord");

					String name = JsonTimeRecord.getString("name");
					String email = JsonTimeRecord.getString("email");

					String address = JsonTimeRecord.getString("address");

					String password = JsonTimeRecord.getString("password");

					String phoneno = JsonTimeRecord.getString("phoneno");

					String userhourlyrate = JsonTimeRecord
							.getString("userhourlyrate");

					String devicetoken = JsonTimeRecord
							.getString("devicetoken");

					String online = JsonTimeRecord.getString("online");

					String status = JsonTimeRecord.getString("status");

					String id = JsonTimeRecord.getString("id");

					String userid = JsonTimeRecord.getString("userid");
					String clientid = JsonTimeRecord.getString("clientid");

					String jobtypeid = JsonTimeRecord.getString("jobtypeid");

					//
					// String intime = JsonTimeRecord.getString("intime");
					//
					// String indate = JsonTimeRecord.getString("indate");
					//
					// String outtime = JsonTimeRecord.getString("outtime");
					//
					// String outdate = JsonTimeRecord.getString("outdate");

					String workingEmployeeInTimeGMT = JsonTimeRecord
							.getString("intime");
					String workingEmployeeInDateGMT = JsonTimeRecord
							.getString("indate");

					String IntimeAndDateInLocal = utils
							.convertGMTtoLocalTime(workingEmployeeInDateGMT
									+ " " + workingEmployeeInTimeGMT);

					String IntimeAndDate[] = IntimeAndDateInLocal.split(" ");

					String intime = IntimeAndDate[1];
					String indate = IntimeAndDate[0];

					//

					String workingEmployeeOutTimeGMT = JsonTimeRecord
							.getString("outtime");
					String workingEmployeeOutDateGMT = JsonTimeRecord
							.getString("outdate");

					if (!workingEmployeeOutTimeGMT.equals("null")) {
						String OuttimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeOutDateGMT
										+ " " + workingEmployeeOutTimeGMT);

						String outTimeAndDate[] = OuttimeAndDateInLocal
								.split(" ");

						outtime = outTimeAndDate[1];

						outdate = outTimeAndDate[0];
					} else {
						outtime = workingEmployeeOutTimeGMT;

						outdate = workingEmployeeOutDateGMT;
					}

					String startlat = JsonTimeRecord.getString("startlat");

					String startlng = JsonTimeRecord.getString("startlng");

					String endlat = JsonTimeRecord.getString("endlat");

					String endlng = JsonTimeRecord.getString("endlng");

					String active = JsonTimeRecord.getString("active");

					String createdGMT = JsonTimeRecord.getString("created");

					String created = utils.convertGMTtoLocalTime(createdGMT);

					String clientname = JsonTimeRecord.getString("clientname");

					String clientaddress = JsonTimeRecord
							.getString("clientaddress");

					String jobname = JsonTimeRecord.getString("jobname");

					String isApproved = JsonTimeRecord.getString("isapprove");

					String isManuallyAdded = JsonTimeRecord
							.getString("ismanuallyadded");

					// inserting all the time records into the database

					db.open();
					db.updateTimeRecordDetails(name, email, address, password,
							phoneno, userhourlyrate, devicetoken, online,
							status, id, userid, clientid, jobtypeid, intime,
							indate, outtime, outdate, startlat, startlng,
							endlat, endlng, active, created, clientname,
							clientaddress, jobname, isApproved, isManuallyAdded);
					db.close();

					Intent intent = new Intent(AddTimeRecordActivity.this,
							TimeRecordsActivity.class);
					startActivity(intent);
					finish();

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
