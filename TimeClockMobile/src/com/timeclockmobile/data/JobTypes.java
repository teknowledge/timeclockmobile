package com.timeclockmobile.data;

public class JobTypes {

	public String id;
	public String mEmplloyerId;
	public String mJobName;
	public String mActive;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getmEmplloyerId() {
		return mEmplloyerId;
	}

	public void setmEmplloyerId(String mEmplloyerId) {
		this.mEmplloyerId = mEmplloyerId;
	}

	public String getmJobName() {
		return mJobName;
	}

	public void setmJobName(String mJobName) {
		this.mJobName = mJobName;
	}

	public String getmActive() {
		return mActive;
	}

	public void setmActive(String mActive) {
		this.mActive = mActive;
	}

}
