package com.timeclockmobile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.timeclockmobile.adapter.EmployeesAdapter;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Utils;

public class ManageEmployeesActivity extends Activity {

	private ListView mEmployeesListView;
	private TextView title_text;
	private ImageButton add_employee, backBtn;
	private ArrayList<Employees> employeesArrayList = new ArrayList<Employees>();
	private DataBaseHelper db;
	private EmployeesAdapter adapter;

	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.manage_clients);

		add_employee = (ImageButton) findViewById(R.id.add);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		title_text = (TextView) findViewById(R.id.title_text);

		mEmployeesListView = (ListView) findViewById(R.id.clientsList);

		title_text.setText("Manage Employees");

		employeesArrayList = getEmployeesDetailsFromDB(ManageEmployeesActivity.this);

		adapter = new EmployeesAdapter(ManageEmployeesActivity.this,
				R.layout.manage_clients_inflate, employeesArrayList);

		mEmployeesListView.setAdapter(adapter);

		add_employee.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ManageEmployeesActivity.this,
						AddEmployeeActivity.class);
				intent.putExtra("type", "add");
				startActivity(intent);
				finish();
			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	public ArrayList<Employees> getEmployeesDetailsFromDB(Context mContext) {
		// TODO Auto-generated method stub
		db = new DataBaseHelper(mContext);

		ArrayList<Employees> employeesArrayList1 = new ArrayList<Employees>();
		db.open();
		Cursor employeesDetails = db.getEmployeesDetails();

		if (employeesDetails.moveToFirst()) {
			do {

				String employee_id = employeesDetails.getString(0);
				String employee_name = employeesDetails.getString(1);
				String employee_email = employeesDetails.getString(2);
				String employee_address = employeesDetails.getString(3);
				String employee_password = employeesDetails.getString(4);
				String employee_phoneno = employeesDetails.getString(5);
				String employee_hourlyRate = employeesDetails.getString(6);

				String employee_is_working = employeesDetails.getString(7);
				String employee_working_tr_id = employeesDetails.getString(8);
				String employee_working_client_id = employeesDetails
						.getString(9);
				String employee_working_jobtype_id = employeesDetails
						.getString(10);
				String employee_working_intime = employeesDetails.getString(11);
				String employee_working_indate = employeesDetails.getString(12);
				String employee_working_startlat = employeesDetails
						.getString(13);
				String employee_working_startlng = employeesDetails
						.getString(14);
				String employee_working_client_name = employeesDetails
						.getString(15);
				String employee_working_jobname = employeesDetails
						.getString(16);

				String employee_working_outtime = employeesDetails
						.getString(17);

				String employee_working_outdate = employeesDetails
						.getString(18);

				String isBreakStarted = employeesDetails.getString(19);

				String online = employeesDetails.getString(20);
				String created = employeesDetails.getString(21);
				String end_lat = employeesDetails.getString(22);
				String end_lng = employeesDetails.getString(23);
				String status = employeesDetails.getString(24);
				String deviceToken = employeesDetails.getString(25);

				Employees employee = new Employees();
				employee.setId(employee_id);
				employee.setName(employee_name);
				employee.setEmail(employee_email);
				employee.setAddress(employee_address);
				employee.setPassword(employee_password);
				if (employee_working_outtime.equalsIgnoreCase("null")) {
					if (!employee_working_intime.equalsIgnoreCase("")) {
						DateFormat df = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						String TodayDate = df.format(Calendar.getInstance()
								.getTime());

						String dateStart = employee_working_indate + " "
								+ employee_working_intime;

						String timer_value = utils.timeDifferenceCalculate(
								dateStart, TodayDate);

						employee.setTimerText(timer_value);

						db.open();
						db.insertClockOnDetails(employee_working_jobtype_id,
								employee_working_jobname, employee_id,
								employee_working_outdate, employee_password,
								employee_working_client_name, online,
								employee_working_indate,
								employee_working_client_id,
								employee_working_tr_id, created, employee_name,
								employee_working_intime, employee_phoneno,
								end_lng, employee_working_outtime,
								employee_hourlyRate, end_lat, status,
								employee_working_startlat, deviceToken,
								employee_working_startlng, employee_address,
								employee_email, "", "");
						db.close();

						// employee.setChecked(true);
					}

				}

				employee.setPhoneno(employee_phoneno);
				employee.setHourlyrate(employee_hourlyRate);

				employee.setIsWorking(employee_is_working);

				employee.setWorking_tr_id(employee_working_tr_id);
				employee.setWorking_client_id(employee_working_client_id);
				employee.setWorking_jobTypeId(employee_working_jobtype_id);
				employee.setWorking_intime(employee_working_intime);
				employee.setWorking_indate(employee_working_indate);
				employee.setWorking_startlat(employee_working_startlat);
				employee.setWorking_startlng(employee_working_startlng);

				employee.setWorking_clientName(employee_working_client_name);
				employee.setWorking_jobName(employee_working_jobname);

				employee.setWorking_outtime(employee_working_outtime);
				employee.setWorking_outdate(employee_working_outdate);
				employee.setIsBreakStarted(isBreakStarted);

				employeesArrayList1.add(employee);

			} while (employeesDetails.moveToNext());
		}
		employeesDetails.close();
		db.close();
		return employeesArrayList1;
	}
}
