package com.timeclockmobile.data;

import java.io.Serializable;
import java.util.ArrayList;

public class TimeRecords implements Serializable {

	public String name;
	public String email;
	public String address;
	public String password;
	public String phoneno;
	public String userhourlyrate;
	public String devicetoken;
	public String online;
	public String status;
	public String id;
	public String userid;
	public String clientid;
	public String jobtypeid;
	public String intime;
	public String indate;
	public String outtime;
	public String outdate;
	public String startlat;
	public String startlng;
	public String endlat;
	public String endlng;
	public String active;
	public String created;
	public String clientname;
	public String clientaddress;
	public String jobname;
	public String totalHours;
	public String isApproved;
	public ArrayList<Breaks> breaksList;

	public ArrayList<Breaks> getBreaksList() {
		return breaksList;
	}

	public void setBreaksList(ArrayList<Breaks> breaksList) {
		this.breaksList = breaksList;
	}

	public String getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}

	public String getIsManuallyAdded() {
		return isManuallyAdded;
	}

	public void setIsManuallyAdded(String isManuallyAdded) {
		this.isManuallyAdded = isManuallyAdded;
	}

	public String isManuallyAdded;

	public String getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(String totalHours) {
		this.totalHours = totalHours;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String totalAmount;

	public boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getUserhourlyrate() {
		return userhourlyrate;
	}

	public void setUserhourlyrate(String userhourlyrate) {
		this.userhourlyrate = userhourlyrate;
	}

	public String getDevicetoken() {
		return devicetoken;
	}

	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getJobtypeid() {
		return jobtypeid;
	}

	public void setJobtypeid(String jobtypeid) {
		this.jobtypeid = jobtypeid;
	}

	public String getIntime() {
		return intime;
	}

	public void setIntime(String intime) {
		this.intime = intime;
	}

	public String getIndate() {
		return indate;
	}

	public void setIndate(String indate) {
		this.indate = indate;
	}

	public String getOuttime() {
		return outtime;
	}

	public void setOuttime(String outtime) {
		this.outtime = outtime;
	}

	public String getOutdate() {
		return outdate;
	}

	public void setOutdate(String outdate) {
		this.outdate = outdate;
	}

	public String getStartlat() {
		return startlat;
	}

	public void setStartlat(String startlat) {
		this.startlat = startlat;
	}

	public String getStartlng() {
		return startlng;
	}

	public void setStartlng(String startlng) {
		this.startlng = startlng;
	}

	public String getEndlat() {
		return endlat;
	}

	public void setEndlat(String endlat) {
		this.endlat = endlat;
	}

	public String getEndlng() {
		return endlng;
	}

	public void setEndlng(String endlng) {
		this.endlng = endlng;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getClientname() {
		return clientname;
	}

	public void setClientname(String clientname) {
		this.clientname = clientname;
	}

	public String getClientaddress() {
		return clientaddress;
	}

	public void setClientaddress(String clientaddress) {
		this.clientaddress = clientaddress;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

}
