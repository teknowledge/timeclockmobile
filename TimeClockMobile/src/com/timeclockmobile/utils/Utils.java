package com.timeclockmobile.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.timeclockmobile.SettingsActivity;
import com.timeclockmobile.TimeRecordsActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {
	public boolean isConnectingToInternet(Context mContext) {
		ConnectivityManager connectivity = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

	public String splitToComponentTimes(int duration) {
		String totalTime = null;
		long longVal = (long) duration;
		int hours = (int) longVal / 3600;
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		// int[] ints = { hours, mins, secs };

		// totalTime = hours + "H: " + mins + "M: " + secs + "S";

		totalTime = String.format("%02d", hours) + ":" + ""
				+ String.format("%02d", mins) + ":"

				+ String.format("%02d", secs);
		return totalTime;
	}

	public long timeDifferenceBetweenBreaks(String startTime, String endTime) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		long difference = 0;

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(startTime);
			d2 = format.parse(endTime);

			// in milliseconds
			difference = d2.getTime() - d1.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return difference;

	}

	public String[] timeDifferenceBreaksAndWorkTime(String startDateAndTime,
			String endDateAndTime, long differenceBreaks) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		long difference = 0;
		long differenceBetweenBreakAndWork;

		Date d1 = null;
		Date d2 = null;
		String time[] = new String[3];
		try {
			d1 = format.parse(startDateAndTime);
			d2 = format.parse(endDateAndTime);

			// in milliseconds
			difference = d2.getTime() - d1.getTime();
			differenceBetweenBreakAndWork = difference - differenceBreaks;
			long diffSeconds = differenceBetweenBreakAndWork / 1000 % 60;
			long diffMinutes = differenceBetweenBreakAndWork / (60 * 1000) % 60;
			long diffHours = differenceBetweenBreakAndWork / (60 * 60 * 1000);
			// % 24;
			// long diffDays = diff / (24 * 60 * 60 * 1000);

			// System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");

			// time = diffHours + diffMinutes + "";
			time[0] = diffHours + "";
			time[1] = diffMinutes + "";
			time[2] = diffSeconds + "";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return time;

	}

	public String[] timeDifference(String startTime, String endTime) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date d1 = null;
		Date d2 = null;
		String time[] = new String[3];
		try {
			d1 = format.parse(startTime);
			d2 = format.parse(endTime);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000);
			// % 24;
			// long diffDays = diff / (24 * 60 * 60 * 1000);

			// System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");

			// time = diffHours + diffMinutes + "";
			time[0] = diffHours + "";
			time[1] = diffMinutes + "";
			time[2] = diffSeconds + "";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return time;

	}

	public String timeDifferenceCalculate(String startTime, String endTime) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date d1 = null;
		Date d2 = null;
		String time = null;
		try {
			d1 = format.parse(startTime);
			d2 = format.parse(endTime);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000);
			// % 24;
			// long diffDays = diff / (24 * 60 * 60 * 1000);

			// System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");

			// time = diffHours + diffMinutes + "";

			time = String.format("%02d", (int) (diffHours)) + ":"
					+ String.format("%02d", (int) (diffMinutes)) + ":"
					+ String.format("%02d", (int) (diffSeconds));

			// time[0] = diffHours + "";
			// time[1] = diffMinutes + "";
			// time[2] = diffSeconds + "";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return time;

	}

	public String timeDifferenceCalculate1(String startTime, String endTime) {

		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date d1 = null;
		Date d2 = null;
		String time = null;
		try {
			d1 = format.parse(startTime);
			d2 = format.parse(endTime);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000);
			// % 24;
			// long diffDays = diff / (24 * 60 * 60 * 1000);

			// System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");

			// time = diffHours + diffMinutes + "";

			time = String.format("%02d", (int) (diffHours)) + ":"
					+ String.format("%02d", (int) (diffMinutes));

			// time[0] = diffHours + "";
			// time[1] = diffMinutes + "";
			// time[2] = diffSeconds + "";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return time;

	}

	public String[] formatHoursAndMinutes(int totalMinutes) {
		String minutes = Integer.toString(totalMinutes % 60);
		minutes = minutes.length() == 1 ? "0" + minutes : minutes;

		String[] hoursAndMinutes = new String[2];
		hoursAndMinutes[0] = (totalMinutes / 60) + "";
		hoursAndMinutes[1] = minutes;
		return hoursAndMinutes;
	}

	public String[] formatMinutesAndSeconds(int totalSeconds) {
		String seconds = Integer.toString(totalSeconds % 60);
		seconds = seconds.length() == 1 ? "0" + seconds : seconds;

		String[] minutesAndSeconds = new String[2];
		minutesAndSeconds[0] = (totalSeconds / 60) + "";
		minutesAndSeconds[1] = seconds;
		return minutesAndSeconds;
	}

	// convert time 24 format to 12 hours
	public static String Convert24to12(String time) {
		String convertedTime = "";
		try {
			SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
			SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
			Date date = parseFormat.parse(time);
			convertedTime = displayFormat.format(date);
			System.out.println("convertedTime : " + convertedTime);

		} catch (final ParseException e) {
			e.printStackTrace();
		}
		return convertedTime;
		// Output will be 10:23 PM
	}

	public int roundUp(int n, Context ctx) {

		int k = Integer.parseInt(Global.getInstance().getPreferenceVal(ctx,
				CONSTANTS.ROUNDING_VALUE));
		return (n + 4) / k * k;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void removeDuplicates(List list) {
		Set uniqueEntries = new HashSet();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (!uniqueEntries.add(element)) // if current element is a
												// duplicate,
				iter.remove(); // remove it
		}
	}

	public void showDialog(Context mContext) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle("No Internet Connection");

		// set dialog message
		alertDialogBuilder
		// .setMessage("Delete the artist")
				.setCancelable(false).setPositiveButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity

								dialog.cancel();

							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	/*************** convert time into milli seconds **************/

	public long convertDateIntoMilliseconds(String indate, String inTime) {
		// String string_date = "2015-01-22 07:39:56";

		String string_date = indate + " " + inTime;
		String time = "";

		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = null;
		try {
			d = f.parse(string_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long milliseconds = d.getTime();
		long now = System.currentTimeMillis();

		long difference = now - milliseconds;
		return difference;
	}

	// validating email id
	public static boolean isValidEmail(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public boolean isCorrectdateAndTime(String startDate, String startTime,
			String endDate, String endTime) {

		String startDateAndTime = startDate + " " + startTime;

		String endDateAndTime = endDate + " " + endTime;

		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d = null;
		try {
			d = f.parse(startDateAndTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long milliseconds = d.getTime();

		SimpleDateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d1 = null;
		try {
			d1 = f1.parse(endDateAndTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long milliseconds1 = d1.getTime();

		long difference = milliseconds1 - milliseconds;

		if (difference > 0) {
			return true;
		} else {
			return false;
		}

	}

	public String convertGMTtoLocalTime(String timeZoneInGMT) {

		String timeZoneInLocal = null;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		TimeZone utcZone = TimeZone.getTimeZone("UTC");
		simpleDateFormat.setTimeZone(utcZone);
		Date myDate = null;
		try {
			myDate = simpleDateFormat.parse(timeZoneInGMT);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		simpleDateFormat.setTimeZone(TimeZone.getDefault());
		timeZoneInLocal = simpleDateFormat.format(myDate);

		return timeZoneInLocal;

	}

	public String convertLocalTimetoGMT(String localTime) {

		String timeInGMT = null;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		// TimeZone utcZone = TimeZone.getTimeZone(TimeZone.getDefault());
		simpleDateFormat.setTimeZone(TimeZone.getDefault());
		Date myDate = null;
		try {
			myDate = simpleDateFormat.parse(localTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TimeZone utcZone = TimeZone.getTimeZone("UTC");
		simpleDateFormat.setTimeZone(utcZone);
		timeInGMT = simpleDateFormat.format(myDate);

		return timeInGMT;

	}

}
