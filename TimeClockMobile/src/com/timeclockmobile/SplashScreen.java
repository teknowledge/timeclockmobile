package com.timeclockmobile;

import com.timeclockmobile.utils.Global;
import com.timeclockmobile.webservices.RequestParameters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends Activity {

	String session = "LOGIN";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splash_screen);

		final String login = Global.getInstance().getPreferenceVal(
				SplashScreen.this, RequestParameters.SESSION);

		Handler handle = new Handler();
		Runnable action = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (session.equals(login)) {
					Intent intent = new Intent(getApplicationContext(),
							HomeActivity.class);
					startActivity(intent);
					finish();
				} else {
					Intent intent = new Intent(getApplicationContext(),
							MainActivity.class);
					startActivity(intent);
					finish();
				}

			}
		};

		handle.postDelayed(action, 2000);
	}

}
