package com.timeclockmobile.data;

import java.io.Serializable;

public class ClockedOnUser implements Serializable {
	public String user_id;
	public String name;
	public String clientName;
	public String jobName;
	public String timeRecordId;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getTimeRecordId() {
		return timeRecordId;
	}

	public void setTimeRecordId(String timeRecordId) {
		this.timeRecordId = timeRecordId;
	}

}
