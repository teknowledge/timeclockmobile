package com.timeclockmobile.adapter;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.google.android.gms.drive.internal.ar;
import com.timeclockmobile.GoogleMapViewActivity;
import com.timeclockmobile.R;
import com.timeclockmobile.TimeRecordDetailsActivity;
import com.timeclockmobile.TimeRecordsActivity;
import com.timeclockmobile.TimeRecordDetailsActivity.DeleteTimeRecordTask;
import com.timeclockmobile.data.Breaks;
import com.timeclockmobile.data.TimeRecords;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class TimeRecordsAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<TimeRecords> mTimeRecordsList;
	LayoutInflater inflater;

	private String clientId;
	private DataBaseHelper db;

	private String inDate;
	Utils utils = new Utils();
	TimeRecordDetailsActivity mActivity;

	ArrayList<String> checkedTimeRecords = new ArrayList<String>();
	ArrayList<TimeRecords> checkedTRObject = new ArrayList<TimeRecords>();

	public TimeRecordsAdapter(Context mContext, int layoutResourceId,
			ArrayList<TimeRecords> TimeRecordsArrayList) {
		this.mContext = mContext;
		this.mTimeRecordsList = TimeRecordsArrayList;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mActivity = new TimeRecordDetailsActivity();
		db = new DataBaseHelper(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mTimeRecordsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mTimeRecordsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.tr_user_name = (TextView) convertView
					.findViewById(R.id.tr_user_name);

			holder.tr_date = (TextView) convertView.findViewById(R.id.tr_date);

			holder.view_gps = (TextView) convertView
					.findViewById(R.id.view_gps);

			holder.tr_time = (TextView) convertView.findViewById(R.id.tr_time);

			holder.tr_client_name = (TextView) convertView
					.findViewById(R.id.tr_client_name);

			holder.tr_amount = (TextView) convertView
					.findViewById(R.id.tr_amount);
			holder.manual = (TextView) convertView.findViewById(R.id.manual);

			holder.break_time = (TextView) convertView
					.findViewById(R.id.break_time);

			holder.approve = (Button) convertView.findViewById(R.id.aprove);
			holder.deny = (Button) convertView.findViewById(R.id.deny);

			holder.check_time_record = (CheckBox) convertView
					.findViewById(R.id.check_time_record);

			holder.check_time_record
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub

							int getPosition = (Integer) buttonView.getTag();

							mTimeRecordsList.get(getPosition).setChecked(
									buttonView.isChecked());

							if (isChecked) {

								// itemChecked[position] = true;
								checkedTimeRecords.add(mTimeRecordsList.get(
										getPosition).getId());

								checkedTRObject.add(mTimeRecordsList
										.get(getPosition));

							} else {

								// itemChecked[position] = false;
								checkedTimeRecords.remove(mTimeRecordsList.get(
										getPosition).getId());

								checkedTRObject.remove(mTimeRecordsList
										.get(getPosition));

							}
						}
					});

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.check_time_record.setTag(position);
		holder.tr_user_name.setText(mTimeRecordsList.get(position).getName());

		holder.tr_client_name.setText(mTimeRecordsList.get(position)
				.getClientname());
		if (mTimeRecordsList.get(position).isManuallyAdded
				.equalsIgnoreCase("1")) {
			holder.manual.setVisibility(View.VISIBLE);
		}

		String date[] = mTimeRecordsList.get(position).getIndate().split("-");

		if (Global.getInstance()
				.getPreferenceVal(mContext, CONSTANTS.DATE_FORMAT)
				.equalsIgnoreCase("DD/MM/YYYY")) {
			// inDate = new SimpleDateFormat("dd/MM/yyyy EEEE").format(new
			// Date());

			inDate = date[2] + "/" + date[1] + "/" + date[0];
		} else {

			// inDate = new SimpleDateFormat("MM/dd/yyyy EEEE").format(new
			// Date());

			inDate = date[1] + "/" + date[2] + "/" + date[0];
		}

		if (Global.getInstance()
				.getBooleanType(mContext, CONSTANTS.ENABLE_TIME)) {
			holder.tr_date.setText(inDate + "  "
					+ mTimeRecordsList.get(position).getIntime());
		} else {
			holder.tr_date.setText(inDate
					+ "  "
					+ Utils.Convert24to12(mTimeRecordsList.get(position)
							.getIntime()));
		}

		// holder.tr_amount.setText(mTimeRecordsList.get(position)
		// .getUserhourlyrate());
		holder.check_time_record.setChecked(mTimeRecordsList.get(position)
				.isChecked());

		holder.tr_time.setText(mTimeRecordsList.get(position).getTotalHours()
				+ "hr");

		// holder.tr_amount.setText("$"
		// + mTimeRecordsList.get(position).getTotalAmount());

		ArrayList<Breaks> array = mTimeRecordsList.get(position)
				.getBreaksList();

		String breakTimes = "";
		
		int breaksArraySize = array.size();

		System.out.println(breaksArraySize);
		for (int i = 0; i < breaksArraySize; i++) {

			if (Global.getInstance().getBooleanType(mContext,
					CONSTANTS.ENABLE_TIME)) {
				if (i == 0) {
					breakTimes = array.get(i).startBreakTime + "-"
							+ array.get(i).endBreakTime;
				} else {
					breakTimes = breakTimes
							+ ","
							+ "\n"
							+ (array.get(i).startBreakTime + "-" + array.get(i).endBreakTime)
							+ "\n";
				}
			} else {
				// holder.tr_date.setText(inDate
				// + "  "
				// + Utils.Convert24to12(mTimeRecordsList.get(position)
				// .getIntime()));

				if (i == 0) {
					breakTimes = Utils
							.Convert24to12(array.get(i).startBreakTime)
							+ "-"
							+ Utils.Convert24to12(array.get(i).endBreakTime);
				} else {
					breakTimes = breakTimes + "," + "\n"
							+ Utils.Convert24to12(array.get(i).startBreakTime)
							+ "-"
							+ Utils.Convert24to12(array.get(i).endBreakTime);
				}
			}

		}

		if (breaksArraySize > 0) {
			holder.break_time.setText("Break(s): " + "\n" + breakTimes);
		} else {
			holder.break_time.setText("");
		}

		holder.tr_amount.setText("$"
				+ mTimeRecordsList.get(position).getTotalAmount());

		if (mTimeRecordsList.get(position).getIsApproved()
				.equalsIgnoreCase("1")) {
			holder.approve.setVisibility(View.GONE);
			holder.deny.setVisibility(View.GONE);
		}

		holder.approve.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mContext);

				// Setting Dialog Title
				alertDialog.setTitle("Time Sheet Approval");

				// Setting Dialog Message
				alertDialog
						.setMessage("Do you want to approve this time sheet?");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.
								// approve employee time sheet
								if (utils.isConnectingToInternet(mContext)) {
									new ApproveTimeRecordTask(mTimeRecordsList
											.get(position).getId(), mContext,
											position, mTimeRecordsList)
											.execute("");
								} else {
									utils.showDialog(mContext);
								}

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog
								dialog.cancel();

							}
						});

				// Showing Alert Message
				alertDialog.show();

			}
		});
		holder.deny.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mContext);

				// Setting Dialog Title
				alertDialog.setTitle("Time sheet denial");

				// Setting Dialog Message
				alertDialog
						.setMessage("Do you want to deny this time sheet?. It will also delete the time sheet.");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.
								// delete denied time sheet
								if (utils.isConnectingToInternet(mContext)) {
									new DeleteTimeRecordTask(
											getCheckedTimeRecords(),
											mTimeRecordsList.get(position)
													.getId(), mTimeRecordsList,
											mContext, true, position)
											.execute("");
								} else {
									utils.showDialog(mContext);
								}

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog
								dialog.cancel();

							}
						});

				// Showing Alert Message
				alertDialog.show();

			}
		});

		holder.view_gps.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext,
						GoogleMapViewActivity.class);

				intent.putExtra("startLat", mTimeRecordsList.get(position)
						.getStartlat());
				intent.putExtra("startLng", mTimeRecordsList.get(position)
						.getStartlng());
				intent.putExtra("endLat", mTimeRecordsList.get(position)
						.getEndlat());
				intent.putExtra("endLng", mTimeRecordsList.get(position)
						.getEndlng());

				intent.putExtra("clockOnTime", mTimeRecordsList.get(position)
						.getIntime());
				intent.putExtra("clockOffTime", mTimeRecordsList.get(position)
						.getOuttime());
				intent.putExtra("from", "time_records");
				mContext.startActivity(intent);

			}
		});
		return convertView;
	}

	class ViewHolder {
		TextView tr_user_name;
		TextView tr_client_name;
		TextView tr_date;
		TextView tr_amount;
		TextView view_gps;
		TextView tr_time;
		TextView break_time;
		TextView manual;
		CheckBox check_time_record;
		Button approve;
		Button deny;
	}

	public ArrayList<String> getArrayListOfTimeRecords() {

		Utils.removeDuplicates(checkedTimeRecords);
		return checkedTimeRecords;

	}

	public class DeleteTimeRecordTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();
		Context mContext;
		String timeRecord_Id;
		ArrayList<TimeRecords> checkedRecord;
		public ProgressDialog progressDialog;
		private ArrayList<TimeRecords> timeRecordsArray;
		boolean isDenied;
		int mPosition;

		public DeleteTimeRecordTask(ArrayList<TimeRecords> checkedRecordObject,
				String tr_id, ArrayList<TimeRecords> timeRecordsList,
				Context mContext, boolean isDenied, int mPosition) {
			timeRecord_Id = tr_id;
			checkedRecord = checkedRecordObject;
			this.mContext = mContext;
			progressDialog = new ProgressDialog(mContext);
			this.isDenied = isDenied;
			timeRecordsArray = timeRecordsList;
			db = new DataBaseHelper(mContext);
			this.mPosition = mPosition;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_TIME_RECORD));
				entity.addPart(RequestParameters.TR_ID, new StringBody(
						timeRecord_Id));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");

				if (success.equalsIgnoreCase("1")
						&& message.equalsIgnoreCase("Success")) {
					Toast.makeText(mContext, "Time Record has been deleted",
							Toast.LENGTH_LONG).show();

					// deleting single time record from database
					db.open();
					db.deleteTimeRecordForId(timeRecord_Id);
					db.close();

					// removing from array list and refreshing listview

					timeRecordsArray.remove(mPosition);

					notifyDataSetChanged();

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public ArrayList<TimeRecords> getCheckedTimeRecords() {

		Utils.removeDuplicates(checkedTRObject);
		return checkedTRObject;

	}

	public class ApproveTimeRecordTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String timeRecord_Id;
		ArrayList<TimeRecords> checkedRecord;
		public ProgressDialog progressDialog;
		int mPosition;
		private ArrayList<TimeRecords> timeRecordsArray;

		public ApproveTimeRecordTask(String tr_id, Context mContext,
				int mPosition, ArrayList<TimeRecords> timeRecordsList) {
			timeRecord_Id = tr_id;
			this.mPosition = mPosition;
			progressDialog = new ProgressDialog(mContext);
			timeRecordsArray = timeRecordsList;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.APROVE_TIME_RECORD));
				entity.addPart(RequestParameters.TIME_RECORD_ID,
						new StringBody(timeRecord_Id));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");

				if (success.equalsIgnoreCase("1")) {

					timeRecordsArray.remove(mPosition);
					notifyDataSetChanged();

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
