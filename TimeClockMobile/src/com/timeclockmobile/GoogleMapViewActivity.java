package com.timeclockmobile;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.timeclockmobile.data.LocationPoints;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;

public class GoogleMapViewActivity extends Activity {

	// Google Map
	private GoogleMap googleMap;

	private String lat0, lng0, lat1, lng1;

	public String clockOnTime, clockOffTime;

	private ImageButton backBtn;

	private String from;

	private ArrayList<LocationPoints> locations = new ArrayList<LocationPoints>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.google_map_view_activity);

		backBtn = (ImageButton) findViewById(R.id.backBtn);
		Intent intent = getIntent();

		from = (intent.getStringExtra("from"));

		if (from.equalsIgnoreCase("time_records")) {
			lat0 = (intent.getStringExtra("startLat"));
			lng0 = (intent.getStringExtra("startLng"));
			lat1 = (intent.getStringExtra("endLat"));
			lng1 = (intent.getStringExtra("endLng"));

			clockOnTime = (intent.getStringExtra("clockOnTime"));

			clockOffTime = (intent.getStringExtra("clockOffTime"));

			if (Global.getInstance().getBooleanType(GoogleMapViewActivity.this,
					CONSTANTS.ENABLE_TIME)) {

			} else {

				clockOnTime = Utils.Convert24to12(clockOnTime);

				clockOffTime = Utils.Convert24to12(clockOffTime);
			}

			LocationPoints locationPoint = new LocationPoints();
			locationPoint.setLatitude(lat0);
			locationPoint.setLongitude(lng0);
			locationPoint.setTime("Clock In " + clockOnTime);

			locations.add(locationPoint);

			LocationPoints locationPoint1 = new LocationPoints();
			locationPoint1.setLatitude(lat1);
			locationPoint1.setLongitude(lng1);
			locationPoint1.setTime("Clock Off " + clockOffTime);

			locations.add(locationPoint1);

		} else {
			lat0 = (intent.getStringExtra("startLat"));
			lng0 = (intent.getStringExtra("startLng"));

			clockOnTime = (intent.getStringExtra("clockOnTime"));

			if (Global.getInstance().getBooleanType(GoogleMapViewActivity.this,
					CONSTANTS.ENABLE_TIME)) {

			} else {

				clockOnTime = Utils.Convert24to12(clockOnTime);

			}

			LocationPoints locationPoint = new LocationPoints();
			locationPoint.setLatitude(lat0);
			locationPoint.setLongitude(lng0);
			locationPoint.setTime("Clock In " + clockOnTime);

			locations.add(locationPoint);

		}

		try {
			// Loading map
			initilizeMap();

			// Changing map type
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

			// Showing / hiding your current location
			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(true);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);

			// double latitude = 17.385044;
			// double longitude = 78.486671;

			// lets place some 10 random markers
			for (int i = 0; i < locations.size(); i++) {
				// random latitude and logitude

				// double[] randomLocation = createRandLocation(
				// Double.parseDouble(locations.get(i).getLatitude()),
				// Double.parseDouble(locations.get(i).getLongitude()));
				//
				// // Adding a marker
				// MarkerOptions marker = new MarkerOptions().position(
				// new LatLng(randomLocation[0], randomLocation[1]))
				// .title(locations.get(i).getTime());
				//
				// Log.e("Random", "> " + randomLocation[0] + ", "
				// + randomLocation[1]);

				// Drawing marker on the map
				drawMarker(
						new LatLng(Double.parseDouble(locations.get(i)
								.getLatitude()), Double.parseDouble(locations
								.get(i).getLongitude())), i, locations.get(i)
								.getTime());

				// changing marker color
				// if (i == 0)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				// if (i == 1)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
				// if (i == 2)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
				// if (i == 3)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				// if (i == 4)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
				// if (i == 5)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
				// if (i == 6)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_RED));
				// if (i == 7)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
				// if (i == 8)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
				// if (i == 9)
				// marker.icon(BitmapDescriptorFactory
				// .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

				// googleMap.addMarker(marker);

				// Move the camera to last position with a zoom level
				if (i == (locations.size() - 1)) {
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(new LatLng(Double.parseDouble(locations
									.get(i).getLatitude()), Double
									.parseDouble(locations.get(i)
											.getLongitude()))).zoom(15).build();

					googleMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
	}

	private void drawMarker(LatLng point, int position, String title) {
		// Creating an instance of MarkerOptions
		MarkerOptions markerOptions = new MarkerOptions();

		// Setting latitude and longitude for the marker
		markerOptions.position(point);

		markerOptions.title(title);

		markerOptions.icon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

		// Adding marker on the Google Map
		googleMap.addMarker(markerOptions);
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	/*
	 * creating random postion around a location for testing purpose only
	 */
	private double[] createRandLocation(double latitude, double longitude) {

		return new double[] { latitude + ((Math.random() - 0.5) / 500),
				longitude + ((Math.random() - 0.5) / 500),
				150 + ((Math.random() - 0.5) * 10) };
	}
}
