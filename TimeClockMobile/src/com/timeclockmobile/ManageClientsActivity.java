package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.timeclockmobile.adapter.ClientsAdapter;
import com.timeclockmobile.data.Clients;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class ManageClientsActivity extends Activity {

	private DataBaseHelper db = new DataBaseHelper(ManageClientsActivity.this);
	private ArrayList<Clients> clientsArray = new ArrayList<Clients>();

	private ListView clientsListView;
	private ImageButton back;
	private ClientsAdapter adapter;
	private ImageButton add_client;
	public ProgressDialog progressDialog;
	private Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.manage_clients);

		progressDialog = new ProgressDialog(ManageClientsActivity.this);

		clientsListView = (ListView) findViewById(R.id.clientsList);

		back = (ImageButton) findViewById(R.id.backBtn);

		clientsArray = getClientsFromDB(ManageClientsActivity.this);

		adapter = new ClientsAdapter(ManageClientsActivity.this,
				R.layout.manage_clients_inflate, clientsArray);

		clientsListView.setAdapter(adapter);

		add_client = (ImageButton) findViewById(R.id.add);

		add_client.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// ***** custom dialog for adding client ********//

				final Dialog d = new Dialog(ManageClientsActivity.this);

				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.setContentView(R.layout.add_client);
				d.show();
				d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				d.setCanceledOnTouchOutside(false);
				@SuppressWarnings("static-access")
				Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				@SuppressWarnings("deprecation")
				int width = display.getWidth();
				@SuppressWarnings("deprecation")
				int height = display.getHeight();

				Log.v("width", width + "");
				//d.getWindow().setLayout((width), (int) (height));

				final EditText client_name = (EditText) d
						.findViewById(R.id.enter_clientName);
				final EditText client_address = (EditText) d
						.findViewById(R.id.enter_clientAddress);
				final ImageButton backBtn = (ImageButton) d
						.findViewById(R.id.backBtn);

				backBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						d.dismiss();
					}
				});

				Button submit = (Button) d.findViewById(R.id.submit);

				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String clientName = client_name.getText().toString();
						String clientAddress = client_address.getText()
								.toString();

						if (!clientName.equalsIgnoreCase("")) {
							if (!clientAddress.equalsIgnoreCase("")) {

								// ******* adding client to the employer
								// ******//
								
								
								
								if (utils.isConnectingToInternet(ManageClientsActivity.this)) {
									new AddClientTask(clientName, clientAddress)
									.execute("");
								} else {
									utils.showDialog(ManageClientsActivity.this);
								}

							} else {
								Toast.makeText(getApplicationContext(),
										"Enter Client Address",
										Toast.LENGTH_LONG).show();
							}
						} else {
							Toast.makeText(getApplicationContext(),
									"Enter Client Name", Toast.LENGTH_LONG)
									.show();
						}

					}
				});

			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	public ArrayList<Clients> getClientsFromDB(Context mContext) {

		db = new DataBaseHelper(mContext);
		ArrayList<Clients> clientsArrayList1 = new ArrayList<Clients>();

		db.open();
		Cursor clientsDetails = db.getClients();

		if (clientsDetails.moveToFirst()) {
			do {

				String clientId = clientsDetails.getString(0);
				String employerId = clientsDetails.getString(1);
				String clientName = clientsDetails.getString(2);
				String clientAddress = clientsDetails.getString(3);

				Clients client = new Clients();
				client.setmClientId(clientId);
				client.setmEmployerId(employerId);
				client.setmClientName(clientName);
				client.setmClientAddress(clientAddress);

				clientsArrayList1.add(client);

			} while (clientsDetails.moveToNext());
		}
		clientsDetails.close();
		db.close();
		return clientsArrayList1;
	}

	private class AddClientTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String mClientName;
		String mClientAddress;

		public AddClientTask(String client_name, String client_address) {
			mClientName = client_name;
			mClientAddress = client_address;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.ADD_CLIENT));
				entity.addPart(
						RequestParameters.EMPLOYER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ManageClientsActivity.this,
								RequestParameters.EMPLOYER_ID)));
				entity.addPart(RequestParameters.CLIENT_NAME, new StringBody(
						mClientName));
				entity.addPart(RequestParameters.CLIENT_ADDRESS,
						new StringBody(mClientAddress));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject clientJson = jsonResponse.getJSONObject("response");
				String success = clientJson.getString("success");
				String message = clientJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Client Added"))) {
					JSONObject clientDetails = clientJson
							.getJSONObject("client");

					String client_Id = clientDetails.getString("id");
					String employer_Id = clientDetails.getString("employerid");
					String client_Name = clientDetails.getString("clientname");
					String client_Address = clientDetails.getString("address");
					String active = clientDetails.getString("active");

					db.open();
					db.insertClients(client_Id, employer_Id, client_Name,
							client_Address, active);
					db.close();

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(ManageClientsActivity.this,
							ManageClientsActivity.class); // your class
					startActivity(i);
					finish();

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
