package com.timeclockmobile;

import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class FieldNoteDetailsActivity extends Activity {

	private ImageButton backBtn;
	private TextView employee_name, client_name, date, field_note;
	private String mEmployeeName, mClientName, mDate, mFieldNote;
	private String inDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.field_note_detail);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		employee_name = (TextView) findViewById(R.id.employee_name);
		client_name = (TextView) findViewById(R.id.client_name);
		date = (TextView) findViewById(R.id.date);
		field_note = (TextView) findViewById(R.id.field_note);

		Intent intent = getIntent();

		mEmployeeName = intent.getStringExtra("employee_name");
		mClientName = intent.getStringExtra("client_name");
		mDate = intent.getStringExtra("date");
		mFieldNote = intent.getStringExtra("field_note");

		String onlyDate[] = mDate.split(" ");

		String dateArray[] = onlyDate[0].split("-");

		if (Global
				.getInstance()
				.getPreferenceVal(FieldNoteDetailsActivity.this,
						CONSTANTS.DATE_FORMAT).equalsIgnoreCase("DD/MM/YYYY")) {
			// inDate = new SimpleDateFormat("dd/MM/yyyy EEEE").format(new
			// Date());

			inDate = dateArray[2] + "/" + dateArray[1] + "/" + dateArray[0];
		} else {

			// inDate = new SimpleDateFormat("MM/dd/yyyy EEEE").format(new
			// Date());

			inDate = dateArray[1] + "/" + dateArray[2] + "/" + dateArray[0];
		}

		employee_name.setText(mEmployeeName);
		client_name.setText(mClientName);
		date.setText(inDate);
		field_note.setText(mFieldNote);

		field_note.setMinLines(3);
		field_note.setMaxLines(8);
		field_note.setVerticalScrollBarEnabled(true);
		field_note.setMovementMethod(new ScrollingMovementMethod());

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}

}
