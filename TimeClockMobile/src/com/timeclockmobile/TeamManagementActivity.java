package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import com.timeclockmobile.adapter.TeamManagementAdapter;
import com.timeclockmobile.data.ClockedOnUser;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class TeamManagementActivity extends Activity {

	private ListView employeesListView;
	private ImageButton backBtn;
	private ArrayList<Employees> employeesArrayList = new ArrayList<Employees>();

	private TeamManagementAdapter adapter;
	private ManageEmployeesActivity manageEmployeesActivity = new ManageEmployeesActivity();

	public Button massive_clock_on, massive_clock_off, massive_switch_location;
	private ArrayList<Employees> checkedEmployeesArray;
	private DataBaseHelper db = new DataBaseHelper(TeamManagementActivity.this);

	private ArrayList<String> clockedOnEmployees;

	private ArrayList<ClockedOnUser> clockedOnUsersList;

	private EditText search_employee;
	private Utils utils = new Utils();
	public ProgressDialog progressDialog;

	private boolean isClockOn;
	private boolean isClockoff;
	private boolean isStartBreak;
	private boolean isFinishedBreak;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.team_management);
		progressDialog = new ProgressDialog(TeamManagementActivity.this);

		massive_clock_on = (Button) findViewById(R.id.massive_clock_on);
		massive_clock_off = (Button) findViewById(R.id.massive_clock_off);
		massive_switch_location = (Button) findViewById(R.id.massive_switch_location);
		search_employee = (EditText) findViewById(R.id.search_employee);

		employeesListView = (ListView) findViewById(R.id.employeesList);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		// get Employee details and displaying in listview.

		employeesArrayList = manageEmployeesActivity
				.getEmployeesDetailsFromDB(TeamManagementActivity.this);

		adapter = new TeamManagementAdapter(TeamManagementActivity.this,
				R.layout.team_management_inflate, employeesArrayList);

		// setting adapter to employees list

		employeesListView.setAdapter(adapter);

		// search employees using name and filter it.
		search_employee.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				String text = search_employee.getText().toString()
						.toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		/**********
		 * massive clock on will be able to select "not clocked on" multiple
		 * employees to be selected and assigning the job by the employer..
		 *************/
		massive_clock_on.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String name = null;

				if (adapter != null) {

					checkedEmployeesArray = null;
					checkedEmployeesArray = adapter.getCheckedEmployees();
					if (checkedEmployeesArray.size() > 0) {
						ArrayList<String> employeeNames = new ArrayList<String>();
						String[] ids = new String[checkedEmployeesArray.size()];
						for (int i = 0; i < checkedEmployeesArray.size(); i++) {
							ids[i] = checkedEmployeesArray.get(i).getId();
						}

						String[] names = new String[checkedEmployeesArray
								.size()];
						for (int i = 0; i < checkedEmployeesArray.size(); i++) {
							names[i] = checkedEmployeesArray.get(i).getName();
						}
						clockedOnEmployees = null;

						clockedOnEmployees = getArrayClockOnDetails();

						for (int i = 0; i < ids.length; i++) {

							if (clockedOnEmployees.contains(ids[i])) {
								String idValue = ids[i];
								String namess = names[i];
								employeeNames.add(namess);
							}
						}

						if (employeeNames.size() > 0) {

							for (int i = 0; i < employeeNames.size(); i++) {

								if (i == 0) {
									name = employeeNames.get(i);
								} else {
									name = name + "," + employeeNames.get(i);
								}

							}

							Toast.makeText(getApplicationContext(),
									name + " are already clocked on",
									Toast.LENGTH_SHORT).show();

						} else {

							Intent intent = new Intent(
									TeamManagementActivity.this,
									SelectClientsActivity.class);
							intent.putExtra("clock_type", "massive_clock_on");
							intent.putExtra("checkedEmployeesArray",
									checkedEmployeesArray);

							startActivity(intent);
							finish();
						}

					} else {
						Toast.makeText(getApplicationContext(),
								"Select atleast one employee to clock on",
								Toast.LENGTH_SHORT).show();
					}
				}

			}
		});

		/********* clock off the selected clocked on employees **************/

		massive_clock_off.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String name = null;

				if (adapter != null) {
					checkedEmployeesArray = null;
					checkedEmployeesArray = adapter.getCheckedEmployees();
					if (checkedEmployeesArray.size() > 0) {
						ArrayList<String> employeeNames = new ArrayList<String>();
						String[] ids = new String[checkedEmployeesArray.size()];
						for (int i = 0; i < checkedEmployeesArray.size(); i++) {
							ids[i] = checkedEmployeesArray.get(i).getId();
						}

						String[] names = new String[checkedEmployeesArray
								.size()];
						for (int i = 0; i < checkedEmployeesArray.size(); i++) {
							names[i] = checkedEmployeesArray.get(i).getName();
						}
						clockedOnEmployees = null;
						clockedOnEmployees = getArrayClockOnDetails();

						for (int i = 0; i < ids.length; i++) {

							if (!clockedOnEmployees.contains(ids[i])) {
								String idValue = ids[i];
								String namess = names[i];
								employeeNames.add(namess);
							}
						}

						if (employeeNames.size() > 0) {

							for (int i = 0; i < employeeNames.size(); i++) {

								if (i == 0) {
									name = employeeNames.get(i);
								} else {
									name = name + "," + employeeNames.get(i);
								}

							}

							Toast.makeText(getApplicationContext(),
									name + " is not yet clocked on",
									Toast.LENGTH_SHORT).show();

						} else {
							
							if (isStartBreak) {
								Toast.makeText(getApplicationContext(),
										"Please finish the break before Clock off",
										Toast.LENGTH_SHORT).show();
							}else{
								Intent intent = new Intent(
										TeamManagementActivity.this,
										MassiveClockOffActivity.class);

								intent.putExtra("ClockedOnUsersList",
										checkedEmployeesArray);

								startActivity(intent);
								finish();
							}

						
						}

					} else {
						Toast.makeText(getApplicationContext(),
								"Select atleast one employee to clock off",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		massive_switch_location.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String name = null;

				if (adapter != null) {
					checkedEmployeesArray = null;
					checkedEmployeesArray = adapter.getCheckedEmployees();
					if (checkedEmployeesArray.size() > 0) {
						ArrayList<String> employeeNames = new ArrayList<String>();
						String[] ids = new String[checkedEmployeesArray.size()];
						for (int i = 0; i < checkedEmployeesArray.size(); i++) {
							ids[i] = checkedEmployeesArray.get(i).getId();
						}

						String[] names = new String[checkedEmployeesArray
								.size()];
						for (int i = 0; i < checkedEmployeesArray.size(); i++) {
							names[i] = checkedEmployeesArray.get(i).getName();
						}

						clockedOnEmployees = null;
						clockedOnEmployees = getArrayClockOnDetails();

						for (int i = 0; i < ids.length; i++) {

							if (!clockedOnEmployees.contains(ids[i])) {
								String idValue = ids[i];
								String namess = names[i];
								employeeNames.add(namess);
							}
						}

						if (employeeNames.size() > 0) {

							for (int i = 0; i < employeeNames.size(); i++) {

								if (i == 0) {
									name = employeeNames.get(i);
								} else {
									name = name + "," + employeeNames.get(i);
								}

							}

							Toast.makeText(getApplicationContext(),
									name + " is not yet clocked on",
									Toast.LENGTH_SHORT).show();

						} else {
							// Intent intent = new Intent(
							// TeamManagementActivity.this,
							// SelectClientsActivity.class);
							// intent.putExtra("clock_type",
							// "massive_switch_location");
							// intent.putExtra("ClockedOnUsersList",
							// checkedEmployeesArray);
							// startActivity(intent);
							// finish();

							if (isStartBreak) {
								finishBreakDialog();
							} else if (isFinishedBreak) {
								startBreakDialog();
							} else {

							}

						}

					} else {
						Toast.makeText(
								getApplicationContext(),
								"Select atleast one employee to switch location",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});

	}

	public void startBreakDialog() {
		// Toast.makeText(getApplicationContext(), "Start Break",
		// Toast.LENGTH_SHORT).show();

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				TeamManagementActivity.this);

		// Setting Dialog Title
		alertDialog.setTitle("Start Break");

		// Setting Dialog Message
		alertDialog.setMessage("Please confirm that you are taking a break");

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// User pressed YES button.
						// delete employee

						dialog.cancel();

					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// User pressed No button.Cancel the
						// dialog
						if (utils
								.isConnectingToInternet(TeamManagementActivity.this)) {
							new StartBreakTask(checkedEmployeesArray)
									.execute("");
						} else {
							utils.showDialog(TeamManagementActivity.this);
						}
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	public void finishBreakDialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				TeamManagementActivity.this);

		// Setting Dialog Title
		alertDialog.setTitle("End Break");

		// Setting Dialog Message
		alertDialog.setMessage("Please confirm that you are ending a break");

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// User pressed YES button.
						// delete employee

						dialog.cancel();

					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// User pressed No button.Cancel the
						// dialog
						if (utils
								.isConnectingToInternet(TeamManagementActivity.this)) {
							new EndBreakTask(checkedEmployeesArray).execute("");
						} else {
							utils.showDialog(TeamManagementActivity.this);
						}
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent = new Intent(TeamManagementActivity.this,
				ManageActivty.class);
		startActivity(intent);
		finish();

	}

	public void changeViews(Context mContext, ArrayList<Employees> employees) {
		// Toast.makeText(mContext, "change views", Toast.LENGTH_LONG).show();

		isClockOn = false;
		isClockoff = true;
		isStartBreak = false;
		isFinishedBreak = false;

		getClockOnInform(employees);
		getClockOffInform(employees);
		getStartedBreak(employees);
		getFinishedBreak(employees);

		System.out.println(isClockOn);

		if (!isClockOn) {
			massive_clock_on.setBackgroundColor(Color.parseColor("#AAAAAA"));
			massive_clock_off.setBackgroundColor(Color.parseColor("#AAAAAA"));
			massive_switch_location.setBackgroundColor(Color
					.parseColor("#AAAAAA"));
		} else {
			massive_clock_on.setBackgroundColor(Color.parseColor("#AAAAAA"));
			massive_clock_off.setBackgroundColor(Color.parseColor("#56A89A"));
			massive_switch_location.setBackgroundColor(Color
					.parseColor("#AAAAAA"));
			if (isStartBreak) {
				massive_switch_location.setBackgroundColor(Color
						.parseColor("#56A89A"));
				massive_switch_location.setText("Finish Break");
				massive_clock_off.setBackgroundColor(Color.parseColor("#AAAAAA"));
			} else if (isFinishedBreak) {
				massive_switch_location.setBackgroundColor(Color
						.parseColor("#56A89A"));
				massive_switch_location.setText("Start Break");
			}
		}

		if (!isClockoff) {
			massive_clock_on.setBackgroundColor(Color.parseColor("#56A89A"));
			massive_clock_off.setBackgroundColor(Color.parseColor("#AAAAAA"));
			massive_switch_location.setBackgroundColor(Color
					.parseColor("#AAAAAA"));
		}

	}

	public void getClockOnInform(ArrayList<Employees> employees) {
		for (int i = 0; i < employees.size(); i++) {
			String isWorking = employees.get(i).getIsWorking();
			if (isWorking.equalsIgnoreCase("1")) {
				isClockOn = true;
			} else {
				isClockOn = false;
				return;
			}
		}
	}

	public void getClockOffInform(ArrayList<Employees> employees) {
		for (int i = 0; i < employees.size(); i++) {
			String isWorking = employees.get(i).getIsWorking();
			if (isWorking.equalsIgnoreCase("1")) {
				isClockoff = true;
				return;
			} else {
				isClockoff = false;

			}
		}
	}

	public void getStartedBreak(ArrayList<Employees> employees) {
		for (int i = 0; i < employees.size(); i++) {
			String isBreakStarted = employees.get(i).getIsBreakStarted();
			if (isBreakStarted.equalsIgnoreCase("1")) {
				isStartBreak = true;

			} else {
				isStartBreak = false;
				return;
			}
		}
	}

	public void getFinishedBreak(ArrayList<Employees> employees) {
		for (int i = 0; i < employees.size(); i++) {
			String isBreakStarted = employees.get(i).getIsBreakStarted();
			if (isBreakStarted.equalsIgnoreCase("1")) {
				isFinishedBreak = false;
				return;
			} else {
				isFinishedBreak = true;

			}
		}
	}

	/************** get clocked on employees from database to check for clock off ***********************/

	public ArrayList<String> getArrayClockOnDetails() {

		// clockedOnUsersList = new ArrayList<ClockedOnUser>();

		db.open();
		Cursor clientsDetails = db.getWorkingEmployeesIds();

		ArrayList<String> employeeIds = new ArrayList<String>();

		if (clientsDetails.moveToFirst()) {
			do {

				String user_id = clientsDetails.getString(0);
				// String user_name = clientsDetails.getString(1);
				// String clientName = clientsDetails.getString(2);
				// String jobName = clientsDetails.getString(3);
				// String timeRecordId = clientsDetails.getString(4);

				// ClockedOnUser clocked_on_user = new ClockedOnUser();
				// clocked_on_user.setUser_id(user_id);
				// clocked_on_user.setName(user_name);
				// clocked_on_user.setClientName(clientName);
				// clocked_on_user.setJobName(jobName);
				// clocked_on_user.setTimeRecordId(timeRecordId);

				// clockedOnUsersList.add(clocked_on_user);

				employeeIds.add(user_id);

			} while (clientsDetails.moveToNext());
		}
		clientsDetails.close();
		db.close();
		return employeeIds;

	}

	private class StartBreakTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();
		ArrayList<Employees> workingEmployees;
		String numberOfEmployees;

		public StartBreakTask(ArrayList<Employees> workingEmployees) {
			this.workingEmployees = workingEmployees;
			numberOfEmployees = String.valueOf(workingEmployees.size());
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.START_BREAK));
				entity.addPart(RequestParameters.NUMBER_OF_TIME_RECORD,
						new StringBody(numberOfEmployees));
				for (int i = 0; i < workingEmployees.size(); i++) {
					entity.addPart(RequestParameters.TIME_RECORD_ID + (i + 1),
							new StringBody(workingEmployees.get(i)
									.getWorking_tr_id()));

				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			JSONObject clockOnObject;
			try {
				clockOnObject = jsonResponse.getJSONObject("response");
				String success = clockOnObject.getString("success");
				if (success.equalsIgnoreCase("1")) {

					JSONArray timeRecordsArray = clockOnObject
							.getJSONArray("timerecords");

					String outtime;
					String outdate;

					for (int i = 0; i < timeRecordsArray.length(); i++) {
						JSONObject timeRecord = timeRecordsArray
								.getJSONObject(i);

						String jobtypeid = timeRecord.getString("jobtypeid");
						String jobname = timeRecord.getString("jobname");
						String userid = timeRecord.getString("userid");

						String password = timeRecord.getString("password");
						String clientname = timeRecord.getString("clientname");
						String online = timeRecord.getString("online");

						String clientid = timeRecord.getString("clientid");

						String id = timeRecord.getString("id");

						String createdGMT = timeRecord.getString("created");
						String created = utils
								.convertGMTtoLocalTime(createdGMT);

						String name = timeRecord.getString("name");

						String workingEmployeeInTimeGMT = timeRecord
								.getString("intime");
						String workingEmployeeInDateGMT = timeRecord
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String intime = IntimeAndDate[1];
						String indate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = timeRecord
								.getString("outtime");
						String workingEmployeeOutDateGMT = timeRecord
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							outtime = outTimeAndDate[1];

							outdate = outTimeAndDate[0];
						} else {
							outtime = workingEmployeeOutTimeGMT;

							outdate = workingEmployeeOutDateGMT;
						}

						String phoneno = timeRecord.getString("phoneno");
						String endlng = timeRecord.getString("endlng");

						String userhourlyrate = timeRecord
								.getString("userhourlyrate");
						String endlat = timeRecord.getString("endlat");
						String status = timeRecord.getString("status");
						String startlat = timeRecord.getString("startlat");
						String devicetoken = timeRecord
								.getString("devicetoken");
						String startlng = timeRecord.getString("startlng");
						String address = timeRecord.getString("address");
						String email = timeRecord.getString("email");
						String active = timeRecord.getString("active");
						String clientaddress = timeRecord
								.getString("clientaddress");

						db.open();
						db.updateEmployeeDetailsForWorking(userid, "1", id,
								clientid, jobtypeid, intime, indate, startlat,
								startlng, clientname, created, endlat, endlng,
								jobname, outtime, outdate, "1");
						db.close();

					}

					Intent intent = new Intent(TeamManagementActivity.this,
							TeamManagementActivity.class);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class EndBreakTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();
		ArrayList<Employees> workingEmployees;
		String numberOfEmployees;

		public EndBreakTask(ArrayList<Employees> workingEmployees) {
			this.workingEmployees = workingEmployees;
			numberOfEmployees = String.valueOf(workingEmployees.size());
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.END_BREAK));
				entity.addPart(RequestParameters.NUMBER_OF_TIME_RECORD,
						new StringBody(numberOfEmployees));
				for (int i = 0; i < workingEmployees.size(); i++) {
					entity.addPart(RequestParameters.TIME_RECORD_ID + (i + 1),
							new StringBody(workingEmployees.get(i)
									.getWorking_tr_id()));

				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			JSONObject clockOnObject;
			try {
				clockOnObject = jsonResponse.getJSONObject("response");
				String success = clockOnObject.getString("success");
				if (success.equalsIgnoreCase("1")) {

					JSONArray timeRecordsArray = clockOnObject
							.getJSONArray("timerecords");

					String outtime;
					String outdate;

					for (int i = 0; i < timeRecordsArray.length(); i++) {
						JSONObject timeRecord = timeRecordsArray
								.getJSONObject(i);

						String jobtypeid = timeRecord.getString("jobtypeid");
						String jobname = timeRecord.getString("jobname");
						String userid = timeRecord.getString("userid");

						String password = timeRecord.getString("password");
						String clientname = timeRecord.getString("clientname");
						String online = timeRecord.getString("online");

						String clientid = timeRecord.getString("clientid");

						String id = timeRecord.getString("id");

						String createdGMT = timeRecord.getString("created");
						String created = utils
								.convertGMTtoLocalTime(createdGMT);

						String name = timeRecord.getString("name");

						String workingEmployeeInTimeGMT = timeRecord
								.getString("intime");
						String workingEmployeeInDateGMT = timeRecord
								.getString("indate");

						String IntimeAndDateInLocal = utils
								.convertGMTtoLocalTime(workingEmployeeInDateGMT
										+ " " + workingEmployeeInTimeGMT);

						String IntimeAndDate[] = IntimeAndDateInLocal
								.split(" ");

						String intime = IntimeAndDate[1];
						String indate = IntimeAndDate[0];

						//

						String workingEmployeeOutTimeGMT = timeRecord
								.getString("outtime");
						String workingEmployeeOutDateGMT = timeRecord
								.getString("outdate");

						if (!workingEmployeeOutTimeGMT.equals("null")) {
							String OuttimeAndDateInLocal = utils
									.convertGMTtoLocalTime(workingEmployeeOutDateGMT
											+ " " + workingEmployeeOutTimeGMT);

							String outTimeAndDate[] = OuttimeAndDateInLocal
									.split(" ");

							outtime = outTimeAndDate[1];

							outdate = outTimeAndDate[0];
						} else {
							outtime = workingEmployeeOutTimeGMT;

							outdate = workingEmployeeOutDateGMT;
						}

						String phoneno = timeRecord.getString("phoneno");
						String endlng = timeRecord.getString("endlng");

						String userhourlyrate = timeRecord
								.getString("userhourlyrate");
						String endlat = timeRecord.getString("endlat");
						String status = timeRecord.getString("status");
						String startlat = timeRecord.getString("startlat");
						String devicetoken = timeRecord
								.getString("devicetoken");
						String startlng = timeRecord.getString("startlng");
						String address = timeRecord.getString("address");
						String email = timeRecord.getString("email");
						String active = timeRecord.getString("active");
						String clientaddress = timeRecord
								.getString("clientaddress");

						db.open();
						db.updateEmployeeDetailsForWorking(userid, "1", id,
								clientid, jobtypeid, intime, indate, startlat,
								startlng, clientname, created, endlat, endlng,
								jobname, outtime, outdate, "0");
						db.close();

					}

					Intent intent = new Intent(TeamManagementActivity.this,
							TeamManagementActivity.class);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
