package com.timeclockmobile.adapter;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.timeclockmobile.ManageClientsActivity;
import com.timeclockmobile.R;
import com.timeclockmobile.data.Clients;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class ClientsAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<Clients> mClientsList;
	LayoutInflater inflater;
	public ProgressDialog progressDialog;
	private String clientId;
	private DataBaseHelper db;
	private Utils utils = new Utils();

	public ClientsAdapter(Context mContext, int layoutResourceId,
			ArrayList<Clients> clientsArrayList) {
		this.mContext = mContext;
		this.mClientsList = clientsArrayList;
		this.layoutResourceId = layoutResourceId;
		progressDialog = new ProgressDialog(mContext);
		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new DataBaseHelper(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mClientsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mClientsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.clientName = (TextView) convertView
					.findViewById(R.id.client_name);

			holder.clientAddress = (TextView) convertView
					.findViewById(R.id.client_address);

			holder.editBtnClient = (ImageButton) convertView
					.findViewById(R.id.edit_client);

			holder.deleteBtnClient = (ImageButton) convertView
					.findViewById(R.id.delete_client);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.clientName.setText(mClientsList.get(position).getmClientName());
		holder.clientAddress.setText(mClientsList.get(position)
				.getmClientAddress());

		holder.editBtnClient.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// ***** custom dialog for editing client ********//

				final Dialog d = new Dialog(mContext);

				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.setContentView(R.layout.add_client);
				d.show();
				d.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				d.setCanceledOnTouchOutside(false);
				@SuppressWarnings("static-access")
				Display display = ((WindowManager) mContext
						.getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				@SuppressWarnings("deprecation")
				int width = display.getWidth();
				@SuppressWarnings("deprecation")
				int height = display.getHeight();

				Log.v("width", width + "");
				// d.getWindow().setLayout((width), (int) (height));

				final EditText client_name = (EditText) d
						.findViewById(R.id.enter_clientName);
				final EditText client_address = (EditText) d
						.findViewById(R.id.enter_clientAddress);
				final TextView title_text = (TextView) d
						.findViewById(R.id.title_text);
				final ImageButton backBtn = (ImageButton) d
						.findViewById(R.id.backBtn);

				title_text.setText("Edit Client");

				client_name
						.setText(mClientsList.get(position).getmClientName());
				client_address.setText(mClientsList.get(position)
						.getmClientAddress());

				backBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						d.dismiss();
					}
				});

				Button submit = (Button) d.findViewById(R.id.submit);

				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String clientName = client_name.getText().toString();
						String clientAddress = client_address.getText()
								.toString();

						if (!clientName.equalsIgnoreCase("")) {
							if (!clientAddress.equalsIgnoreCase("")) {

								// ******* editing client to the employer
								// ******//

								if (utils.isConnectingToInternet(mContext)) {
									new UpdateClientTask(mClientsList.get(
											position).getmClientId(),
											clientName, clientAddress)
											.execute("");
								} else {
									utils.showDialog(mContext);
								}

							} else {
								Toast.makeText(mContext,
										"Enter Client Address",
										Toast.LENGTH_LONG).show();
							}
						} else {
							Toast.makeText(mContext, "Enter Client Name",
									Toast.LENGTH_LONG).show();
						}

					}
				});

			}
		});
		holder.deleteBtnClient.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				clientId = mClientsList.get(position).getmClientId();

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mContext);

				// Setting Dialog Title
				alertDialog.setTitle("Delete" + " "
						+ mClientsList.get(position).getmClientName());

				// Setting Dialog Message
				alertDialog.setMessage("Are you sure you want to delete" + " "
						+ mClientsList.get(position).getmClientName() + "?");

				// Setting Positive "Yes" Button
				alertDialog.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed YES button.

								if (utils.isConnectingToInternet(mContext)) {
									new DeleteClientTask(clientId, position)
											.execute("");
								} else {
									utils.showDialog(mContext);
								}

							}
						});

				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("NO",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// User pressed No button.Cancel the dialog
								dialog.cancel();
							}
						});

				// Showing Alert Message
				alertDialog.show();
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView clientName;
		TextView clientAddress;
		ImageButton editBtnClient;
		ImageButton deleteBtnClient;

	}

	private class DeleteClientTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String mClientId;
		int mPosition;

		public DeleteClientTask(String client_id, int position) {
			mClientId = client_id;
			mPosition = position;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_CLIENT));
				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject mResponseObject = jsonResponse
						.getJSONObject("response");

				String success = mResponseObject.getString("success");
				String message = mResponseObject.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Client Deleted"))) {

					// deleting client from database
					db.open();
					db.deleteClientForId(mClientId);
					db.close();

					mClientsList.remove(mPosition);
					notifyDataSetChanged();

					Toast.makeText(mContext, "Client has been deleted",
							Toast.LENGTH_LONG).show();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private class UpdateClientTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String mClientName;
		String mClientAddress;
		String mClientId;

		public UpdateClientTask(String client_id, String client_name,
				String client_address) {
			mClientId = client_id;
			mClientName = client_name;
			mClientAddress = client_address;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.UPDATE_CLIENT));
				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));
				entity.addPart(RequestParameters.CLIENT_NAME, new StringBody(
						mClientName));
				entity.addPart(RequestParameters.CLIENT_ADDRESS,
						new StringBody(mClientAddress));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject clientJson = jsonResponse.getJSONObject("response");
				String success = clientJson.getString("success");
				String message = clientJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Client Updated"))) {
					JSONObject clientDetails = clientJson
							.getJSONObject("client");
					String client_Id = clientDetails.getString("id");
					// String employer_Id =
					// clientDetails.getString("employerid");
					String client_Name = clientDetails.getString("clientname");
					String client_Address = clientDetails.getString("address");
					// String active = clientDetails.getString("active");

					db.open();
					db.updateClientDetails(client_Id, client_Name,
							client_Address);
					db.close();

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(mContext, ManageClientsActivity.class); // your
																					// class
					mContext.startActivity(i);

					((Activity) mContext).finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
