package com.timeclockmobile.utils;

public class CONSTANTS {

	public static final String ENABLE_NOTIFICATION = "enable_notification";

	public static final String ENABLE_TIME = "enable_time";

	public static final String EMAIL_ID = "email_id";

	public static final String HOURLY_RATE = "hourly_rate";

	public static final String DATE_FORMAT = "date_format";

	public static final String ROUNDING_VALUE = "rounding_value";
	
	public static final String INCLUDE_BREAKS = "include_breaks";

	public static final String CLOCKED_ON = "clock_on";

	public static final String USER_NAME = "user_name";

	public static final String CLIENT_NAME = "client_name";

	public static final String JOB_NAME = "job_name";

	public static final String USER_IS_WORKING = "user_is_working";

	public static final String USER_IN_TIME = "userInTime";

	public static final String USER_IN_DATE = "userInDate";

	public static final String CLOCKED_ON_TIME = "clocked_on_time";

	public static final String CLOCKED_ON_JOBTYPEID = "clocked_on_jobTypeId";

	public static final String CLOCKED_ON_CLIENTID = "clocked_on_clientId";

	public static final String CLOCKED_ON_JOBNAME = "clocked_on_jobName";

	public static final String CLOCKED_ON_CLIENTNAME = "clocked_on_clientName";

	public static final String CLOCKED_ON_TRID = "clocked_on_trId";

	public static final String START_BREAK_VALUE = "start_break";

	public static final String END_BREAK_VALUE = "end_break";

}
