package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.timeclockmobile.service.RepeatService;
import com.timeclockmobile.service.TimerService;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.GpsTracker;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class ClockOnActivity extends Activity {

	private TextView employeeName, clientName, jobTypeName;
	private Button submit_clockOn;
	public ProgressDialog progressDialog;
	private ImageButton homeBtn;

	private String mClientId, mJobTypeId;
	// private long startTime = 0L;
	// long timeInMilliseconds = 0L;
	//
	// long timeSwapBuff = 0L;
	//
	// long updatedTime = 0L;
	//
	// Handler customHandler = new Handler();
	// public static String timer;

	private static String TAG = "Timer";
	public static Timer mTimer = null;
	public static TimerTask mTimerTask = null;

	public static Handler mHandler = null;

	public static int count = 0;
	// private boolean isPause = false;
	// private boolean isStop = true;

	public static int delay = 1000; // 1s
	public static int period = 1000; // 1s

	public static final int UPDATE_TEXTVIEW = 0;

	Utils utils = new Utils();

	GpsTracker gpsTracker;
	String latitude;
	String longitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.clock_on);
		progressDialog = new ProgressDialog(ClockOnActivity.this);
		employeeName = (TextView) findViewById(R.id.employee_name);
		clientName = (TextView) findViewById(R.id.client_name);
		jobTypeName = (TextView) findViewById(R.id.job_type_name);
		submit_clockOn = (Button) findViewById(R.id.submit_clock_on);
		homeBtn = (ImageButton) findViewById(R.id.homeBtn);

		gpsTracker = new GpsTracker(this);

		if (gpsTracker.canGetLocation()) {
			System.out.println("GPRS is enabled");
			latitude = String.valueOf(gpsTracker.latitude);
			longitude = String.valueOf(gpsTracker.longitude);

			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LAT, latitude);
			// Global.getInstance().storeIntoPreference(ClockOnActivity.this,
			// RequestParameters.START_LNG, longitude);
		} else {
			System.out.println("GPRS is dissabled");
			gpsTracker.showSettingsAlert(this);
		}

		Intent intent = getIntent();

		mClientId = intent.getStringExtra("client_id");
		mJobTypeId = intent.getStringExtra("job_type_id");

		employeeName.setText(Global.getInstance().getPreferenceVal(
				ClockOnActivity.this, CONSTANTS.USER_NAME));

		jobTypeName.setText(Global.getInstance().getPreferenceVal(
				ClockOnActivity.this, CONSTANTS.JOB_NAME));

		clientName.setText(Global.getInstance().getPreferenceVal(
				ClockOnActivity.this, CONSTANTS.CLIENT_NAME));

		submit_clockOn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (utils.isConnectingToInternet(ClockOnActivity.this)) {
					new ClockOnTask().execute("");
				} else {
					utils.showDialog(ClockOnActivity.this);
				}
			}
		});

		mHandler = new Handler() {

			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case UPDATE_TEXTVIEW:
					updateTextView();
					break;
				default:
					break;
				}
			}
		};

		homeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(ClockOnActivity.this,
						HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
			}
		});

	}

	private class ClockOnTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.CLOCK_ON));
				entity.addPart(
						RequestParameters.USER_ID,
						new StringBody(Global.getInstance()
								.getPreferenceVal(ClockOnActivity.this,
										RequestParameters.USER_ID)));
				entity.addPart(RequestParameters.CLIENT_ID, new StringBody(
						mClientId));
				entity.addPart(RequestParameters.JOBTYPE_ID, new StringBody(
						mJobTypeId));
				entity.addPart(RequestParameters.START_LAT, new StringBody(
						latitude));
				entity.addPart(RequestParameters.START_LNG, new StringBody(
						longitude));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject clockOnObject = jsonResponse
						.getJSONObject("response");
				String success = clockOnObject.getString("success");
				String message = clockOnObject.getString("message");
				if ((success.equalsIgnoreCase("1"))
						&& (success.equalsIgnoreCase("1"))) {
					JSONObject timeRecordObject = clockOnObject
							.getJSONObject("timerecord");

					String employee_name = timeRecordObject.getString("name");
					String client_name = timeRecordObject
							.getString("clientname");
					String job_name = timeRecordObject.getString("jobname");

					String time_record_id = timeRecordObject.getString("id");

					Global.getInstance().storeIntoPreference(
							ClockOnActivity.this,
							RequestParameters.TIME_RECORD_ID, time_record_id);
					Global.getInstance().storeIntoPreference(
							ClockOnActivity.this, CONSTANTS.USER_NAME,
							employee_name);
					Global.getInstance().storeIntoPreference(
							ClockOnActivity.this, CONSTANTS.CLIENT_NAME,
							client_name);
					Global.getInstance().storeIntoPreference(
							ClockOnActivity.this, CONSTANTS.JOB_NAME, job_name);

					Global.getInstance().storeBooleanType(ClockOnActivity.this,
							CONSTANTS.CLOCKED_ON, true);

					// startTimer();

					startService(new Intent(ClockOnActivity.this,
							TimerService.class));

					// getting user's location
					submitLocationService();

					Intent intent = new Intent(ClockOnActivity.this,
							HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(intent);
					finish();

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**********
	 * submitting user's location to the server for every 5 min in the backgroud
	 * using service
	 *************/

	public void submitLocationService() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 30);

		Intent intent = new Intent(this, RepeatService.class);

		PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		// for 30 mint 60*60*1000
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
				5 * 60 * 1000, pintent);
		

		startService(new Intent(this, RepeatService.class));
	}

	public void updateTextView() {

		HomeActivity.timer.setText(utils.splitToComponentTimes(count));
	}

	private void startTimer() {
		if (mTimer == null) {
			mTimer = new Timer();
		}

		if (mTimerTask == null) {
			mTimerTask = new TimerTask() {
				@Override
				public void run() {
					Log.i(TAG, "count: " + String.valueOf(count));
					sendMessage(UPDATE_TEXTVIEW);

					// do {
					// // try {
					// // Log.i(TAG, "sleep(1000)...");
					// // Thread.sleep(1000);
					// // } catch (InterruptedException e) {
					// // }
					// } while (isPause);

					count++;

				}
			};
		}

		if (mTimer != null && mTimerTask != null)
			mTimer.schedule(mTimerTask, delay, period);

	}

	public void stopTimer() {

		if (mTimer != null) {
			mTimer.cancel();
			mTimer = null;
		}

		if (mTimerTask != null) {
			mTimerTask.cancel();
			mTimerTask = null;
		}

		count = 0;

	}

	public void sendMessage(int id) {

		if (mHandler != null) {
			Message message = Message.obtain(mHandler, id);
			mHandler.sendMessage(message);
		}
	}

}
