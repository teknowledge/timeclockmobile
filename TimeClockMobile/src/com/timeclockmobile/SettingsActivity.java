package com.timeclockmobile;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.webservices.RequestParameters;

public class SettingsActivity extends Activity {

	private EditText edit_email, hourly_rate_edit;
	private CheckBox check_icon_notification, check_enable_time,check_enable_breaks;
	private Spinner rounding_time_spinner, date_format_spinner;
	private Button saveBtn;
	private TextView hourly_rate_text, rounding_time, date_format,
			enable_time_text,enable_breaks_text;

	private ImageButton backBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.settings_activity);

		edit_email = (EditText) findViewById(R.id.edit_email);
		hourly_rate_edit = (EditText) findViewById(R.id.hourly_rate_edit);

		check_icon_notification = (CheckBox) findViewById(R.id.check_icon_notification);
		check_enable_time = (CheckBox) findViewById(R.id.check_enable_time);
		check_enable_breaks = (CheckBox)findViewById(R.id.check_enable_breaks);

		rounding_time_spinner = (Spinner) findViewById(R.id.rounding_time_spinner);
		date_format_spinner = (Spinner) findViewById(R.id.date_format_spinner);

		hourly_rate_text = (TextView) findViewById(R.id.hourly_rate_text);
		rounding_time = (TextView) findViewById(R.id.rounding_time);
		date_format = (TextView) findViewById(R.id.date_format);
		enable_time_text = (TextView) findViewById(R.id.enable_time_text);
		enable_breaks_text = (TextView)findViewById(R.id.enable_breaks_text);

		saveBtn = (Button) findViewById(R.id.saveBtn);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		if (!Global.getInstance().getBooleanType(SettingsActivity.this,
				RequestParameters.IS_EMPLOYER)) {
			hourly_rate_edit.setVisibility(View.GONE);
			rounding_time_spinner.setVisibility(View.GONE);
			date_format_spinner.setVisibility(View.GONE);
			check_enable_time.setVisibility(View.GONE);
			hourly_rate_text.setVisibility(View.GONE);
			rounding_time.setVisibility(View.GONE);
			date_format.setVisibility(View.GONE);
			enable_time_text.setVisibility(View.GONE);
			check_enable_breaks.setVisibility(View.GONE);
			enable_breaks_text.setVisibility(View.GONE);
		}

		if (!Global.getInstance()
				.getPreferenceVal(SettingsActivity.this, CONSTANTS.EMAIL_ID)
				.equalsIgnoreCase("")) {
			edit_email.setText(Global.getInstance().getPreferenceVal(
					SettingsActivity.this, CONSTANTS.EMAIL_ID));
		}

		if (!Global.getInstance()
				.getPreferenceVal(SettingsActivity.this, CONSTANTS.HOURLY_RATE)
				.equalsIgnoreCase("")) {

			hourly_rate_edit.setText(Global.getInstance().getPreferenceVal(
					SettingsActivity.this, CONSTANTS.HOURLY_RATE));
		}

		// setting rounding values for the time to the spinner
		List<String> rounding_list = new ArrayList<String>();
		rounding_list.add("0");
		rounding_list.add("5");
		rounding_list.add("10");
		rounding_list.add("15");
		rounding_list.add("20");
		rounding_list.add("25");
		rounding_list.add("30");

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, rounding_list);

		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		rounding_time_spinner.setAdapter(dataAdapter);

		rounding_time_spinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						String rounding_time = (String) parent
								.getItemAtPosition(position);

						Global.getInstance().storeIntoPreference(
								SettingsActivity.this,
								CONSTANTS.ROUNDING_VALUE, rounding_time);

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		// setting date format to display the dates through out the app

		List<String> date_formats = new ArrayList<String>();
		date_formats.add("DD/MM/YYYY");
		date_formats.add("MM/DD/YYYY");

		ArrayAdapter<String> dateFormatsAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, date_formats);

		dateFormatsAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		date_format_spinner.setAdapter(dateFormatsAdapter);
		if (!Global.getInstance()
				.getPreferenceVal(SettingsActivity.this, "position")
				.equalsIgnoreCase("")) {
			date_format_spinner.setSelection(Integer.parseInt(Global
					.getInstance().getPreferenceVal(SettingsActivity.this,
							"position")));
		}

		date_format_spinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						String date_format = (String) parent
								.getItemAtPosition(position);

						Global.getInstance().storeIntoPreference(
								SettingsActivity.this, CONSTANTS.DATE_FORMAT,
								date_format);

						Global.getInstance().storeIntoPreference(
								SettingsActivity.this, "position",
								String.valueOf(position));
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		check_icon_notification.setChecked(Global.getInstance().getBooleanType(
				SettingsActivity.this, CONSTANTS.ENABLE_NOTIFICATION));

		check_icon_notification
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub

						Global.getInstance().storeBooleanType(
								SettingsActivity.this,
								CONSTANTS.ENABLE_NOTIFICATION, isChecked);
						if (isChecked) {
							showNotification(SettingsActivity.this);
						} else {
							cancelNotification(0, SettingsActivity.this);
						}

					}
				});

		check_enable_time.setChecked(Global.getInstance().getBooleanType(
				SettingsActivity.this, CONSTANTS.ENABLE_TIME));

		check_enable_time
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						Global.getInstance().storeBooleanType(
								SettingsActivity.this, CONSTANTS.ENABLE_TIME,
								isChecked);
					}
				});
		
		check_enable_breaks.setChecked(Global.getInstance().getBooleanType(
				SettingsActivity.this, CONSTANTS.INCLUDE_BREAKS));
		check_enable_breaks.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				Global.getInstance().storeBooleanType(
						SettingsActivity.this, CONSTANTS.INCLUDE_BREAKS,
						isChecked);

			}
		});

		saveBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!Global.getInstance().getBooleanType(SettingsActivity.this,
						RequestParameters.IS_EMPLOYER)) {
					String emailId = edit_email.getText().toString();

					if (!emailId.equalsIgnoreCase("")) {
						Global.getInstance().storeIntoPreference(
								SettingsActivity.this, CONSTANTS.EMAIL_ID,
								emailId);
						onBackPressed();
					} else {
						Toast.makeText(getApplicationContext(),
								"Enter email id", Toast.LENGTH_SHORT).show();
					}
				} else {
					String emailId = edit_email.getText().toString();

					String hourlyRate = hourly_rate_edit.getText().toString();

					if (!emailId.equalsIgnoreCase("")) {
						if (!hourlyRate.equalsIgnoreCase("")) {

							Global.getInstance().storeIntoPreference(
									SettingsActivity.this, CONSTANTS.EMAIL_ID,
									emailId);
							Global.getInstance().storeIntoPreference(
									SettingsActivity.this,
									CONSTANTS.HOURLY_RATE, hourlyRate);

							onBackPressed();
						} else {
							Toast.makeText(getApplicationContext(),
									"Enter hourly rate", Toast.LENGTH_SHORT)
									.show();
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Enter email id", Toast.LENGTH_SHORT).show();
					}
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent = new Intent(SettingsActivity.this, HomeActivity.class);
		startActivity(intent);
		finish();
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void showNotification(Context mContext) {

		// define sound URI, the sound to be played when there's a notification
		Uri soundUri = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		// // intent triggered, you can add other intent for other actions
		Intent intent = new Intent(mContext, HomeActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(mContext, 0, intent,
				(Intent.FLAG_ACTIVITY_CLEAR_TASK));

		// this is it, we'll build the notification!
		// in the addAction method, if you don't want any icon, just set the
		// first param to 0
		Notification mNotification = new Notification.Builder(mContext)

		.setContentTitle("Time Clock Mobile!")
				// .setContentText("Here's an awesome update for you!")
				.setSmallIcon(R.drawable.icon_notify).setContentIntent(pIntent)
				.setSound(soundUri).setOngoing(true)

				// .addAction(R.drawable.add, "View", pIntent)
				// .addAction(0, "Remind", pIntent)

				.build();

		NotificationManager notificationManager = (NotificationManager) mContext
				.getSystemService(NOTIFICATION_SERVICE);

		// If you want to hide the notification after it was selected, do the
		// code below
		// myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

		notificationManager.notify(0, mNotification);
	}

	public void cancelNotification(int notificationId, Context mContext) {

		if (Context.NOTIFICATION_SERVICE != null) {
			String ns = mContext.NOTIFICATION_SERVICE;
			NotificationManager nMgr = (NotificationManager) mContext
					.getSystemService(ns);
			nMgr.cancel(notificationId);
		}
	}

}
