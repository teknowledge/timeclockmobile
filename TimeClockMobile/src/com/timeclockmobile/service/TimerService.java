package com.timeclockmobile.service;

import com.timeclockmobile.HomeActivity;
import com.timeclockmobile.utils.CONSTANTS;
import com.timeclockmobile.utils.Global;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

public class TimerService extends Service {

	private long startTime = 0L;

	private Handler customHandler;// = new Handler();

	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		customHandler = new Handler();
		startTime = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 0);
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		customHandler.removeCallbacks(updateTimerThread);
		if (HomeActivity.timer != null) {
			HomeActivity.timer
					.setText(String.format("%02d", 0) + ":"
							+ String.format("%02d", 0) + ":"
							+ String.format("%02d", 0));
		}
		// String country = VPNAreaPreferences.getPreferences(
		// VPNAreaLocalService.this, "connected_country");
		// VPNAreaPreferences.savePreferences(VPNAreaLocalService.this,
		// "status",
		// "EXITING");
		// VPNAreaPreferences.savePreferences(VPNAreaLocalService.this,
		// "connected_country", country);
		super.onDestroy();
	}

	private Runnable updateTimerThread = new Runnable() {

		public void run() {

			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

			updatedTime = timeSwapBuff + timeInMilliseconds;

			if (Global.getInstance().getBooleanType(getApplicationContext(),
					CONSTANTS.USER_IS_WORKING)) {
				updatedTime = updatedTime
						+ Long.parseLong(Global.getInstance().getPreferenceVal(
								getApplicationContext(),
								CONSTANTS.CLOCKED_ON_TIME));
			}

			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;

			int hours = mins / 60;

			mins = mins % 60;
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			if (HomeActivity.timer != null) {
				HomeActivity.timer.setText(String.format("%02d", hours) + ":"
						+ String.format("%02d", mins) + ":"
						+ String.format("%02d", secs));
			}
			customHandler.postDelayed(this, 0);
		}

	};
}
