package com.timeclockmobile.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper {

	// table for clients

	public static final String _ID = "_id";
	public static final String CLIENT_ID = "client_id";

	public static final String EMPLOYER_ID = "employer_id";
	public static final String CLIENT_NAME = "client_name";
	public static final String CLIENT_ADDRESS = "client_address";
	public static final String CLIENT_ACTIVE = "active";

	public static final String CLIENTS_TABLE = "clients";

	// ****** table for job types ********//
	public static final String J_ID = "j_id";
	public static final String JOB_TYPE_ID = "job_type_id";
	public static final String J_EMPLOYER_ID = "j_employer_id";
	public static final String JOB_NAME = "job_name";
	public static final String JOB_ACTIVE = "job_active";

	public static final String JOB_TYPES_TABLE = "job_types";

	// ******* table for employees********//
	public static final String E_ID = "e_id";
	public static final String EMPLOYEE_ID = "employee_id";
	public static final String EMPLOYEE_NAME = "employee_name";
	public static final String EMPLOYEE_MAIL = "employee_email";
	public static final String EMPLOYEE_ADDRESS = "employee_address";
	public static final String EMPLOYEE_PASSWORD = "employee_password";
	public static final String EMPLOYEE_PHONENO = "employee_phoneno";
	public static final String EMPLOYEE_PROFILE_IMAGE = "employee_profile_image";
	public static final String EMPLOYEE_HOURLYRATE = "employee_hourlyrate";
	public static final String EMPLOYEE_DEVICETOKEN = "employee_devicetoken";
	public static final String EMPLOYEE_ISEMPLOYER = "employee_isemployer";
	public static final String EMPLOYEE_EMPLOYER_ID = "employee_employer_id";
	public static final String EMPLOYEE_ONLINE = "employee_online";
	public static final String EMPLOYEE_STATUS = "employee_status";
	public static final String EMPLOYEE_CREATED = "employee_created";
	public static final String EMPLOYEE_LASTLOGIN = "employee_lastlogin";
	public static final String EMPLOYEE_IS_WORKING = "employee_is_working";
	public static final String EMPLOYEE_WORKING_TR_ID = "employee_working_tr_id";
	public static final String EMPLOYEE_WORKING_CLIENT_ID = "employee_working_client_id";
	public static final String EMPLOYEE_WORKING_JOBTYPE_ID = "employee_working_jobtype_id";
	public static final String EMPLOYEE_WORKING_INTIME = "employee_working_intime";
	public static final String EMPLOYEE_WORKING_INDATE = "employee_working_indate";
	public static final String EMPLOYEE_WORKING_STARTLAT = "employee_working_startlat";
	public static final String EMPLOYEE_WORKING_STARTLNG = "employee_working_startlng";
	public static final String EMPLOYEE_WORKING_CLIENT_NAME = "employee_working_client_name";
	public static final String EMPLOYEE_WORKING_JOBNAME = "employee_working_jobname";
	public static final String EMPLOYEE_WORKING_CREATED = "employee_working_created";
	public static final String EMPLOYEE_WORKING_END_LAT = "employee_working_end_lat";
	public static final String EMPLOYEE_WORKING_END_LNG = "employee_working_end_lng";

	public static final String EMPLOYEE_WORKING_OUTTIME = "employee_working_outtime";
	public static final String EMPLOYEE_WORKING_OUTDATE = "employee_working_outdate";
	public static final String EMPLOYEE_WORKING_IS_BREAK_STARTED = "employee_working_is_break_started";

	public static final String EMPLOYEES_TABLE = "employees";

	// ********** table for working employees ***********//
	public static final String WE_ID = "we_id";
	public static final String WORKING_EMPLOYEE_NAME = "working_employee_name";
	public static final String WORKING_EMPLOYEE_EMAIL = "working_employee_email";
	public static final String WORKING_EMPLOYEE_ADDRESS = "working_employee_address";
	public static final String WORKING_EMPLOYEE_PASSWORD = "working_employee_password";
	public static final String WORKING_EMPLOYEE_PHONENO = "working_employee_phoneno";
	public static final String WORKING_EMPLOYEE_HOURLY_RATE = "working_employee_hourly_rate";
	public static final String WORKING_EMPLOYEE_DEVICE_TOKEN = "working_employee_device_token";
	public static final String WORKING_EMPLOYEE_ONLINE = "working_employee_online";
	public static final String WORKING_EMPLOYEE_STATUS = "working_employee_status";
	public static final String WORKING_EMPLOYEE_ID = "working_employee_id";
	public static final String WORKING_EMPLOYEE_USER_ID = "working_employee_user_id";
	public static final String WORKING_EMPLOYEE_CLIENT_ID = "working_employee_client_id";
	public static final String WORKING_EMPLOYEE_JOBTYPE_ID = "working_employee_jobtype_id";
	public static final String WORKING_EMPLOYEE_INTIME = "working_employee_intime";
	public static final String WORKING_EMPLOYEE_INDATE = "working_employee_indate";
	public static final String WORKING_EMPLOYEE_OUTTIME = "working_employee_outtime";
	public static final String WORKING_EMPLOYEE_OUTDATE = "working_employee_outdate";
	public static final String WORKING_EMPLOYEE_STARTLAT = "working_employee_startlat";
	public static final String WORKING_EMPLOYEE_STARTLNG = "working_employee_startlng";
	public static final String WORKING_EMPLOYEE_ENDLAT = "working_employee_endlat";
	public static final String WORKING_EMPLOYEE_ENDLNG = "working_employee_endlng";
	public static final String WORKING_EMPLOYEE_ACTIVE = "working_employee_active";
	public static final String WORKING_EMPLOYEE_CREATED = "working_employee_created";
	public static final String WORKING_EMPLOYEE_CLIENTNAME = "working_employee_clientname";
	public static final String WORKING_EMPLOYEE_CLIENT_ADDRESS = "working_employee_client_address";
	public static final String WORKING_EMPLOYEE_JOBNAME = "working_employee_job_name";

	public static final String WORKING_EMPLOYEES_TABLE = "working_employees";

	// ********** table for time records ************//
	public static final String TR_ID = "tr_id";
	public static final String TIME_RECORD_NAME = "time_record_name";
	public static final String TIME_RECORD_EMAIL = "time_record_email";
	public static final String TIME_RECORD_ADDRESS = "time_record_address";
	public static final String TIME_RECORD_PASSWORD = "time_record_password";
	public static final String TIME_RECORD_PHONENO = "time_record_phoneno";
	public static final String TIME_RECORD_USER_HOURLY_RATE = "time_record_user_hourly_note";
	public static final String TIME_RECORD_DEVICE_TOKEN = "time_record_device_token";
	public static final String TIME_RECORD_ONLINE = "time_record_online";
	public static final String TIME_RECORD_STATUS = "time_record_status";
	public static final String TIME_RECORD_ID = "time_record_id";
	public static final String TIME_RECORD_USER_ID = "time_record_user_id";
	public static final String TIME_RECORD_CLIENT_ID = "time_record_client_id";
	public static final String TIME_RECORD_JOB_TYPE_ID = "time_record_job_type_id";
	public static final String TIME_RECORD_IN_TIME = "time_record_in_time";
	public static final String TIME_RECORD_IN_DATE = "time_record_in_date";
	public static final String TIME_RECORD_OUT_TIME = "time_record_out_time";
	public static final String TIME_RECORD_OUT_DATE = "time_record_out_date";
	public static final String TIME_RECORD_START_LAT = "time_record_start_lat";
	public static final String TIME_RECORD_START_LNG = "time_record_start_lng";
	public static final String TIME_RECORD_END_LAT = "time_record_end_lat";
	public static final String TIME_RECORD_END_LNG = "time_record_end_lng";
	public static final String TIME_RECORD_ACTIVE = "time_record_active";
	public static final String TIME_RECORD_CREATED = "time_record_created";
	public static final String TIME_RECORD_CLIENT_NAME = "time_record_client_name";
	public static final String TIME_RECORD_CLIENT_ADDRESS = "time_record_client_address";
	public static final String TIME_RECORD_JOB_NAME = "time_record_job_name";
	public static final String TIME_RECORD_IS_APPROVE = "time_record_is_approve";
	public static final String TIME_RECORD_IS_MANUALLY_ADDED = "time_record_manually_added";

	public static final String TIME_RECORDS_TABLE = "time_records";

	// *************** table for breaks of time sheets **************//
	public static final String B_ID = "b_id";
	public static final String BREAK_TR_ID = "break_tr_id";
	public static final String START_BREAK_TIME = "start_break_time";
	public static final String START_BREAK_DATE = "start_break_date";
	public static final String END_BREAK_TIME = "end_break_time";
	public static final String END_BREAK_DATE = "end_break_date";

	public static final String BREAKS_TABLE = "breaks";

	// ********** table for field notes ************//
	public static final String FN_ID = "fn_id";
	public static final String FN_NAME = "field_note_name";

	public static final String FIELD_NOTE_EMAIL = "field_note_email";
	public static final String FIELD_NOTE_ADDRESS = "field_note_address";
	public static final String FIELD_NOTE_PASSWORD = "field_note_password";
	public static final String FIELD_NOTE_PHONE_NO = "field_note_phone_no";
	public static final String FIELD_NOTE_HOURLY_RATE = "field_note_hourly_rate";
	public static final String FIELD_NOTE_DEVICE_TOKEN = "field_note_device_token";
	public static final String FIELD_NOTE_ONLINE = "field_note_online";
	public static final String FIELD_NOTE_STATUS = "field_note_status";
	public static final String FIELD_NOTE_ID = "field_note_id";
	public static final String FIELD_NOTE_USER_ID = "field_note_user_id";
	public static final String FIELD_NOTE_CLIENT_ID = "field_note_client_id";
	public static final String FIELD_NOTE_NOTE = "field_note_note";
	public static final String FIELD_NOTE_CREATED = "field_note_created";
	public static final String FIELD_NOTE_ACTIVE = "field_note_active";
	public static final String FIELD_NOTE_CLIENT_NAME = "field_note_client_name";
	public static final String FIELD_NOTE_CLIENT_ADDRESS = "field_note_client_address";

	public static final String FIELD_NOTES_TABLE = "fieldnotes";

	// ************** table for clock on emloyees ************//

	public static final String CO_ID = "co_id";
	public static final String CO_JOB_TYPE_ID = "co_job_type_id";
	public static final String CO_JOB_NAME = "co_job_name";
	public static final String CO_USER_ID = "co_user_id";
	public static final String CO_OUT_DATE = "co_out_date";
	public static final String CO_PASSWORD = "co_password";
	public static final String CO_CLIENT_NAME = "co_client_name";
	public static final String CO_ONLINE = "co_online";
	public static final String CO_INDATE = "co_indate";
	public static final String CO_CLIENT_ID = "co_client_id";
	public static final String CO_TIME_RECORD_ID = "co_time_record_id";
	public static final String CO_CREATED = "co_created";
	public static final String CO_NAME = "co_name";
	public static final String CO_INTIME = "co_intime";
	public static final String CO_PHONE_NO = "co_phone_no";
	public static final String CO_END_LNG = "co_end_lng";
	public static final String CO_OUT_TIME = "co_out_time";
	public static final String CO_USER_HOURLY_RATE = "co_user_hourly_rate";
	public static final String CO_END_LAT = "co_end_lat";
	public static final String CO_STATUS = "co_status";
	public static final String CO_START_LAT = "co_start_lat";
	public static final String CO_DEVICE_TOKEN = "co_device_token";
	public static final String CO_START_LNG = "co_start_lng";
	public static final String CO_ADDRESS = "co_address";
	public static final String CO_EMAIL = "co_email";
	public static final String CO_ACTIVE = "co_active";
	public static final String CO_CLIENT_ADDRESS = "co_client_address";

	public static final String MASSIVE_CLOCK_ON_TABLE = "massive_clock_on_table";

	// ********** fields for exporting time records **********/

	public static final String EXPORT_ID = "export_id";
	public static final String EXPORT_DATE = "export_date";
	public static final String EXPORT_NAME = "export_name";
	public static final String EXPORT_CLIENT = "export_client";
	public static final String EXPORT_JOB_NAME = "export_job_name";
	public static final String EXPORT_CLOCK_ON = "export_clock_on";
	public static final String EXPORT_CLOCK_OFF = "export_clock_off";
	public static final String EXPORT_HOURS = "export_hours";
	public static final String EXPORT_PAY = "export_pay";
	public static final String EXPORT_MANUALLY_ADDED = "manually_added";

	public static final String EXPORT_RECORDS = "export_records";

	// ******* database name *********//
	private static final String DATABASE_NAME = "timeclock";

	// ******* database version *********//
	private static final int DATABASE_VERSION = 256;

	private static final String CREATE_CLIENTS = "create table clients(_id integer primary key autoincrement , "
			+ "client_id text not null,"
			+ "employer_id text not null,"
			+ "client_name text not null,"
			+ "client_address text not null,"
			+ "active text not null);";

	private static final String CREATE_JOBTYPES = "create table job_types(j_id integer primary key autoincrement , "
			+ "job_type_id text not null,"
			+ "j_employer_id text not null,"
			+ "job_name text not null," + "job_active text not null);";

	private static final String CREATE_EMPLOYEES = "create table employees(e_id integer primary key autoincrement , "
			+ "employee_id text not null,"
			+ "employee_name text not null,"
			+ "employee_email text not null,"
			+ "employee_address text not null,"
			+ "employee_password text not null,"
			+ "employee_phoneno text not null,"
			+ "employee_profile_image text not null,"
			+ "employee_hourlyrate text not null,"
			+ "employee_devicetoken text not null,"
			+ "employee_isemployer text not null,"
			+ "employee_employer_id text not null,"
			+ "employee_online text not null,"
			+ "employee_status text not null,"
			+ "employee_created text not null,"
			+ "employee_lastlogin text not null,"
			+ "employee_is_working text not null,"
			+ "employee_working_tr_id text not null,"
			+ "employee_working_client_id text not null,"
			+ "employee_working_jobtype_id text not null,"
			+ "employee_working_intime text not null,"
			+ "employee_working_indate text not null,"
			+ "employee_working_startlat text not null,"
			+ "employee_working_startlng text not null,"
			+ "employee_working_client_name text not null,"
			+ "employee_working_jobname text not null,"
			+ "employee_working_created text not null,"
			+ "employee_working_end_lat text not null,"
			+ "employee_working_end_lng text not null,"
			+ "employee_working_outtime text not null,"

			+ "employee_working_outdate text not null,"
			+ "employee_working_is_break_started text not null);";

	private static final String CREATE_WORKING_EMPLOYEES = "create table working_employees(we_id integer primary key autoincrement , "
			+ "working_employee_name text not null,"
			+ "working_employee_email text not null,"
			+ "working_employee_address text not null,"
			+ "working_employee_password text not null,"
			+ "working_employee_phoneno text not null,"
			+ "working_employee_hourly_rate text not null,"
			+ "working_employee_device_token text not null,"
			+ "working_employee_online text not null,"
			+ "working_employee_status text not null,"
			+ "working_employee_id text not null,"
			+ "working_employee_user_id text not null,"
			+ "working_employee_client_id text not null,"
			+ "working_employee_jobtype_id text not null,"
			+ "working_employee_intime text not null,"
			+ "working_employee_indate text not null,"
			+ "working_employee_outtime text not null,"
			+ "working_employee_outdate text not null,"
			+ "working_employee_startlat text not null,"
			+ "working_employee_startlng text not null,"
			+ "working_employee_endlat text not null,"
			+ "working_employee_endlng text not null,"
			+ "working_employee_active text not null,"
			+ "working_employee_created text not null,"
			+ "working_employee_clientname text not null,"
			+ "working_employee_client_address text not null,"
			+ "working_employee_job_name text not null);";

	private static final String CREATE_TIME_RECORDS = "create table time_records(tr_id integer primary key autoincrement , "
			+ "time_record_name text not null,"
			+ "time_record_email text not null,"
			+ "time_record_address text not null,"
			+ "time_record_password text not null,"
			+ "time_record_phoneno text not null,"
			+ "time_record_user_hourly_note text not null,"
			+ "time_record_device_token text not null,"
			+ "time_record_online text not null,"
			+ "time_record_status text not null,"
			+ "time_record_id text not null,"
			+ "time_record_user_id text not null,"
			+ "time_record_client_id text not null,"
			+ "time_record_job_type_id text not null,"
			+ "time_record_in_time text not null,"
			+ "time_record_in_date text not null,"
			+ "time_record_out_time text not null,"
			+ "time_record_out_date text not null,"
			+ "time_record_start_lat text not null,"
			+ "time_record_start_lng text not null,"
			+ "time_record_end_lat text not null,"
			+ "time_record_end_lng text not null,"
			+ "time_record_active text not null,"
			+ "time_record_created text not null,"
			+ "time_record_client_name text not null,"
			+ "time_record_client_address text not null,"
			+ "time_record_job_name text not null,"
			+ "time_record_is_approve text not null,"
			+ "time_record_manually_added text not null);";

	private static final String CREATE_BREAKS = "create table breaks(b_id integer primary key autoincrement , "
			+ "break_tr_id text not null,"
			+ "start_break_time text not null,"
			+ "start_break_date text not null,"
			+ "end_break_time text not null,"

			+ "end_break_date text not null);";

	private static final String CREATE_FIELD_NOTES = "create table fieldnotes(fn_id integer primary key autoincrement , "
			+ "field_note_name text not null,"
			+ "field_note_email text not null,"
			+ "field_note_address text not null,"
			+ "field_note_password text not null,"
			+ "field_note_phone_no text not null,"
			+ "field_note_hourly_rate text not null,"
			+ "field_note_device_token text not null,"
			+ "field_note_online text not null,"
			+ "field_note_status text not null,"
			+ "field_note_id text not null,"
			+ "field_note_user_id text not null,"
			+ "field_note_client_id text not null,"
			+ "field_note_note text not null,"
			+ "field_note_created text not null,"
			+ "field_note_active text not null,"
			+ "field_note_client_name text not null,"
			+ "field_note_client_address text not null);";
	private static final String CREATE_MASSIVE_CLOCK_ON = "create table massive_clock_on_table(co_id integer primary key autoincrement , "
			+ "co_job_type_id text not null,"
			+ "co_job_name text not null,"
			+ "co_user_id text not null,"
			+ "co_out_date text not null,"
			+ "co_password text not null,"
			+ "co_client_name text not null,"
			+ "co_online text not null,"
			+ "co_indate text not null,"
			+ "co_client_id text not null,"
			+ "co_time_record_id text not null,"
			+ "co_created text not null,"
			+ "co_name text not null,"
			+ "co_intime text not null,"
			+ "co_phone_no text not null,"
			+ "co_end_lng text not null,"
			+ "co_out_time text not null,"
			+ "co_user_hourly_rate text not null,"
			+ "co_end_lat text not null,"
			+ "co_status text not null,"
			+ "co_start_lat text not null,"
			+ "co_device_token text not null,"
			+ "co_start_lng text not null,"
			+ "co_address text not null,"
			+ "co_email text not null,"
			+ "co_active text not null,"
			+ "co_client_address text not null);";

	private static final String CREATE_EXPORT_RECORDS = "create table export_records(export_id integer primary key autoincrement , "
			+ "export_date text not null,"
			+ "export_name text not null,"
			+ "export_client text not null,"
			+ "export_job_name text not null,"
			+ "export_clock_on text not null,"
			+ "export_clock_off text not null,"
			+ "export_hours text not null,"
			+ "export_pay text not null," + "manually_added text not null);";

	private final Context context;

	private DBhelper DBHelper;
	private SQLiteDatabase db;

	public DataBaseHelper(Context ctx) {
		this.context = ctx;
		DBHelper = new DBhelper(context);
	}

	private static class DBhelper extends SQLiteOpenHelper {
		DBhelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_CLIENTS);
			db.execSQL(CREATE_JOBTYPES);
			db.execSQL(CREATE_EMPLOYEES);
			db.execSQL(CREATE_WORKING_EMPLOYEES);
			db.execSQL(CREATE_TIME_RECORDS);
			db.execSQL(CREATE_FIELD_NOTES);
			db.execSQL(CREATE_MASSIVE_CLOCK_ON);
			db.execSQL(CREATE_EXPORT_RECORDS);
			db.execSQL(CREATE_BREAKS);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log.w(TAG, "Upgrading database from version " + oldVersion+
			// " to "+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS clients");
			db.execSQL("DROP TABLE IF EXISTS job_types");
			db.execSQL("DROP TABLE IF EXISTS employees");
			db.execSQL("DROP TABLE IF EXISTS working_employees");
			db.execSQL("DROP TABLE IF EXISTS time_records");
			db.execSQL("DROP TABLE IF EXISTS fieldnotes");
			db.execSQL("DROP TABLE IF EXISTS massive_clock_on_table");
			db.execSQL("DROP TABLE IF EXISTS export_records");
			db.execSQL("DROP TABLE IF EXISTS breaks");

			onCreate(db);
		}
	}

	// ---opens the database---
	public DataBaseHelper open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ----insert data into the clients table----
	public long insertClients(String client_id, String employer_id,
			String clientName, String clientAdress, String active) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(CLIENT_ID, client_id);
		initialValues.put(EMPLOYER_ID, employer_id);
		initialValues.put(CLIENT_NAME, clientName);
		initialValues.put(CLIENT_ADDRESS, clientAdress);
		initialValues.put(CLIENT_ACTIVE, active);

		return db.insert(CLIENTS_TABLE, null, initialValues);
	}

	// ---retrieves all clients from clients table--

	public Cursor getClients() throws SQLException {

		return db.query(CLIENTS_TABLE, new String[] { CLIENT_ID, EMPLOYER_ID,
				CLIENT_NAME, CLIENT_ADDRESS }, null, null, null, null, null);

	}

	// ----update client details for id-----
	public void updateClientDetails(String mClientId, String mClientName,
			String mClientAddress) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(CLIENT_NAME, mClientName);
		initialValues.put(CLIENT_ADDRESS, mClientAddress);

		db.update(CLIENTS_TABLE, initialValues, "client_id " + "=" + mClientId,
				null);
	}

	// -----delete client for id------
	public boolean deleteClientForId(String rowId) {
		return db.delete(CLIENTS_TABLE, CLIENT_ID + "=" + rowId, null) > 0;
	}

	// delete all rows in exports records table//

	public boolean deleteClients() {

		db.delete(CLIENTS_TABLE, null, null);
		return true;

	}

	// ******* insert data into jobtypes table********//
	public long insertJobTypes(String job_type_id, String j_emploer_id,
			String job_name, String job_active) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(JOB_TYPE_ID, job_type_id);
		initialValues.put(J_EMPLOYER_ID, j_emploer_id);
		initialValues.put(JOB_NAME, job_name);
		initialValues.put(JOB_ACTIVE, job_name);

		return db.insert(JOB_TYPES_TABLE, null, initialValues);

	}

	// ******** get alll the details of jobtypes**********//
	public Cursor getJobTypes() throws SQLException {

		return db.query(JOB_TYPES_TABLE, new String[] { JOB_TYPE_ID,
				J_EMPLOYER_ID, JOB_NAME, JOB_ACTIVE }, null, null, null, null,
				null);

	}

	// ----update job type details for id-----
	public void updateJobTypeDetails(String mJobTypeId, String mJobName) {

		ContentValues initialValues = new ContentValues();

		initialValues.put(JOB_NAME, mJobName);

		db.update(JOB_TYPES_TABLE, initialValues, "job_type_id " + "="
				+ mJobTypeId, null);
	}

	// -----delete job type for id------
	public boolean deleteJobTypeForId(String rowId) {
		return db.delete(JOB_TYPES_TABLE, JOB_TYPE_ID + "=" + rowId, null) > 0;
	}

	public boolean deleteJobTypes() {

		db.delete(JOB_TYPES_TABLE, null, null);
		return true;

	}

	// ******** insert employees details ***********//
	public long insertEmployees(String employee_id, String employee_name,
			String employee_email, String employee_address,
			String employee_password, String employee_phoneno,
			String employee_profile_image, String hourly_rate,
			String deviceToken, String employee_isEmployer, String employer_id,
			String employee_online, String employee_status,
			String employee_created, String employee_lastlogin,
			String employee_is_working, String employee_working_tr_id,
			String employee_working_client_id,
			String employee_working_jobtype_id, String employee_working_intime,
			String employee_working_indate, String employee_working_startlat,
			String employee_working_startlng,
			String employee_working_client_name,
			String employee_working_jobname, String employee_working_created,
			String employee_working_end_lat, String employee_working_end_lng,
			String employee_working_outtime, String employee_working_outdate,
			String employee_working_is_break_started) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(EMPLOYEE_ID, employee_id);
		initialValues.put(EMPLOYEE_NAME, employee_name);
		initialValues.put(EMPLOYEE_MAIL, employee_email);
		initialValues.put(EMPLOYEE_ADDRESS, employee_address);
		initialValues.put(EMPLOYEE_PASSWORD, employee_password);
		initialValues.put(EMPLOYEE_PHONENO, employee_phoneno);
		initialValues.put(EMPLOYEE_PROFILE_IMAGE, employee_profile_image);
		initialValues.put(EMPLOYEE_HOURLYRATE, hourly_rate);
		initialValues.put(EMPLOYEE_DEVICETOKEN, deviceToken);
		initialValues.put(EMPLOYEE_ISEMPLOYER, employee_isEmployer);
		initialValues.put(EMPLOYEE_EMPLOYER_ID, employer_id);
		initialValues.put(EMPLOYEE_ONLINE, employee_online);
		initialValues.put(EMPLOYEE_STATUS, employee_status);
		initialValues.put(EMPLOYEE_CREATED, employee_created);
		initialValues.put(EMPLOYEE_LASTLOGIN, employee_lastlogin);
		initialValues.put(EMPLOYEE_IS_WORKING, employee_is_working);

		initialValues.put(EMPLOYEE_WORKING_TR_ID, employee_working_tr_id);
		initialValues.put(EMPLOYEE_WORKING_CLIENT_ID,
				employee_working_client_id);
		initialValues.put(EMPLOYEE_WORKING_JOBTYPE_ID,
				employee_working_jobtype_id);
		initialValues.put(EMPLOYEE_WORKING_INTIME, employee_working_intime);
		initialValues.put(EMPLOYEE_WORKING_INDATE, employee_working_indate);
		initialValues.put(EMPLOYEE_WORKING_STARTLAT, employee_working_startlat);
		initialValues.put(EMPLOYEE_WORKING_STARTLNG, employee_working_startlng);
		initialValues.put(EMPLOYEE_WORKING_CLIENT_NAME,
				employee_working_client_name);
		initialValues.put(EMPLOYEE_WORKING_JOBNAME, employee_working_jobname);
		initialValues.put(EMPLOYEE_WORKING_CREATED, employee_working_created);
		initialValues.put(EMPLOYEE_WORKING_END_LAT, employee_working_end_lat);
		initialValues.put(EMPLOYEE_WORKING_END_LNG, employee_working_end_lng);

		initialValues.put(EMPLOYEE_WORKING_OUTTIME, employee_working_outtime);
		initialValues.put(EMPLOYEE_WORKING_OUTDATE, employee_working_outdate);
		initialValues.put(EMPLOYEE_WORKING_IS_BREAK_STARTED,
				employee_working_is_break_started);

		return db.insert(EMPLOYEES_TABLE, null, initialValues);

	}

	// ********* retrieve all the details of all employees (name) ************//
	public Cursor getEmployeesDetails() throws SQLException {

		return db.query(EMPLOYEES_TABLE,
				new String[] { EMPLOYEE_ID, EMPLOYEE_NAME, EMPLOYEE_MAIL,
						EMPLOYEE_ADDRESS, EMPLOYEE_PASSWORD, EMPLOYEE_PHONENO,
						EMPLOYEE_HOURLYRATE, EMPLOYEE_IS_WORKING,
						EMPLOYEE_WORKING_TR_ID, EMPLOYEE_WORKING_CLIENT_ID,
						EMPLOYEE_WORKING_JOBTYPE_ID, EMPLOYEE_WORKING_INTIME,
						EMPLOYEE_WORKING_INDATE, EMPLOYEE_WORKING_STARTLAT,
						EMPLOYEE_WORKING_STARTLNG,
						EMPLOYEE_WORKING_CLIENT_NAME, EMPLOYEE_WORKING_JOBNAME,
						EMPLOYEE_WORKING_OUTTIME, EMPLOYEE_WORKING_OUTDATE,
						EMPLOYEE_WORKING_IS_BREAK_STARTED, EMPLOYEE_ONLINE,
						EMPLOYEE_WORKING_CREATED, EMPLOYEE_WORKING_END_LAT,
						EMPLOYEE_WORKING_END_LNG, EMPLOYEE_STATUS,
						EMPLOYEE_DEVICETOKEN }, null, null, null, null, null);

	}

	// *********** get working employees ids ***************//
	public Cursor getWorkingEmployeesIds() throws SQLException {

		return db.query(EMPLOYEES_TABLE, new String[] { EMPLOYEE_ID,
				EMPLOYEE_NAME, EMPLOYEE_WORKING_CLIENT_NAME,
				EMPLOYEE_WORKING_JOBNAME, EMPLOYEE_WORKING_TR_ID

		}, EMPLOYEE_IS_WORKING + "=" + "1", null, null, null, null);

	}

	// ----update employee details for id-----
	public void updateEmployeeDetails(String employeeId, String employee_name,
			String employee_email, String employee_address,
			String employee_password, String employee_phoneno,
			String employee_profile_image, String hourly_rate,
			String deviceToken, String employee_isEmployer, String employer_id,
			String employee_online, String employee_status,
			String employee_created, String employee_lastlogin) {

		ContentValues initialValues = new ContentValues();

		initialValues.put(EMPLOYEE_NAME, employee_name);
		initialValues.put(EMPLOYEE_MAIL, employee_email);
		initialValues.put(EMPLOYEE_ADDRESS, employee_address);
		initialValues.put(EMPLOYEE_PASSWORD, employee_password);
		initialValues.put(EMPLOYEE_PHONENO, employee_phoneno);
		initialValues.put(EMPLOYEE_PROFILE_IMAGE, employee_profile_image);
		initialValues.put(EMPLOYEE_HOURLYRATE, hourly_rate);
		initialValues.put(EMPLOYEE_DEVICETOKEN, deviceToken);
		initialValues.put(EMPLOYEE_ISEMPLOYER, employee_isEmployer);
		initialValues.put(EMPLOYEE_EMPLOYER_ID, employer_id);
		initialValues.put(EMPLOYEE_ONLINE, employee_online);
		initialValues.put(EMPLOYEE_STATUS, employee_status);
		initialValues.put(EMPLOYEE_CREATED, employee_created);
		initialValues.put(EMPLOYEE_LASTLOGIN, employee_lastlogin);

		db.update(EMPLOYEES_TABLE, initialValues, EMPLOYEE_ID + "="
				+ employeeId, null);
	}

	// ----update employee details for id who are working-----
	public void updateEmployeeDetailsForWorking(String employeeId,
			String employee_is_working, String employee_working_tr_id,
			String employee_working_client_id,
			String employee_working_jobtype_id, String employee_working_intime,
			String employee_working_indate, String employee_working_startlat,
			String employee_working_startlng,
			String employee_working_client_name,
			String employee_working_created, String employee_working_end_lat,
			String employee_working_end_lng, String employee_working_jobname,
			String employee_working_outtime, String employee_working_outdate,
			String employee_working_is_break_started) {

		ContentValues initialValues = new ContentValues();

		initialValues.put(EMPLOYEE_IS_WORKING, employee_is_working);
		initialValues.put(EMPLOYEE_WORKING_TR_ID, employee_working_tr_id);
		initialValues.put(EMPLOYEE_WORKING_CLIENT_ID,
				employee_working_client_id);
		initialValues.put(EMPLOYEE_WORKING_JOBTYPE_ID,
				employee_working_jobtype_id);
		initialValues.put(EMPLOYEE_WORKING_INTIME, employee_working_intime);
		initialValues.put(EMPLOYEE_WORKING_INDATE, employee_working_indate);
		initialValues.put(EMPLOYEE_WORKING_STARTLAT, employee_working_startlat);
		initialValues.put(EMPLOYEE_WORKING_STARTLNG, employee_working_startlng);
		initialValues.put(EMPLOYEE_WORKING_CLIENT_NAME,
				employee_working_client_name);
		initialValues.put(EMPLOYEE_WORKING_CREATED, employee_working_created);
		initialValues.put(EMPLOYEE_WORKING_END_LAT, employee_working_end_lat);
		initialValues.put(EMPLOYEE_WORKING_END_LNG, employee_working_end_lng);
		initialValues.put(EMPLOYEE_WORKING_JOBNAME, employee_working_jobname);

		initialValues.put(EMPLOYEE_WORKING_OUTTIME, employee_working_outtime);
		initialValues.put(EMPLOYEE_WORKING_OUTDATE, employee_working_outdate);

		initialValues.put(EMPLOYEE_WORKING_IS_BREAK_STARTED,
				employee_working_is_break_started);

		db.update(EMPLOYEES_TABLE, initialValues, EMPLOYEE_ID + "="
				+ employeeId, null);
	}

	// -----delete job type for id------
	public boolean deleteEmployeeForId(String rowId) {
		return db.delete(EMPLOYEES_TABLE, EMPLOYEE_ID + "=" + rowId, null) > 0;
	}

	public boolean deleteEmployees() {

		db.delete(EMPLOYEES_TABLE, null, null);
		return true;

	}

	// ******** insert working employees details ***********//
	public long insertWorkingEmployees(String working_employee_name,
			String working_employee_email, String working_employee_address,
			String working_employee_password, String working_employee_phoneno,
			String working_employee_hourly_rate,
			String working_employee_device_token,
			String working_employee_online, String working_employee_status,
			String working_employee_id, String working_employee_user_id,
			String working_employee_client_id,
			String working_employee_jobtype_id, String working_employee_intime,
			String working_employee_indate, String working_employee_outtime,
			String working_employee_outdate, String working_employee_startlat,
			String working_employee_startlng, String working_employee_endlat,
			String working_employee_endlng, String working_employee_active,
			String working_employee_created,
			String working_employee_clientname,
			String working_employee_client_address,
			String working_employee_job_name) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(WORKING_EMPLOYEE_NAME, working_employee_name);
		initialValues.put(WORKING_EMPLOYEE_EMAIL, working_employee_email);

		initialValues.put(WORKING_EMPLOYEE_ADDRESS, working_employee_address);
		initialValues.put(WORKING_EMPLOYEE_PASSWORD, working_employee_password);
		initialValues.put(WORKING_EMPLOYEE_PHONENO, working_employee_phoneno);
		initialValues.put(WORKING_EMPLOYEE_HOURLY_RATE,
				working_employee_hourly_rate);
		initialValues.put(WORKING_EMPLOYEE_DEVICE_TOKEN,
				working_employee_device_token);
		initialValues.put(WORKING_EMPLOYEE_ONLINE, working_employee_online);
		initialValues.put(WORKING_EMPLOYEE_STATUS, working_employee_status);
		initialValues.put(WORKING_EMPLOYEE_ID, working_employee_id);
		initialValues.put(WORKING_EMPLOYEE_USER_ID, working_employee_user_id);
		initialValues.put(WORKING_EMPLOYEE_CLIENT_ID,
				working_employee_client_id);
		initialValues.put(WORKING_EMPLOYEE_JOBTYPE_ID,
				working_employee_jobtype_id);
		initialValues.put(WORKING_EMPLOYEE_INTIME, working_employee_intime);
		initialValues.put(WORKING_EMPLOYEE_INDATE, working_employee_indate);
		initialValues.put(WORKING_EMPLOYEE_OUTTIME, working_employee_outtime);
		initialValues.put(WORKING_EMPLOYEE_OUTDATE, working_employee_outdate);
		initialValues.put(WORKING_EMPLOYEE_STARTLAT, working_employee_startlat);
		initialValues.put(WORKING_EMPLOYEE_STARTLNG, working_employee_startlng);
		initialValues.put(WORKING_EMPLOYEE_ENDLAT, working_employee_endlat);
		initialValues.put(WORKING_EMPLOYEE_ENDLNG, working_employee_endlng);
		initialValues.put(WORKING_EMPLOYEE_ACTIVE, working_employee_active);
		initialValues.put(WORKING_EMPLOYEE_CREATED, working_employee_created);

		initialValues.put(WORKING_EMPLOYEE_CLIENTNAME,
				working_employee_clientname);
		initialValues.put(WORKING_EMPLOYEE_CLIENT_ADDRESS,
				working_employee_client_address);
		initialValues.put(WORKING_EMPLOYEE_JOBNAME, working_employee_job_name);

		return db.insert(WORKING_EMPLOYEES_TABLE, null, initialValues);

	}

	// ********* retrieve all the details of all working employees (name)
	// ************//
	public Cursor getWorkingEmployeesDetails() throws SQLException {

		return db.query(WORKING_EMPLOYEES_TABLE, new String[] {
				WORKING_EMPLOYEE_USER_ID, WORKING_EMPLOYEE_CLIENT_ID,
				WORKING_EMPLOYEE_JOBTYPE_ID, WORKING_EMPLOYEE_NAME,
				WORKING_EMPLOYEE_INTIME, WORKING_EMPLOYEE_INDATE,
				WORKING_EMPLOYEE_CLIENTNAME, WORKING_EMPLOYEE_JOBNAME }, null,
				null, null, null, null);

	}

	public boolean deleteWorkingEmployees() {

		db.delete(WORKING_EMPLOYEES_TABLE, null, null);
		return true;

	}

	// ************** inserting time records of all users ************//

	public long insertTimeRecords(String timeRecordName,
			String timeRecordEmail, String timeRecordAddress,
			String timeRecordPassword, String timeRecordPhoneno,
			String timeRecordHourly_rate, String timeRecordDeviceToken,
			String timeRecordOnline, String timeRecordStatus,
			String timeRecordId, String timeRecordUserId,
			String timeRecordClientId, String timeRecordJobTypeId,
			String timeRecordInTime, String timeRecordInDate,
			String timeRecordOuttime, String timeRecordOutDate,
			String timeRecordStartLat, String timeRecordStartLng,
			String timeRecordEndLat, String timeRecordEndLng,
			String timeRecordActive, String timeRecordCreated,
			String timeRecordClientName, String timeRecordClientAddress,
			String timeRecordJobName, String timeRecordApprove,
			String timeRecordIsMaanuallyAdded) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(TIME_RECORD_NAME, timeRecordName);
		initialValues.put(TIME_RECORD_EMAIL, timeRecordEmail);

		initialValues.put(TIME_RECORD_ADDRESS, timeRecordAddress);
		initialValues.put(TIME_RECORD_PASSWORD, timeRecordPassword);
		initialValues.put(TIME_RECORD_PHONENO, timeRecordPhoneno);
		initialValues.put(TIME_RECORD_USER_HOURLY_RATE, timeRecordHourly_rate);
		initialValues.put(TIME_RECORD_DEVICE_TOKEN, timeRecordDeviceToken);
		initialValues.put(TIME_RECORD_ONLINE, timeRecordOnline);
		initialValues.put(TIME_RECORD_STATUS, timeRecordStatus);
		initialValues.put(TIME_RECORD_ID, timeRecordId);
		initialValues.put(TIME_RECORD_USER_ID, timeRecordUserId);
		initialValues.put(TIME_RECORD_CLIENT_ID, timeRecordClientId);
		initialValues.put(TIME_RECORD_JOB_TYPE_ID, timeRecordJobTypeId);
		initialValues.put(TIME_RECORD_IN_TIME, timeRecordInTime);
		initialValues.put(TIME_RECORD_IN_DATE, timeRecordInDate);
		initialValues.put(TIME_RECORD_OUT_TIME, timeRecordOuttime);
		initialValues.put(TIME_RECORD_OUT_DATE, timeRecordOutDate);
		initialValues.put(TIME_RECORD_START_LAT, timeRecordStartLat);
		initialValues.put(TIME_RECORD_START_LNG, timeRecordStartLng);
		initialValues.put(TIME_RECORD_END_LAT, timeRecordEndLat);
		initialValues.put(TIME_RECORD_END_LNG, timeRecordEndLng);
		initialValues.put(TIME_RECORD_ACTIVE, timeRecordActive);
		initialValues.put(TIME_RECORD_CREATED, timeRecordCreated);

		initialValues.put(TIME_RECORD_CLIENT_NAME, timeRecordClientName);
		initialValues.put(TIME_RECORD_CLIENT_ADDRESS, timeRecordClientAddress);
		initialValues.put(TIME_RECORD_JOB_NAME, timeRecordJobName);
		initialValues.put(TIME_RECORD_IS_APPROVE, timeRecordApprove);
		initialValues.put(TIME_RECORD_IS_MANUALLY_ADDED,
				timeRecordIsMaanuallyAdded);

		return db.insert(TIME_RECORDS_TABLE, null, initialValues);

	}

	// ********* retrieve all the details of all working employees (name)
	// ************//
	public Cursor getTimeRecords() throws SQLException {

		String where = "time_record_is_approve" + "='" + "1" + "'";

		return db.query(TIME_RECORDS_TABLE, new String[] { TIME_RECORD_NAME,
				TIME_RECORD_EMAIL, TIME_RECORD_ADDRESS, TIME_RECORD_PASSWORD,
				TIME_RECORD_PHONENO, TIME_RECORD_USER_HOURLY_RATE,
				TIME_RECORD_DEVICE_TOKEN, TIME_RECORD_ONLINE,
				TIME_RECORD_STATUS, TIME_RECORD_ID, TIME_RECORD_USER_ID,
				TIME_RECORD_CLIENT_ID, TIME_RECORD_JOB_TYPE_ID,
				TIME_RECORD_IN_TIME, TIME_RECORD_IN_DATE, TIME_RECORD_OUT_TIME,
				TIME_RECORD_OUT_DATE, TIME_RECORD_START_LAT,
				TIME_RECORD_START_LNG, TIME_RECORD_END_LAT,
				TIME_RECORD_END_LNG, TIME_RECORD_ACTIVE, TIME_RECORD_CREATED,
				TIME_RECORD_CLIENT_NAME, TIME_RECORD_CLIENT_ADDRESS,
				TIME_RECORD_JOB_NAME, TIME_RECORD_IS_APPROVE,
				TIME_RECORD_IS_MANUALLY_ADDED }, where, null, null, null, null);

	}

	// ********* retrieve all the details of all working employees (name)
	// ************//
	public Cursor getTimeRecordsForSelection(String where) throws SQLException {

		return db.query(TIME_RECORDS_TABLE, new String[] { TIME_RECORD_NAME,
				TIME_RECORD_EMAIL, TIME_RECORD_ADDRESS, TIME_RECORD_PASSWORD,
				TIME_RECORD_PHONENO, TIME_RECORD_USER_HOURLY_RATE,
				TIME_RECORD_DEVICE_TOKEN, TIME_RECORD_ONLINE,
				TIME_RECORD_STATUS, TIME_RECORD_ID, TIME_RECORD_USER_ID,
				TIME_RECORD_CLIENT_ID, TIME_RECORD_JOB_TYPE_ID,
				TIME_RECORD_IN_TIME, TIME_RECORD_IN_DATE, TIME_RECORD_OUT_TIME,
				TIME_RECORD_OUT_DATE, TIME_RECORD_START_LAT,
				TIME_RECORD_START_LNG, TIME_RECORD_END_LAT,
				TIME_RECORD_END_LNG, TIME_RECORD_ACTIVE, TIME_RECORD_CREATED,
				TIME_RECORD_CLIENT_NAME, TIME_RECORD_CLIENT_ADDRESS,
				TIME_RECORD_JOB_NAME, TIME_RECORD_IS_APPROVE,
				TIME_RECORD_IS_MANUALLY_ADDED }, where, null, null, null, null);

	}

	// -----delete time record for id------
	public boolean deleteTimeRecordForId(String rowId) {
		return db
				.delete(TIME_RECORDS_TABLE, TIME_RECORD_ID + "=" + rowId, null) > 0;
	}

	// delete multiple records in time records table
	public int deleteRec(String[] ids) { // ids is an array

		return db.delete(TIME_RECORDS_TABLE, TIME_RECORD_ID + " IN ("
				+ new String(new char[ids.length - 1]).replace("\0", "?,")
				+ "?)", ids);

	}

	// delete all rows in time records table//

	public boolean deleteAllTimeRecords() {

		db.delete(TIME_RECORDS_TABLE, null, null);
		return true;

	}

	// ----update job type details for id-----
	public void updateTimeRecordDetails(String timeRecordName,
			String timeRecordEmail, String timeRecordAddress,
			String timeRecordPassword, String timeRecordPhoneno,
			String timeRecordHourly_rate, String timeRecordDeviceToken,
			String timeRecordOnline, String timeRecordStatus,
			String timeRecordId, String timeRecordUserId,
			String timeRecordClientId, String timeRecordJobTypeId,
			String timeRecordInTime, String timeRecordInDate,
			String timeRecordOuttime, String timeRecordOutDate,
			String timeRecordStartLat, String timeRecordStartLng,
			String timeRecordEndLat, String timeRecordEndLng,
			String timeRecordActive, String timeRecordCreated,
			String timeRecordClientName, String timeRecordClientAddress,
			String timeRecordJobName, String is_approve,
			String is_manually_added) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(TIME_RECORD_NAME, timeRecordName);
		initialValues.put(TIME_RECORD_EMAIL, timeRecordEmail);

		initialValues.put(TIME_RECORD_ADDRESS, timeRecordAddress);
		initialValues.put(TIME_RECORD_PASSWORD, timeRecordPassword);
		initialValues.put(TIME_RECORD_PHONENO, timeRecordPhoneno);
		initialValues.put(TIME_RECORD_USER_HOURLY_RATE, timeRecordHourly_rate);
		initialValues.put(TIME_RECORD_DEVICE_TOKEN, timeRecordDeviceToken);
		initialValues.put(TIME_RECORD_ONLINE, timeRecordOnline);
		initialValues.put(TIME_RECORD_STATUS, timeRecordStatus);
		initialValues.put(TIME_RECORD_ID, timeRecordId);
		initialValues.put(TIME_RECORD_USER_ID, timeRecordUserId);
		initialValues.put(TIME_RECORD_CLIENT_ID, timeRecordClientId);
		initialValues.put(TIME_RECORD_JOB_TYPE_ID, timeRecordJobTypeId);
		initialValues.put(TIME_RECORD_IN_TIME, timeRecordInTime);
		initialValues.put(TIME_RECORD_IN_DATE, timeRecordInDate);
		initialValues.put(TIME_RECORD_OUT_TIME, timeRecordOuttime);
		initialValues.put(TIME_RECORD_OUT_DATE, timeRecordOutDate);
		initialValues.put(TIME_RECORD_START_LAT, timeRecordStartLat);
		initialValues.put(TIME_RECORD_START_LNG, timeRecordStartLng);
		initialValues.put(TIME_RECORD_END_LAT, timeRecordEndLat);
		initialValues.put(TIME_RECORD_END_LNG, timeRecordEndLng);
		initialValues.put(TIME_RECORD_ACTIVE, timeRecordActive);
		initialValues.put(TIME_RECORD_CREATED, timeRecordCreated);

		initialValues.put(TIME_RECORD_CLIENT_NAME, timeRecordClientName);
		initialValues.put(TIME_RECORD_CLIENT_ADDRESS, timeRecordClientAddress);
		initialValues.put(TIME_RECORD_JOB_NAME, timeRecordJobName);
		initialValues.put(TIME_RECORD_IS_APPROVE, is_approve);
		initialValues.put(TIME_RECORD_IS_MANUALLY_ADDED, is_manually_added);

		db.update(TIME_RECORDS_TABLE, initialValues, "time_record_id " + "="
				+ timeRecordId, null);
	}

	// ************** inserting field notes of all users ************//

	public long insertBreaksForTID(String break_tr_id, String start_break_time,
			String start_break_date, String end_break_time,
			String end_break_date) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(BREAK_TR_ID, break_tr_id);
		initialValues.put(START_BREAK_TIME, start_break_time);

		initialValues.put(START_BREAK_DATE, start_break_date);
		initialValues.put(END_BREAK_TIME, end_break_time);
		initialValues.put(END_BREAK_DATE, end_break_date);

		return db.insert(BREAKS_TABLE, null, initialValues);

	}

	// get the breaks for time sheets of particular id
	public Cursor getBreaksForTimeSheet(String break_tr_id) throws SQLException {

		String where = "break_tr_id" + "='" + break_tr_id + "'";

		return db.query(BREAKS_TABLE, new String[] { START_BREAK_TIME,
				START_BREAK_DATE, END_BREAK_TIME, END_BREAK_DATE }, where,
				null, null, null, null);

	}

	// delete all rows in breaks for time records table//

	public boolean deleteBreaksForAllTimeRecords() {

		db.delete(BREAKS_TABLE, null, null);
		return true;

	}

	// ************** inserting field notes of all users ************//

	public long insertFieldNotes(String field_note_name,
			String field_note_email, String field_note_address,
			String field_note_password, String field_note_phone_no,
			String field_note_hourly_rate, String field_note_device_token,
			String field_note_online, String field_note_status,
			String field_note_id, String field_note_user_id,
			String field_note_client_id, String field_note_note,
			String field_note_created, String field_note_active,
			String field_note_client_name, String field_note_client_address) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(FN_NAME, field_note_name);
		initialValues.put(FIELD_NOTE_EMAIL, field_note_email);

		initialValues.put(FIELD_NOTE_ADDRESS, field_note_address);
		initialValues.put(FIELD_NOTE_PASSWORD, field_note_password);
		initialValues.put(FIELD_NOTE_PHONE_NO, field_note_phone_no);
		initialValues.put(FIELD_NOTE_HOURLY_RATE, field_note_hourly_rate);
		initialValues.put(FIELD_NOTE_DEVICE_TOKEN, field_note_device_token);
		initialValues.put(FIELD_NOTE_ONLINE, field_note_online);
		initialValues.put(FIELD_NOTE_STATUS, field_note_status);
		initialValues.put(FIELD_NOTE_ID, field_note_id);
		initialValues.put(FIELD_NOTE_USER_ID, field_note_user_id);
		initialValues.put(FIELD_NOTE_CLIENT_ID, field_note_client_id);
		initialValues.put(FIELD_NOTE_NOTE, field_note_note);
		initialValues.put(FIELD_NOTE_CREATED, field_note_created);
		initialValues.put(FIELD_NOTE_ACTIVE, field_note_active);
		initialValues.put(FIELD_NOTE_CLIENT_NAME, field_note_client_name);
		initialValues.put(FIELD_NOTE_CLIENT_ADDRESS, field_note_client_address);

		return db.insert(FIELD_NOTES_TABLE, null, initialValues);

	}

	// update field notes infield notes table
	public void updateFieldNotes(String field_note_name,
			String field_note_email, String field_note_address,
			String field_note_password, String field_note_phone_no,
			String field_note_hourly_rate, String field_note_device_token,
			String field_note_online, String field_note_status,
			String field_note_id, String field_note_user_id,
			String field_note_client_id, String field_note_note,
			String field_note_created, String field_note_active,
			String field_note_client_name, String field_note_client_address) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(FN_NAME, field_note_name);
		initialValues.put(FIELD_NOTE_EMAIL, field_note_email);

		initialValues.put(FIELD_NOTE_ADDRESS, field_note_address);
		initialValues.put(FIELD_NOTE_PASSWORD, field_note_password);
		initialValues.put(FIELD_NOTE_PHONE_NO, field_note_phone_no);
		initialValues.put(FIELD_NOTE_HOURLY_RATE, field_note_hourly_rate);
		initialValues.put(FIELD_NOTE_DEVICE_TOKEN, field_note_device_token);
		initialValues.put(FIELD_NOTE_ONLINE, field_note_online);
		initialValues.put(FIELD_NOTE_STATUS, field_note_status);
		initialValues.put(FIELD_NOTE_ID, field_note_id);
		initialValues.put(FIELD_NOTE_USER_ID, field_note_user_id);
		initialValues.put(FIELD_NOTE_CLIENT_ID, field_note_client_id);
		initialValues.put(FIELD_NOTE_NOTE, field_note_note);
		initialValues.put(FIELD_NOTE_CREATED, field_note_created);
		initialValues.put(FIELD_NOTE_ACTIVE, field_note_active);
		initialValues.put(FIELD_NOTE_CLIENT_NAME, field_note_client_name);
		initialValues.put(FIELD_NOTE_CLIENT_ADDRESS, field_note_client_address);

		db.update(FIELD_NOTES_TABLE, initialValues, FIELD_NOTE_ID + "="
				+ field_note_id, null);

	}

	// ********* retrieve all the details of all field notes (name)
	// ************//
	public Cursor getFieldNotes() throws SQLException {

		return db.query(FIELD_NOTES_TABLE, new String[] { FN_NAME,
				FIELD_NOTE_EMAIL, FIELD_NOTE_ADDRESS, FIELD_NOTE_PASSWORD,
				FIELD_NOTE_PHONE_NO, FIELD_NOTE_HOURLY_RATE,
				FIELD_NOTE_DEVICE_TOKEN, FIELD_NOTE_ONLINE, FIELD_NOTE_STATUS,
				FIELD_NOTE_ID, FIELD_NOTE_USER_ID, FIELD_NOTE_CLIENT_ID,
				FIELD_NOTE_NOTE, FIELD_NOTE_CREATED, FIELD_NOTE_ACTIVE,
				FIELD_NOTE_CLIENT_NAME, FIELD_NOTE_CLIENT_ADDRESS }, null,
				null, null, null, null);

	}

	// ********* retrieve all the details of selected field notes (name)
	// ************//
	public Cursor getSelectedFieldNotes(String where) throws SQLException {

		return db.query(FIELD_NOTES_TABLE, new String[] { FN_NAME,
				FIELD_NOTE_EMAIL, FIELD_NOTE_ADDRESS, FIELD_NOTE_PASSWORD,
				FIELD_NOTE_PHONE_NO, FIELD_NOTE_HOURLY_RATE,
				FIELD_NOTE_DEVICE_TOKEN, FIELD_NOTE_ONLINE, FIELD_NOTE_STATUS,
				FIELD_NOTE_ID, FIELD_NOTE_USER_ID, FIELD_NOTE_CLIENT_ID,
				FIELD_NOTE_NOTE, FIELD_NOTE_CREATED, FIELD_NOTE_ACTIVE,
				FIELD_NOTE_CLIENT_NAME, FIELD_NOTE_CLIENT_ADDRESS }, where,
				null, null, null, null);

	}

	// delete multiple field notes in time records table
	public int deleteFieldNotes(String[] ids) { // ids is an array

		return db.delete(FIELD_NOTES_TABLE, FIELD_NOTE_ID + " IN ("
				+ new String(new char[ids.length - 1]).replace("\0", "?,")
				+ "?)", ids);

	}

	// delete all rows in filed notes table//

	public boolean deleteAllFieldNotes() {

		db.delete(FIELD_NOTES_TABLE, null, null);
		return true;

	}

	public long insertClockOnDetails(String co_job_type_id, String co_job_name,
			String co_user_id, String co_out_date, String co_password,
			String co_client_name, String co_online, String co_indate,
			String co_client_id, String co_time_record_id, String co_created,
			String co_name, String co_intime, String co_phone_no,
			String co_end_lng, String co_out_time, String co_user_hourly_rate,
			String co_end_lat, String co_status, String co_start_lat,
			String co_device_token, String co_start_lng, String co_address,
			String co_email, String co_active, String co_client_address) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(CO_JOB_TYPE_ID, co_job_type_id);
		initialValues.put(CO_JOB_NAME, co_job_name);

		initialValues.put(CO_USER_ID, co_user_id);
		initialValues.put(CO_OUT_DATE, co_out_date);
		initialValues.put(CO_PASSWORD, co_password);
		initialValues.put(CO_CLIENT_NAME, co_client_name);
		initialValues.put(CO_ONLINE, co_online);
		initialValues.put(CO_INDATE, co_indate);
		initialValues.put(CO_CLIENT_ID, co_client_id);
		initialValues.put(CO_TIME_RECORD_ID, co_time_record_id);
		initialValues.put(CO_CREATED, co_created);
		initialValues.put(CO_NAME, co_name);
		initialValues.put(CO_INTIME, co_intime);
		initialValues.put(CO_PHONE_NO, co_phone_no);
		initialValues.put(CO_END_LNG, co_end_lng);
		initialValues.put(CO_OUT_TIME, co_out_time);
		initialValues.put(CO_USER_HOURLY_RATE, co_user_hourly_rate);
		initialValues.put(CO_END_LAT, co_end_lat);
		initialValues.put(CO_STATUS, co_status);
		initialValues.put(CO_START_LAT, co_start_lat);
		initialValues.put(CO_DEVICE_TOKEN, co_device_token);
		initialValues.put(CO_START_LNG, co_start_lng);
		initialValues.put(CO_ADDRESS, co_address);
		initialValues.put(CO_EMAIL, co_email);
		initialValues.put(CO_ACTIVE, co_active);
		initialValues.put(CO_CLIENT_ADDRESS, co_client_address);

		return db.insert(MASSIVE_CLOCK_ON_TABLE, null, initialValues);

	}

	// delete multiple field notes in time records table
	public int deleteDetailsOnClockOff(String[] ids) { // ids is an array

		return db.delete(MASSIVE_CLOCK_ON_TABLE, CO_USER_ID + " IN ("
				+ new String(new char[ids.length - 1]).replace("\0", "?,")
				+ "?)", ids);

	}

	// update field notes infield notes table
	public void updateClockOnDetailsForSwitchLocation(String co_job_type_id,
			String co_job_name, String co_user_id, String co_out_date,
			String co_password, String co_client_name, String co_online,
			String co_indate, String co_client_id, String co_time_record_id,
			String co_created, String co_name, String co_intime,
			String co_phone_no, String co_end_lng, String co_out_time,
			String co_user_hourly_rate, String co_end_lat, String co_status,
			String co_start_lat, String co_device_token, String co_start_lng,
			String co_address, String co_email, String co_active,
			String co_client_address, String newRecordId) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(CO_JOB_TYPE_ID, co_job_type_id);
		initialValues.put(CO_JOB_NAME, co_job_name);

		initialValues.put(CO_USER_ID, co_user_id);
		initialValues.put(CO_OUT_DATE, co_out_date);
		initialValues.put(CO_PASSWORD, co_password);
		initialValues.put(CO_CLIENT_NAME, co_client_name);
		initialValues.put(CO_ONLINE, co_online);
		initialValues.put(CO_INDATE, co_indate);
		initialValues.put(CO_CLIENT_ID, co_client_id);
		initialValues.put(CO_TIME_RECORD_ID, newRecordId);
		initialValues.put(CO_CREATED, co_created);
		initialValues.put(CO_NAME, co_name);
		initialValues.put(CO_INTIME, co_intime);
		initialValues.put(CO_PHONE_NO, co_phone_no);
		initialValues.put(CO_END_LNG, co_end_lng);
		initialValues.put(CO_OUT_TIME, co_out_time);
		initialValues.put(CO_USER_HOURLY_RATE, co_user_hourly_rate);
		initialValues.put(CO_END_LAT, co_end_lat);
		initialValues.put(CO_STATUS, co_status);
		initialValues.put(CO_START_LAT, co_start_lat);
		initialValues.put(CO_DEVICE_TOKEN, co_device_token);
		initialValues.put(CO_START_LNG, co_start_lng);
		initialValues.put(CO_ADDRESS, co_address);
		initialValues.put(CO_EMAIL, co_email);
		initialValues.put(CO_ACTIVE, co_active);
		initialValues.put(CO_CLIENT_ADDRESS, co_client_address);

		db.update(MASSIVE_CLOCK_ON_TABLE, initialValues, CO_TIME_RECORD_ID
				+ "=" + co_time_record_id, null);

	}

	// ********* retrieve all the details of all clocked on employees
	// ************//
	// public Cursor getDetailsForClockOn(String[] ids) throws SQLException {
	//
	// String where = CO_USER_ID + " IN ("
	// + new String(new char[ids.length - 1]).replace("\0", "?,")
	// + "?)";
	//
	// return db.query(MASSIVE_CLOCK_ON_TABLE, new String[] { CO_USER_ID,
	// CO_NAME, CO_CLIENT_NAME, CO_JOB_NAME, CO_TIME_RECORD_ID },
	// where, ids, null, null, null);
	//
	// }

	public Cursor getDetailsForClockOn(String[] ids) throws SQLException {

		String where = CO_USER_ID + " IN ("
				+ new String(new char[ids.length - 1]).replace("\0", "?,")
				+ "?)";

		return db.query(MASSIVE_CLOCK_ON_TABLE, new String[] { CO_USER_ID,
				CO_NAME, CO_CLIENT_NAME, CO_JOB_NAME, CO_TIME_RECORD_ID },
				null, null, null, null, null);

	}

	// delete all rows in clockon records table//

	public boolean deleteDetailsForClockOn() {

		db.delete(MASSIVE_CLOCK_ON_TABLE, null, null);
		return true;

	}

	// insert time records for exporting...

	public long insertExportRecords(String date, String name, String client,
			String jobName, String clockOn, String clockOff, String hours,
			String pay, String maually_added) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(EXPORT_DATE, date);
		initialValues.put(EXPORT_NAME, name);

		initialValues.put(EXPORT_CLIENT, client);
		initialValues.put(EXPORT_JOB_NAME, jobName);
		initialValues.put(EXPORT_CLOCK_ON, clockOn);
		initialValues.put(EXPORT_CLOCK_OFF, clockOff);

		initialValues.put(EXPORT_HOURS, hours);
		initialValues.put(EXPORT_PAY, pay);
		initialValues.put(EXPORT_MANUALLY_ADDED, maually_added);

		return db.insert(EXPORT_RECORDS, null, initialValues);

	}

	// delete all rows in exports records table//

	public boolean deleteExportTimeRecords() {

		db.delete(EXPORT_RECORDS, null, null);
		return true;

	}
}
