package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.timeclockmobile.adapter.TimeRecordsAdapter;
import com.timeclockmobile.data.TimeRecords;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class TimeRecordDetailsActivity extends Activity {

	private ListView timeRecordsListView;
	private Button deleteBtn;
	private Button editBtn;
	private ImageButton backBtn;

	public TimeRecordsAdapter adapter;
	private ArrayList<TimeRecords> timeRecordsList;
	private ArrayList<String> checkedTimeRecordsList;
	private ArrayList<TimeRecords> checkedRecordsList;
	public TimeRecordsActivity timeRecordsActivity = new TimeRecordsActivity();

	private String numberOfRecords;
	private DataBaseHelper db = new DataBaseHelper(
			TimeRecordDetailsActivity.this);

	String timeRecordId;
	private String employeeId;
	Utils utils = new Utils();

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.time_record_detail_list);

		timeRecordsListView = (ListView) findViewById(R.id.time_records_list);
		deleteBtn = (Button) findViewById(R.id.delete_time_record);
		editBtn = (Button) findViewById(R.id.edit_time_record);
		backBtn = (ImageButton) findViewById(R.id.backBtn);

		Intent intent = getIntent();

		timeRecordsList = (ArrayList<TimeRecords>) intent
				.getSerializableExtra("arrayList");
		// timeRecordsList = timeRecordsActivity.getTimeRecordsArayList();

		// load time recprds list
		loadTimeRecordsList();

		deleteBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (adapter != null) {
					checkedTimeRecordsList = adapter
							.getArrayListOfTimeRecords();
					checkedRecordsList = adapter.getCheckedTimeRecords();

					if (checkedTimeRecordsList.size() == 1) {

						timeRecordId = checkedTimeRecordsList.get(0);
						employeeId = checkedRecordsList.get(0).getUserid();

						// for deleting single time record

						if (utils
								.isConnectingToInternet(TimeRecordDetailsActivity.this)) {
							new DeleteTimeRecordTask(checkedRecordsList,
									timeRecordId, timeRecordsList,
									TimeRecordDetailsActivity.this, false)
									.execute("");
						} else {
							utils.showDialog(TimeRecordDetailsActivity.this);
						}

					} else if (checkedTimeRecordsList.size() > 1) {

						numberOfRecords = checkedTimeRecordsList.size() + "";

						if (utils
								.isConnectingToInternet(TimeRecordDetailsActivity.this)) {
							// deleting multiple records
							new DeleteMultipleRecordsTask(checkedRecordsList,
									checkedTimeRecordsList).execute("");
						} else {
							utils.showDialog(TimeRecordDetailsActivity.this);
						}

					}
				}

			}
		});

		editBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (adapter != null) {
					checkedTimeRecordsList = adapter
							.getArrayListOfTimeRecords();
					checkedRecordsList = adapter.getCheckedTimeRecords();
				}
				if (checkedTimeRecordsList.size() == 1) {
					timeRecordId = checkedTimeRecordsList.get(0);
					employeeId = checkedRecordsList.get(0).getUserid();

					Intent intent = new Intent(TimeRecordDetailsActivity.this,
							AddTimeRecordActivity.class);

					intent.putExtra("type", "update");
					intent.putExtra("employeeId", employeeId);
					intent.putExtra("trid", timeRecordId);
					startActivity(intent);
					finish();
				}

			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Intent intent = new Intent(TimeRecordDetailsActivity.this,
				TimeRecordsActivity.class);
		startActivity(intent);
		finish();
	}

	public void loadTimeRecordsList() {
		adapter = new TimeRecordsAdapter(TimeRecordDetailsActivity.this,
				R.layout.time_record_list_inflate, timeRecordsList);

		timeRecordsListView.setAdapter(adapter);
	}

	public class DeleteTimeRecordTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();
		Context mContext;
		String timeRecord_Id;
		ArrayList<TimeRecords> checkedRecord;
		public ProgressDialog progressDialog;
		private ArrayList<TimeRecords> timeRecordsArray;
		boolean isDenied;

		public DeleteTimeRecordTask(ArrayList<TimeRecords> checkedRecordObject,
				String tr_id, ArrayList<TimeRecords> timeRecordsList,
				Context mContext, boolean isDenied) {
			timeRecord_Id = tr_id;
			checkedRecord = checkedRecordObject;
			this.mContext = mContext;
			progressDialog = new ProgressDialog(mContext);
			this.isDenied = isDenied;
			timeRecordsArray = timeRecordsList;
			db = new DataBaseHelper(mContext);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_TIME_RECORD));
				entity.addPart(RequestParameters.TR_ID, new StringBody(
						timeRecord_Id));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");

				if (success.equalsIgnoreCase("1")
						&& message.equalsIgnoreCase("Success")) {
					Toast.makeText(mContext, "Time Record has been deleted",
							Toast.LENGTH_LONG).show();

					// deleting single time record from database
					db.open();
					db.deleteTimeRecordForId(timeRecord_Id);
					db.close();

					for (int i = 0; i < checkedRecord.size(); i++) {
						timeRecordsArray.remove(checkedRecord.get(i));
					}

					//if (!isDenied) {
						adapter.notifyDataSetChanged();
					//}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class DeleteMultipleRecordsTask extends
			AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		private ArrayList<String> checkedTimeRecordsArray;

		public ProgressDialog progressDialog;
		ArrayList<TimeRecords> checkedRecord;

		public DeleteMultipleRecordsTask(
				ArrayList<TimeRecords> checkedRecordObject,
				ArrayList<String> checkedTimeRecords) {

			checkedTimeRecordsArray = checkedTimeRecords;
			checkedRecord = checkedRecordObject;
			progressDialog = new ProgressDialog(TimeRecordDetailsActivity.this);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.DELETE_MULTIPLE_RECORDS));
				entity.addPart(RequestParameters.NUMBER_OF_RECORDS,
						new StringBody(numberOfRecords));
				for (int i = 0; i < checkedTimeRecordsArray.size(); i++) {
					entity.addPart(RequestParameters.TIME_RECORD_ID + (i + 1),
							new StringBody(checkedTimeRecordsArray.get(i)));

				}

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {

				String[] ids = new String[checkedTimeRecordsArray.size()];
				JSONObject jsonObject = jsonResponse.getJSONObject("response");

				String success = jsonObject.getString("success");
				String message = jsonObject.getString("message");

				if (success.equalsIgnoreCase("1")
						&& message.equalsIgnoreCase("Success")) {
					Toast.makeText(getApplicationContext(),
							"Time Record has been deleted", Toast.LENGTH_LONG)
							.show();
					for (int i = 0; i < checkedTimeRecordsArray.size(); i++) {
						ids[i] = checkedTimeRecordsArray.get(i);
					}
					// deleting single time record from database
					db.open();
					db.deleteRec(ids);
					db.close();

					for (int i = 0; i < checkedRecord.size(); i++) {
						timeRecordsList.remove(checkedRecord.get(i));
					}

					adapter.notifyDataSetChanged();

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
