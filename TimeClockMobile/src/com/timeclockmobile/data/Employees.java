package com.timeclockmobile.data;

import java.io.Serializable;

public class Employees implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String id;
	public String name;
	public String email;
	public String address;
	public String password;
	public String phoneno;
	public String profileimage;
	public String hourlyrate;
	public String devicetoken;
	public String isemployer;
	public String employerid;
	public String online;
	public String status;
	public boolean checked;

	public String isWorking;
	public String isBreakStarted;

	public String getIsBreakStarted() {
		return isBreakStarted;
	}

	public void setIsBreakStarted(String isBreakStarted) {
		this.isBreakStarted = isBreakStarted;
	}

	public String getIsWorking() {
		return isWorking;
	}

	public void setIsWorking(String isWorking) {
		this.isWorking = isWorking;
	}

	public String getWorking_tr_id() {
		return working_tr_id;
	}

	public void setWorking_tr_id(String working_tr_id) {
		this.working_tr_id = working_tr_id;
	}

	public String getWorking_client_id() {
		return working_client_id;
	}

	public void setWorking_client_id(String working_client_id) {
		this.working_client_id = working_client_id;
	}

	public String getWorking_jobTypeId() {
		return working_jobTypeId;
	}

	public void setWorking_jobTypeId(String working_jobTypeId) {
		this.working_jobTypeId = working_jobTypeId;
	}

	public String getWorking_intime() {
		return working_intime;
	}

	public void setWorking_intime(String working_intime) {
		this.working_intime = working_intime;
	}

	public String getWorking_indate() {
		return working_indate;
	}

	public void setWorking_indate(String working_indate) {
		this.working_indate = working_indate;
	}

	public String getWorking_startlat() {
		return working_startlat;
	}

	public void setWorking_startlat(String working_startlat) {
		this.working_startlat = working_startlat;
	}

	public String getWorking_startlng() {
		return working_startlng;
	}

	public void setWorking_startlng(String working_startlng) {
		this.working_startlng = working_startlng;
	}

	public String getWorking_clientName() {
		return working_clientName;
	}

	public void setWorking_clientName(String working_clientName) {
		this.working_clientName = working_clientName;
	}

	public String getWorking_jobName() {
		return working_jobName;
	}

	public void setWorking_jobName(String working_jobName) {
		this.working_jobName = working_jobName;
	}

	public String working_tr_id;
	public String working_client_id;
	public String working_jobTypeId;
	public String working_intime;
	public String working_indate;
	public String working_startlat;
	public String working_startlng;
	public String working_clientName;
	public String working_jobName;
	public String working_outtime;

	public String timerText;

	public String getTimerText() {
		return timerText;
	}

	public void setTimerText(String timerText) {
		this.timerText = timerText;
	}

	public String getWorking_outtime() {
		return working_outtime;
	}

	public void setWorking_outtime(String working_outtime) {
		this.working_outtime = working_outtime;
	}

	public String getWorking_outdate() {
		return working_outdate;
	}

	public void setWorking_outdate(String working_outdate) {
		this.working_outdate = working_outdate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String working_outdate;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getProfileimage() {
		return profileimage;
	}

	public void setProfileimage(String profileimage) {
		this.profileimage = profileimage;
	}

	public String getHourlyrate() {
		return hourlyrate;
	}

	public void setHourlyrate(String hourlyrate) {
		this.hourlyrate = hourlyrate;
	}

	public String getDevicetoken() {
		return devicetoken;
	}

	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}

	public String getIsemployer() {
		return isemployer;
	}

	public void setIsemployer(String isemployer) {
		this.isemployer = isemployer;
	}

	public String getEmployerid() {
		return employerid;
	}

	public void setEmployerid(String employerid) {
		this.employerid = employerid;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getLastlogin() {
		return lastlogin;
	}

	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}

	public String created;
	public String lastlogin;

}
