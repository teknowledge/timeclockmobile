package com.timeclockmobile;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.timeclockmobile.adapter.JobTypeAdapter;
import com.timeclockmobile.data.JobTypes;
import com.timeclockmobile.database.DataBaseHelper;
import com.timeclockmobile.utils.Global;
import com.timeclockmobile.utils.Utils;
import com.timeclockmobile.webservices.ApplicationAPIs;
import com.timeclockmobile.webservices.PostingDataToServer;
import com.timeclockmobile.webservices.RequestParameters;

public class ManageJobTypesActivity extends Activity {

	private ListView jobTypesListView;
	private ArrayList<JobTypes> jobTypesArray = new ArrayList<JobTypes>();
	private DataBaseHelper db = new DataBaseHelper(ManageJobTypesActivity.this);
	private JobTypeAdapter adapter;
	public ProgressDialog progressDialog;
	private ImageButton add_jobType, backBtn;
	private TextView title_text;
	Utils utils = new Utils();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// ****** managge_clients layout can be used for manage everything
		// ********//
		setContentView(R.layout.manage_clients);

		progressDialog = new ProgressDialog(ManageJobTypesActivity.this);

		jobTypesListView = (ListView) findViewById(R.id.clientsList);

		add_jobType = (ImageButton) findViewById(R.id.add);

		backBtn = (ImageButton) findViewById(R.id.backBtn);

		title_text = (TextView) findViewById(R.id.title_text);

		title_text.setText("Manage Job Types");

		jobTypesArray = getJobTypesFromDB(ManageJobTypesActivity.this);

		adapter = new JobTypeAdapter(ManageJobTypesActivity.this,
				R.layout.manage_clients_inflate, jobTypesArray);

		jobTypesListView.setAdapter(adapter);

		add_jobType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog d = new Dialog(ManageJobTypesActivity.this);

				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.setContentView(R.layout.add_jobtype);
				d.show();
				d.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				d.setCanceledOnTouchOutside(false);
				@SuppressWarnings("static-access")
				Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				@SuppressWarnings("deprecation")
				int width = display.getWidth();
				@SuppressWarnings("deprecation")
				int height = display.getHeight();

				Log.v("width", width + "");
				// d.getWindow().setLayout((width), (int) (height));

				final EditText job_name = (EditText) d
						.findViewById(R.id.enter_jobName);
				final TextView title_text = (TextView) d
						.findViewById(R.id.title_text);

				final ImageButton backBtn = (ImageButton) d
						.findViewById(R.id.backBtn);

				title_text.setText("Add Job Type");

				backBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						d.dismiss();
					}
				});

				Button submit = (Button) d.findViewById(R.id.submit);

				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String jobName = job_name.getText().toString();

						if (!jobName.equalsIgnoreCase("")) {

							// to add job types to employer

							if (utils
									.isConnectingToInternet(ManageJobTypesActivity.this)) {
								new AddJobTypeTask(jobName).execute("");
							} else {
								utils.showDialog(ManageJobTypesActivity.this);
							}
						} else {
							Toast.makeText(getApplicationContext(),
									"Enter Job Name", Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		});

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});

	}

	public ArrayList<JobTypes> getJobTypesFromDB(Context mContext) {
		// TODO Auto-generated method stub

		ArrayList<JobTypes> jobTypesList = new ArrayList<JobTypes>();
		db = new DataBaseHelper(mContext);
		db.open();
		Cursor jobTypesdetails = db.getJobTypes();

		if (jobTypesdetails.moveToFirst()) {
			do {

				String jobType_id = jobTypesdetails.getString(0);
				String employerId = jobTypesdetails.getString(1);
				String jobTypeName = jobTypesdetails.getString(2);
				String jobTypeActive = jobTypesdetails.getString(3);

				JobTypes jobType = new JobTypes();
				jobType.setId(jobType_id);
				jobType.setmEmplloyerId(employerId);
				jobType.setmJobName(jobTypeName);
				jobType.setmActive(jobTypeActive);

				jobTypesList.add(jobType);

			} while (jobTypesdetails.moveToNext());
		}
		jobTypesdetails.close();
		db.close();
		return jobTypesList;
	}

	private class AddJobTypeTask extends AsyncTask<String, Void, Void> {

		String mResponse = "";

		JSONObject jsonResponse = new JSONObject();

		String mJobName;

		public AddJobTypeTask(String job_name) {
			mJobName = job_name;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progressDialog.setMessage("Please wait....");
			progressDialog.setCancelable(false);
			progressDialog.show();

		}

		@Override
		protected Void doInBackground(String... params) {
			//

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			try {

				// *** Add The JSON to the Entity ********

				entity.addPart(RequestParameters.METHOD, new StringBody(
						RequestParameters.ADD_JOB_TYPE));
				entity.addPart(
						RequestParameters.EMPLOYER_ID,
						new StringBody(Global.getInstance().getPreferenceVal(
								ManageJobTypesActivity.this,
								RequestParameters.EMPLOYER_ID)));
				entity.addPart(RequestParameters.JOB_NAME, new StringBody(
						mJobName));

			} catch (UnsupportedEncodingException e) {

				mResponse = "exception";

				e.printStackTrace();

			} catch (Exception e) {
				mResponse = "exception";
				e.printStackTrace();
			}

			// *** fetching team counts from api ********
			try {

				jsonResponse = PostingDataToServer.getJSONFromServer(entity,
						ApplicationAPIs.BASE_URL);

				System.out.println(jsonResponse);

			} catch (Exception e) {
				mResponse = "exception";

				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//
			super.onPostExecute(result);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				JSONObject jobTypeJson = jsonResponse.getJSONObject("response");
				String success = jobTypeJson.getString("success");
				String message = jobTypeJson.getString("message");

				if ((success.equalsIgnoreCase("1"))
						&& (message.equalsIgnoreCase("Job Type Added"))) {
					JSONObject jobTypeDetails = jobTypeJson
							.getJSONObject("jobtype");

					String jobType_Id = jobTypeDetails.getString("id");
					String jb_employer_Id = jobTypeDetails
							.getString("employerid");
					String job_Name = jobTypeDetails.getString("jobname");

					String active = jobTypeDetails.getString("active");

					db.open();
					db.insertJobTypes(jobType_Id, jb_employer_Id, job_Name,
							active);
					db.close();

					// refreshing the current activity to get the current
					// data...
					Intent i = new Intent(ManageJobTypesActivity.this,
							ManageJobTypesActivity.class); // your class
					startActivity(i);
					finish();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
