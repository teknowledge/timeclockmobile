package com.timeclockmobile;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.timeclockmobile.adapter.WorkingEmployeesAdapter;
import com.timeclockmobile.data.Employees;
import com.timeclockmobile.database.DataBaseHelper;

public class ManageWorkingEmployeesActivity extends Activity {

	private ListView workingEmployeesListView;
	private ImageButton backBtn, add;
	private TextView title_text;

	private ArrayList<Employees> workingEmployeesArrayList = new ArrayList<Employees>();
	private DataBaseHelper db = new DataBaseHelper(
			ManageWorkingEmployeesActivity.this);
	private WorkingEmployeesAdapter adapter;
	private ManageEmployeesActivity manageEmployeesActivity = new ManageEmployeesActivity();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.manage_clients);

		workingEmployeesListView = (ListView) findViewById(R.id.clientsList);

		backBtn = (ImageButton) findViewById(R.id.backBtn);
		add = (ImageButton) findViewById(R.id.add);

		title_text = (TextView) findViewById(R.id.title_text);

		add.setVisibility(View.INVISIBLE);

		title_text.setText("Who's Working");

		// getWorkingEmployeesDetails();

		workingEmployeesArrayList = manageEmployeesActivity
				.getEmployeesDetailsFromDB(ManageWorkingEmployeesActivity.this);

		adapter = new WorkingEmployeesAdapter(
				ManageWorkingEmployeesActivity.this,
				R.layout.manage_working_employee_inflate,
				workingEmployeesArrayList);

		workingEmployeesListView.setAdapter(adapter);

		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				onBackPressed();
			}
		});

	}

}
