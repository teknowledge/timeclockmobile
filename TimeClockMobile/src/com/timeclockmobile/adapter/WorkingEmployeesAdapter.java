package com.timeclockmobile.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.timeclockmobile.GetWorkingEmployeeLocation;
import com.timeclockmobile.GoogleMapViewActivity;
import com.timeclockmobile.R;
import com.timeclockmobile.data.Employees;

public class WorkingEmployeesAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<Employees> mWorkingEmployeesList;
	LayoutInflater inflater;
	GetWorkingEmployeeLocation getWorkingEmpLocation;

	public WorkingEmployeesAdapter(Context mContext, int layoutResourceId,
			ArrayList<Employees> workingEmployeesArrayList) {
		this.mContext = mContext;
		this.mWorkingEmployeesList = workingEmployeesArrayList;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mWorkingEmployeesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mWorkingEmployeesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.workingEmployeeName = (TextView) convertView
					.findViewById(R.id.working_employee_name);
			holder.working_employee_intime = (TextView) convertView
					.findViewById(R.id.working_employee_intime);
			holder.working_employee_client_and_name = (TextView) convertView
					.findViewById(R.id.working_employee_client_and_name);

			holder.check_working_employee = (CheckBox) convertView
					.findViewById(R.id.check__working_employee);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.workingEmployeeName.setText(mWorkingEmployeesList.get(position)
				.getName());

		if (mWorkingEmployeesList.get(position).getIsWorking()
				.equalsIgnoreCase("1")) {
			holder.working_employee_intime.setText(mWorkingEmployeesList.get(
					position).getTimerText());

			holder.working_employee_client_and_name
					.setText(mWorkingEmployeesList.get(position)
							.getWorking_clientName()
							+ ">"
							+ mWorkingEmployeesList.get(position)
									.getWorking_jobName());

			holder.check_working_employee.setChecked(true);

		} else {
			holder.working_employee_intime.setText("");

			holder.working_employee_client_and_name.setText("Not Working");

			holder.check_working_employee.setChecked(false);

		}

		holder.workingEmployeeName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (mWorkingEmployeesList.get(position).getIsWorking()
						.equalsIgnoreCase("1")) {

					getWorkingEmpLocation = new GetWorkingEmployeeLocation(
							mContext, mWorkingEmployeesList.get(position)
									.getId(), mWorkingEmployeesList.get(
									position).getWorking_intime());

					getWorkingEmpLocation.execute("");

					// Intent intent = new Intent(mContext,
					// GoogleMapViewActivity.class);
					//
					// intent.putExtra("startLat",
					// mWorkingEmployeesList.get(position)
					// .getWorking_startlat());
					// intent.putExtra("startLng",
					// mWorkingEmployeesList.get(position)
					// .getWorking_startlng());
					//
					//
					// intent.putExtra("clockOnTime",
					// mWorkingEmployeesList.get(position)
					// .getWorking_intime());
					//
					// intent.putExtra("from", "team_management");
					//
					//
					// mContext.startActivity(intent);
				}

			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView workingEmployeeName;
		TextView working_employee_intime;
		TextView working_employee_client_and_name;
		CheckBox check_working_employee;

	}

}
